﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master_Page.Master" AutoEventWireup="true" CodeBehind="problem_CloseCoverSheet.aspx.cs" Inherits="Jig_Checker_Problem_Data_Base.problem_CloseCoverSheet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="Content/css/main.css" rel="stylesheet" />
    <link href="Content/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" />
    <link href="Content/bootstrap-sweetalert-master/dist/animate.css" rel="stylesheet" />
    <link href="Content/bootstrap-sweetalert-master/dist/sweetalert.css" rel="stylesheet" />
    <%-- Block Back Load--%>
    <script type="text/javascript">
        function noBack() {
            window.history.forward()
        }

        noBack();
        window.onload = noBack;
        window.onpageshow = function (evt) { if (evt.persisted) noBack() }
        window.onunload = function () { void (0) }
    </script>
    <link href="Content/JsGrid/jsgrid-theme.css" rel="stylesheet" />
    <link href="Content/JsGrid/jsgrid-theme.min.css" rel="stylesheet" />
    <link href="Content/JsGrid/jsgrid.css" rel="stylesheet" />
    <link href="Content/JsGrid/jsgrid.min.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="app-title">
        <div>
            <h1><i class="fa fa-pied-piper-pp"></i>&nbsp;Close Problem List</h1>
            <p>ตารางแสดงรายการ Problem List.</p>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
            <li class="breadcrumb-item"><i class="fa fa-pied-piper-pp"></i></li>
            <li class="breadcrumb-item">Tables</li>
            <li class="breadcrumb-item active"><a href="#">Cover Sheet</a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="row">
                    <div class="col-sm-12 col-md-6" id="CountSum" style="color: red"></div>
                </div>
                <div class="tile-body">
                    <div id="LoadTool"></div>
                    <table id="jsGrid">
                        <thead>
                            <tr></tr>
                        </thead>
                        <tbody>
                            <tr></tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script src="Content/js/jquery-3.2.1.min.js"></script>
    <script src="Content/JsGrid/jsgrid.js"></script>
    <script src="Content/JsGrid/jsgrid.min.js"></script>
    <script src="Content/js/plugins/sweetalert.min.js"></script>
    <script src="Content/js/plugins/bootstrap-datepicker.min.js"></script>
    <script src="Content/numeral.min.js"></script>
    <script>$("#checkDataClose").addClass("active");</script>
    <script>
        $(document).ready(function () {
            $.ajax({
                type: "POST",
                url: "problem_CloseCoverSheet.aspx/ajaxRaad_CloseAll",
                async: false,
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: DataBindJ3CheckALL,
                error: function (response) {
                    alert(response.responseText.Message);
                }
            });
            function DataBindJ3CheckALL(response) {
                _dataJ3_Check = response.d;
                _dataJ3_Check = $.parseJSON(_dataJ3_Check);
                count = _dataJ3_Check.length;
                $("#CountSum").html("จำนวนข้อมูลทั้งหมด :" + count);
                $(function () {
                    $("#jsGrid").jsGrid({
                        hight: "85%",
                        width: "100%",
                        sorting: true,
                        paging: true,
                        autoload: true,
                        data: _dataJ3_Check,
                        pageIndex: 1,
                        pageSize: 11,
                        pageButtonCount: 3,
                        pagerFormat: "หน้าปัจจุบัน: {pageIndex} &nbsp;&nbsp; {first} {prev} {pages} {next} {last} &nbsp;&nbsp; หน้าทั้งหมด: {pageCount}",
                        pagePrevText: "<",
                        pageNextText: ">",
                        pageFirstText: "หน้าแรก  &nbsp;",
                        pageLastText: "หน้าสุดท้าย  &nbsp;",
                        pageNavigatorNextText: "&#8230;",
                        pageNavigatorPrevText: "&#8230;",

                        fields: [
                            { name: "NO", title: "No.", type: "text", width: "20%", css: "h6" },
                            { name: "PROBLEM", title: "Problem (สภาพปัญหา).", type: "text", width: "70%", css: "h6" },
                            { name: "EMP_NO", title: "Person In Charge.", type: "text", width: "25%", css: "h6" },
                            { name: "JIG_TOOL_CODE", title: "Jig Code.", type: "text", width: "25%", css: "h6" },
                            { name: "CHECK_DATE", title: "Schedule.", type: "text", width: "20%", css: "h6" },
                            {
                                name: "PLACE", title: "Status Check.", type: "text", width: "20%",
                                itemTemplate: function (value, item) {
                                    if (value == "Wait Close") {
                                        return "<div style='color: #FF8C00'>" + value + "</div>"
                                    } else if (value == "J3 Reject") {
                                        return "<div style='color: red'>" + value + "</div>"
                                    } else {
                                        return "<div style='color: #00635a'>" + value + "</div>"
                                    }

                                }
                            }, {
                                name: "LINK_FILE", title: "File Excel.", type: "text", width: "15%", css: "h6",
                                itemTemplate: function (value, item) {
                                    return $("<a  onclick='exportExcelCheck(" + '"' + item.NO + '"' + ")' target='_blank' class='btn btn-button'><span class='fa fa-folder-open' style='color: #000000' border: 1px solid #000000;></span></a>")
                                }
                            },
                              {
                                  title: "M1 Close Problem.", type: "text", width: "15%", css: "h6",
                                  itemTemplate: function (value, item) {
                                      return $("<a  class='btn btn-button' ><span class='fa fa-thumbs-up' style='color: #000000' border: 1px solid #000000;></span></a>")
                                      .on("click", function () {
                                          CloseOK(item.NO);
                                          return false;
                                      });
                                  }
                              }
                        ]
                    });
                });
            }
        });
        function CloseOK(numBer) {
            swal({
                title: "Are you sure?",
                text: "Confirm Close Ok!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes !',
                cancelButtonText: "No, cancel !",
                closeOnConfirm: false,
                closeOnCancel: true
            },
    function (isConfirm) {
        if (isConfirm) {
            var nameGet = sessionStorage.getItem("SetEMP_NAME");
            M1_CloseOK = {};
            $.extend(M1_CloseOK, {
                NO: numBer,
                APPROVE_NAME: nameGet,
                APPROVE_STATUS: "",
                APPROVE_DATE: ""
            });
            $.ajax({
                type: "POST",
                url: "problem_CloseCoverSheet.aspx/closeOk",
                data: "{M1_CloseOK: " + JSON.stringify(M1_CloseOK) + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "JSON",
                async: false,
                success: function (response) {
                    msg_Close = response.d;
                    msg_Close = $.parseJSON(msg_Close);
                    if (msg_Close == "UPDATE SUCCESS") {
                        swal({
                            title: "แจ้ง !",
                            text: "Close Ok",
                            type: "success",
                            dangerMode: true,
                            animation: true,
                            showCancelButton: true,
                        },
                        function () {
                            window.location.href = "problem_CloseCoverSheet.aspx";
                        }
                        );
                    } else {
                        swal({
                            title: "เตือน !",
                            text: "Close Fail ติดต่อ Admin",
                            type: "error",
                            dangerMode: true,
                            animation: true,
                        });
                    }
                },
                error: function (response) {
                    alert(response);
                }
            });
        } else {
            //e.preventDefault();
        }
    });
        }

        function exportExcelCheck(numBer) {
            var Coversheet_SendtoExport = {};
            $.extend(Coversheet_SendtoExport, {
                NO: numBer,
                DATE_PROBLEM: "",
                MODEL: "",
                PHASE_MT_MP: "",
                RANK_JIG: "",
                PROBLEM: "",
                JIG_TOOL_CODE: "",
                PROBLEM_TYPE: "",
                DEPT: "",
                PERSON_INCHARG: "",
                CAUSE: "",
                PERMANENT_ACTION: "",
                SCHEDULE: "",
                EMP_NO: "",
                LINK_FILE: "",
                TOOL_KEY: "",
                PROBLEM_KEY: "",
                PLACE: "",
                JIG_TOOL_NAME: ""
            });
            $.ajax({
                type: "POST",
                url: "problem_CoverSheet.aspx/sendToExportEnd",
                data: "{Coversheet_SendtoExport: " + JSON.stringify(Coversheet_SendtoExport) + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "JSON",
                async: false,
                success: function (response) {
                    msg_User = response.d;
                    msg_User = $.parseJSON(msg_User);
                    if (msg_User != "Failed") {
                        window.open("/Content/Template/tempForRead.xlsx", "_blank");
                    } else {

                    }
                },
                error: function (response) {
                    alert("ERROR");
                }
            });
        }
    </script>
</asp:Content>
