﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master_Page.Master" AutoEventWireup="true" CodeBehind="problem_CoverSheet.aspx.cs" Inherits="Jig_Checker_Problem_Data_Base.problem_CoverSheet" %>

<%@ OutputCache Duration="3600" Location="Server" VaryByParam="none" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%-- Block Back Load--%>
    <script type="text/javascript">
        function noBack() {
            window.history.forward()
        }
        noBack();
        window.onload = noBack;
        window.onpageshow = function (evt) { if (evt.persisted) noBack() }
        window.onunload = function () { void (0) }
    </script>
    <style>
        .table_cell_OutPlan {
            background-color: red;
        }

        .table_cell_SuccessPlan {
            background-color: forestgreen;
        }

        .tdbreak {
            word-break: break-all;
        }
    </style>
    <%-- class search--%>
    <style>
        .searchClass {
            margin-left: 0.5em;
            display: inline-block;
            width: auto;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            padding: .25rem .5rem;
            font-size: .875rem;
            line-height: 1.5;
            border-radius: .2rem;
        }
        @-moz-document url-prefix() {
            .dropdown-select {
                padding-left: 6px;
            }
        }

        .dropdown-dark {
            color: #fff;
            background: #444;
            border-color: #111111 #0a0a0a black;
            line-height: 1.5;
            font-size: .875rem;
            width: auto;
            background-image: -webkit-linear-gradient(top, transparent, rgba(0, 0, 0, 0.4));
            background-image: -moz-linear-gradient(top, transparent, rgba(0, 0, 0, 0.4));
            background-image: -o-linear-gradient(top, transparent, rgba(0, 0, 0, 0.4));
            background-image: linear-gradient(to bottom, transparent, rgba(0, 0, 0, 0.4));
            -webkit-box-shadow: inset 0 1px rgba(255, 255, 255, 0.1), 0 1px 1px rgba(0, 0, 0, 0.2);
            box-shadow: inset 0 1px rgba(255, 255, 255, 0.1), 0 1px 1px rgba(0, 0, 0, 0.2);
        }
            .dropdown-dark:before {
                border-bottom-color: #aaa;
            }

            .dropdown-dark:after {
                border-top-color: #aaa;
            }

            .dropdown-dark .dropdown-select {
                color: #cedbdb;
                text-shadow: 0 1px #fff;
                background: #444; /* Fallback for IE 8 */
            }

                .dropdown-dark .dropdown-select:focus {
                    color: #ccc;
                }

                .dropdown-dark .dropdown-select > option {
                    background: #444;
                    text-shadow: 0 1px rgba(0, 0, 0, 0.4);
                }
    </style>

  
    <link href="Content/JsGrid/jsgrid-theme.css" rel="stylesheet" />
    <link href="Content/JsGrid/jsgrid-theme.min.css" rel="stylesheet" />
    <link href="Content/JsGrid/jsgrid.css" rel="stylesheet" />
    <link href="Content/JsGrid/jsgrid.min.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--model detail--%>
    <div class="modal fade" id="modalDetail" style="border: 1px solid #ddd;">
        <div class="modal-dialog" style="width: 100%;">
            <div class="modal-content">
                <div class="modal-header" style="background-image: linear-gradient(#cedbdb, #e0ffe0);">
                    <h3 class="modal-title" style="font-family: Trocchi, serif; text-align: center;">Detail Problem</h3>
                    <a href="#" class="close" data-dismiss="modal">&times;</a>
                </div>
                <div class="modal-body">
                    <table class="table table-hover ">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Detail</th>
                            </tr>
                        </thead>
                        <tbody id="TdData" class="table">
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer" style="background-image: linear-gradient(#cedbdb, #e0ffe0);">
                    <a href="#" id="closeBtn" class="btn btn-secondary" data-dismiss="modal">Close</a>
                </div>
            </div>
        </div>
    </div>

    <%-- model Update CoverSheet--%>
    <div class="modal fade" id="modalInput" style="border: 1px solid #ddd;">
        <div class="modal-dialog" style="width: 1200px;">
            <div class="modal-content">
                <div class="modal-header" style="background-image: linear-gradient(#cedbdb, #e0ffe0);">
                    <h3 class="modal-title" style="font-family: Trocchi, serif; text-align: center;">Update Cover Sheet</h3>
                    <a href="#" class="close" data-dismiss="modal">&times;</a>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="selectModelproblem">Model.</label>
                            <select class="form-control required" id="selectModelproblem">
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="selectPhaseproblem">Phase.</label>
                            <select class="form-control required" id="selectPhaseproblem">
                                <option value="">Select Phase MT/MP.</option>
                                <option value="MP">MP</option>
                                <option value="MT">MT</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="selectRankproblem">Jig Rank.</label>
                            <select class="form-control required" id="selectRankproblem">
                                <option value="">Select Rank.</option>
                                <option value="A">A</option>
                                <option value="B1">B1</option>
                                <option value="B2">B2</option>
                                <option value="C">C</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="textareaProblem">Problem.</label>
                            <textarea class="form-control required" id="textareaProblem" rows="4" placeholder="กรอก Problem"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputJigCode">Jig Code</label>
                            <input class="form-control required" id="exampleInputJigCode" type="text" placeholder="กรอก Jig Code" />
                        </div>
                        <div class="form-group">
                            <label for="selectTypeproblem ">Problem Type.</label>
                            <select class="form-control required" id="selectTypeproblem">
                                <option value="">Select Rank.</option>
                                <option value="A">A</option>
                                <option value="B">B</option>
                                <option value="C">C</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="textareaCause">Cause.</label>
                            <textarea class="form-control required" id="textareaCause" rows="4" placeholder="กรอก Cause"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="textareaPermanentaction">Permanent Action.</label>
                            <textarea class="form-control required" id="textareaPermanentaction" rows="4" placeholder="กรอก Problem"></textarea>
                        </div>
                        <%--  <div class="form-group">
                            <label for="exampleInputFile">File input</label>
                            <input class="form-control-file" id="exampleInputFile" type="file" aria-describedby="fileHelp" />
                        </div>--%>
                        <div class="form-group">
                            <label for="selectToolKey">Tool Group.</label>
                            <select class="form-control required" id="selectToolKey">
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="selectProblemKey">Problem Group.</label>
                            <select class="form-control required" id="selectProblemKey">
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="selectCheck">Select J3 Check.</label>
                            <select class="form-control required" id="selectCheck">
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="updateFile">Update File.</label>
                            <input class="form-control required" type="file" id="updateFile" />
                        </div>
                        <div>
                            <label for="demoDate">Schedule.</label>
                            <input class="form-control required" id="demoDate" type="text" placeholder="Select Date" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="background-image: linear-gradient(#cedbdb, #e0ffe0);">
                    <a href="#" id="submitBtnAddCoverSheet" class="btn btn-primary"><i class="fa fa-fw fa-lg fa-check-circle"></i>Register</a>
                    <a href="#" id="closeBtnAddCoverSheet" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-fw fa-lg fa-times-circle"></i>Close</a>
                </div>
            </div>
        </div>
    </div>

    <div class="app-title">
        <div>
            <h1><i class="fa fa-pied-piper-pp"></i>&nbsp;Cover Sheet</h1>
            <p>ตารางแสดงรายการ Problem Database.</p>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
            <li class="breadcrumb-item"><i class="fa fa-pied-piper-pp"></i></li>
            <li class="breadcrumb-item">Tables</li>
            <li class="breadcrumb-item active"><a href="#">Cover Sheet</a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="row">
                    <div class="col-sm-12 col-md-6" id="CountSum" style="color: red"></div>
                    <div class="col-sm-12 col-md-6" style="text-align: right;">
                        <select class="dropdown dropdown-dark" id="selectSearch">
                            <option value="0">Select…</option>
                            <option value="TOOL_KEY_SEARCH">Tool Type</option>
                            <option value="PROBLEM_KEY_SEARCH">Problem Type</option>                          
                            <option value="PROBLEM">Problem</option>
                            <option value="JIG_TOOL_CODE">Tool Code</option>
                            <option value="JIG_TOOL_NAME">Tool Name</option>
                            <option value="EMP_NAME">Person in Change</option>
                            <option value="CHECK_STATUS">J3 Check Status</option>
                            <option value="APPROVE_STATUS">Close Status</option>
                        </select>
                        <label>
                            <input type="text" class="searchClass" onkeypress="return runScript(event)" id="myInput" placeholder="ค้นหา " title="Type in a name" />
                        </label>
                    </div>
                </div>
                <div class="tile-body">
                    <div id="LoadTool"></div>
                    <table id="jsGrid">
                        <thead>
                            <tr></tr>
                        </thead>
                        <tbody>
                            <tr></tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


    <!-- The Modal Loading -->
    <div class="modal fade" id="modalLoading">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div style="text-align: center;">
                    <img src="Content/loadingFile.gif" />
                </div>
            </div>
        </div>
    </div>

    <%-- model download file--%>
    <div class="modal fade" id="modelDownload">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h3>Upload File Problem</h3>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="tile">
                    <div class="form-group">
                        <input class="form-control" type="file" onchange="ExcleLoad(this, 'img');" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="submitSave()">Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <!-- Data table plugin-->
    <script src="Content/js/jquery-3.2.1.min.js"></script>
    <script src="Content/JsGrid/jsgrid.js"></script>
    <script src="Content/JsGrid/jsgrid.min.js"></script>
    <script src="Content/js/plugins/sweetalert.min.js"></script>
    <script src="Content/js/plugins/bootstrap-datepicker.min.js"></script>
    <script src="Content/numeral.min.js"></script>
    <script>$("#coverSheet").addClass("active");</script>
    <script>$("#coverSheetMenu").addClass("active");</script>


    <%--View--%>
    <script type="text/javascript">
        $("#LoadTool").html("<div class='centerMid'>" + "<img src='Content/load.gif' >" + "</div>");
        $.ajax({
            type: "POST",
            url: "problem_CoverSheet.aspx/ajaxRaadCoversheet",
            async: false,
            data: '{}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: DataBind,
            error: function (response) {
                alert(response.responseText.Message);
            }
        });
        function DataBind(response) {
            _dataCoversheet = response.d;
            _dataCoversheet = $.parseJSON(_dataCoversheet);
            count = _dataCoversheet.length;
            $("#CountSum").html("จำนวนข้อมูลทั้งหมด :" + count);
            $(function () {
                $("#jsGrid").jsGrid({
                    hight: "85%",
                    width: "100%",

                    sorting: true,
                    paging: true,
                    autoload: true,

                    data: _dataCoversheet,
                    pageIndex: 1,
                    pageSize: 11,
                    pageButtonCount: 3,
                    pagerFormat: "หน้าปัจจุบัน: {pageIndex} &nbsp;&nbsp; {first} {prev} {pages} {next} {last} &nbsp;&nbsp; หน้าทั้งหมด: {pageCount}",
                    pagePrevText: "<",
                    pageNextText: ">",
                    pageFirstText: "หน้าแรก  &nbsp;",
                    pageLastText: "หน้าสุดท้าย  &nbsp;",
                    pageNavigatorNextText: "&#8230;",
                    pageNavigatorPrevText: "&#8230;",

                    fields: [
                        { name: "NO", title: "No.", type: "text", width: "20%", css: "h6" },
                        { name: "DATE_PROBLEM", title: "Date.", type: "text", width: "20%", css: "h6" },
                        { name: "PROBLEM", title: "Problem (สภาพปัญหา).", type: "text", width: "70%", css: "h6" },
                        { name: "EMP_NO", title: "Person In Charge.", type: "text", width: "25%", css: "h6" },
                        { name: "PROBLEM_TYPE", title: "Problem Type.", type: "text", width: "20%", css: "h6" },
                        { name: "SCHEDULE", title: "Schedule.", type: "text", width: "20%", css: "h6" },
                        {
                            name: "status_Approved", title: "Status Check.", type: "text", width: "20%",
                            itemTemplate: function (value, item) {
                                if(value == "Wait Check"){
                                    return "<div style='color: #FF8C00'>" + value + "</div>"
                                } else if (value == "J3 Reject") {
                                    return "<div style='color: red'>" + value + "</div>"
                                } else {
                                    return "<div style='color: #00635a'>" + value + "</div>"
                                }
                               
                            }
                        },
                           {
                               name: "PLACE", title: "Status Close.", type: "text", width: "20%",
                               itemTemplate: function (value, item) {
                                   if (value == "Wait Close") {
                                       return "<div style='color: #FF8C00'>" + value + "</div>"                                  
                                   } else {
                                       return "<div style='color: #00635a'>" + value + "</div>"
                                   }

                               }
                           },

                    ],
                    rowClick: function (args) {
                        var getData = args.item;
                        var keys = Object.keys(getData);
                        var text = [];
                        detailProblem(getData.NO)
                    }
                });
            });
        }
        $("#LoadTool").html(" ");
    </script>
    <%-- detail problem--%>
    <script>
        function detailProblem(number) {
            $("#modalDetail").modal({
                backdrop: 'static',
                keyboard: false
            });
            $(function () {
                $.ajax({
                    type: "POST",
                    url: "problem_CoverSheet.aspx/ajaxRaadDetailCoversheet",
                    async: false,
                    data: '{number:"' + number + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: DataBindDetail,
                    error: function (response) {
                        alert(response.responseText.Message);
                    }
                });
            });
            function DataBindDetail(response) {
                _dataDetail = response.d;
                _dataDetail = $.parseJSON(_dataDetail);
                noSend = String(_dataDetail[0].NO);
                var statusShow = _dataDetail[0].APPROVE_STATUS;
                if (statusShow != "Close Problem") {
                    $("#TdData").html(
                                 "<tr>" +
                                    "<td>No</td>" +
                                    "<td style='color: #216bd0'>" +
                                            _dataDetail[0].NO
                                    + "</td>" +
                                "</tr>" +
                                "<tr>" +
                                    "<td>Date</td>" +
                                    "<td style='color: #216bd0'>" +
                                       _dataDetail[0].DATE_PROBLEM
                                    + "</td>" +
                                "</tr>" +
                                "<tr>" +
                                    "<td>Model</td>" +
                                    "<td style='color: #216bd0'>" +
                                         _dataDetail[0].MODEL
                                    + "</td>" +
                                "</tr>" +
                                "<tr>"
                                    + "<td>Phase MT/MP</td>" +
                                    "<td style='color: #216bd0'>" +
                                     _dataDetail[0].PHASE_MT_MP
                                    + "</td>" +
                                "</tr>" +
                                "<tr>" +
                                    "<td>Rank Jig</td>" +
                                    "<td style='color: #216bd0'>" + _dataDetail[0].RANK_JIG + "</td>" +
                                "</tr>" +
                                "<tr>" +
                                    "<td>Problem (สภาพปัญหา)</td>" +
                                    "<td class='tdbreak' style='color: #216bd0' >" + _dataDetail[0].PROBLEM + "</td>"
                                + "</tr>" +
                                "<tr>" +
                                    "<td>Jig Code</td>" +
                                    "<td style='color: #216bd0'>" + _dataDetail[0].JIG_TOOL_CODE + "</td>"
                                + "</tr>" +
                                "<tr>" +
                                    "<td>Problem Type</td>" +
                                    "<td style='color: #216bd0'>" + _dataDetail[0].PROBLEM_TYPE + "</td>"
                                + "</tr>" +
                                "<tr>" +
                                    "<td>Dept</td>" +
                                    "<td style='color: #216bd0'>" +
                                         _dataDetail[0].DEPTS
                                    + "</td>" +
                                "</tr>" +
                                 "<tr>" +
                                     "<td>Person In Charge</td>" +
                                    "<td style='color: #216bd0'>" +
                                         _dataDetail[0].EMP_NO
                                    + "</td>" +
                                "</tr>" +
                               "<tr>" +
                                    "<td>Cause</td>" +
                                   "<td class='tdbreak' style='color: #216bd0' >" + _dataDetail[0].CAUSE + "</td>" +
                                "</tr>" +
                                "<tr>" +
                                    "<td>Parmanent Action</td>" +
                                   "<td class='tdbreak' style='color: #216bd0' >" + _dataDetail[0].PERMANENT_ACTION + "</td>" +
                                "</tr>" +
                                "<tr>" +
                                   "<td>Schedule</td>" +
                                   "<td style='color: #216bd0'>" +
                                     _dataDetail[0].SCHEDULE
                                   + "</td>" +
                                "</tr>" +
                                  "<td>Flie Problem Download.</td>" +
                                   "<td>" +
                                    "<a  class='btn btn-outline-primary fa fa-download' style='color: #000000' onclick='exportExcelCheck(" + '"' + noSend + '"' + ")'></a>"
                                   + "</td>" +
                                "</tr>" +
                                "<tr  style='border-style: solid; border-color: coral;'>" +
                                "<tr >" +
                                    "<td>STEP1: Export to Template File</td>" +
                                    "<td>" +
                                        "<a class='btn btn-outline-primary fa fa-file-excel-o' style='color: #000000' onclick='exportExcleProblem(" + '"' + noSend + '"' + ")'></a>"
                                    + "</td>"
                                 + "</tr>" +
                                    "<tr>" +
                                    "<td>STEP2: Import Excel File Problem</td>" +
                                    "<td>" +
                                        "<a class='btn btn-outline-primary fa fa-file-excel-o' style='color: #000000'  data-toggle='modal' onclick='openModalupload(" + '"' + noSend + '"' + ")'></a>"
                                    + "</td>"
                                 + "</tr>" + "</tr>"
                                 );
                } else {
                    $("#TdData").html(
                               "<tr>" +
                                  "<td>No</td>" +
                                  "<td style='color: #216bd0'>" +
                                          _dataDetail[0].NO
                                  + "</td>" +
                              "</tr>" +
                              "<tr>" +
                                  "<td>Date</td>" +
                                  "<td style='color: #216bd0'>" +
                                     _dataDetail[0].DATE_PROBLEM
                                  + "</td>" +
                              "</tr>" +
                              "<tr>" +
                                  "<td>Model</td>" +
                                  "<td style='color: #216bd0'>" +
                                       _dataDetail[0].MODEL
                                  + "</td>" +
                              "</tr>" +
                              "<tr>"
                                  + "<td>Phase MT/MP</td>" +
                                  "<td style='color: #216bd0'>" +
                                   _dataDetail[0].PHASE_MT_MP
                                  + "</td>" +
                              "</tr>" +
                              "<tr>" +
                                  "<td>Rank Jig</td>" +
                                  "<td style='color: #216bd0'>" + _dataDetail[0].RANK_JIG + "</td>" +
                              "</tr>" +
                              "<tr>" +
                                  "<td>Problem (สภาพปัญหา)</td>" +
                                  "<td class='tdbreak' style='color: #216bd0' >" + _dataDetail[0].PROBLEM + "</td>"
                              + "</tr>" +
                              "<tr>" +
                                  "<td>Jig Code</td>" +
                                  "<td style='color: #216bd0'>" + _dataDetail[0].JIG_TOOL_CODE + "</td>"
                              + "</tr>" +
                              "<tr>" +
                                  "<td>Problem Type</td>" +
                                  "<td style='color: #216bd0'>" + _dataDetail[0].PROBLEM_TYPE + "</td>"
                              + "</tr>" +
                              "<tr>" +
                                  "<td>Dept</td>" +
                                  "<td style='color: #216bd0'>" +
                                       _dataDetail[0].DEPTS
                                  + "</td>" +
                              "</tr>" +
                               "<tr>" +
                                   "<td>Person In Charge</td>" +
                                  "<td style='color: #216bd0'>" +
                                       _dataDetail[0].EMP_NO
                                  + "</td>" +
                              "</tr>" +
                             "<tr>" +
                                  "<td>Cause</td>" +
                                 "<td class='tdbreak' style='color: #216bd0' >" + _dataDetail[0].CAUSE + "</td>" +
                              "</tr>" +
                              "<tr>" +
                                  "<td>Parmanent Action</td>" +
                                 "<td class='tdbreak' style='color: #216bd0' >" + _dataDetail[0].PERMANENT_ACTION + "</td>" +
                              "</tr>" +
                              "<tr>" +
                                 "<td>Schedule</td>" +
                                 "<td style='color: #216bd0'>" +
                                   _dataDetail[0].SCHEDULE
                                 + "</td>" +
                              "</tr>" +
                                "<td>Flie Problem Download.</td>" +
                                 "<td>" +
                                  "<a  class='btn btn-outline-primary fa fa-download' style='color: #000000' onclick='exportExcelCheck(" + '"' + noSend + '"' + ")'></a>"
                                 + "</td>" +
                              "</tr>" 
                               );
                }
            }
        }
        function exportExcleProblem(numBer) {
            var Coversheet_SendtoExport = {};
            $.extend(Coversheet_SendtoExport, {
                NO: numBer,
                DATE_PROBLEM: "",
                MODEL: "",
                PHASE_MT_MP: "",
                RANK_JIG: "",
                PROBLEM: "",
                JIG_TOOL_CODE: "",
                PROBLEM_TYPE: "",
                DEPT: "",
                PERSON_INCHARG: "",
                CAUSE: "",
                PERMANENT_ACTION: "",
                SCHEDULE: "",
                EMP_NO: "",
                LINK_FILE: "",
                TOOL_KEY: "",
                PROBLEM_KEY: "",
                PLACE: "",
                JIG_TOOL_NAME: ""
            });
            $.ajax({
                type: "POST",
                url: "problem_CoverSheet.aspx/sendToExportTemplate",
                data: "{Coversheet_SendtoExport: " + JSON.stringify(Coversheet_SendtoExport) + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "JSON",
                async: false,
                success: function (response) {
                    msg_User = response.d;
                    msg_User = $.parseJSON(msg_User);
                    if (msg_User != "Failed") {
                        window.open("/Content/Template/TemplateExport.xlsx", "_blank");
                    } else {

                    }
                },
                error: function (x, y ,error) {
                    alert(error);
                }
            });
        }

        function exportExcelCheck(numBer) {
            var Coversheet_SendtoExport = {};
            $.extend(Coversheet_SendtoExport, {
                NO: numBer,
                DATE_PROBLEM: "",
                MODEL: "",
                PHASE_MT_MP: "",
                RANK_JIG: "",
                PROBLEM: "",
                JIG_TOOL_CODE: "",
                PROBLEM_TYPE: "",
                DEPT: "",
                PERSON_INCHARG: "",
                CAUSE: "",
                PERMANENT_ACTION: "",
                SCHEDULE: "",
                EMP_NO: "",
                LINK_FILE: "",
                TOOL_KEY: "",
                PROBLEM_KEY: "",
                PLACE: "",
                JIG_TOOL_NAME: ""
            });
            $.ajax({
                type: "POST",
                url: "problem_CoverSheet.aspx/sendToExportEnd",
                data: "{Coversheet_SendtoExport: " + JSON.stringify(Coversheet_SendtoExport) + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "JSON",
                async: false,
                success: function (response) {
                    msg_User = response.d;
                    msg_User = $.parseJSON(msg_User);
                    if (msg_User != "Failed") {
                        window.open("/Content/Template/tempForRead.xlsx", "_blank");
                    } else {

                    }
                },
                error: function (response) {
                    alert("ERROR");
                }
            });
        }
        

    </script>
    <%-- Search--%>
    <script>    
        function runScript(e) {
            //See notes about 'which' and 'key'
            if (e.keyCode == 13) {
                var keyword = document.getElementById("selectSearch").value;
                var data = document.getElementById("myInput").value;
                searchData = {};
                $.extend(searchData, {
                    toolSelect: keyword.toString(),
                    listSearch: data.toString()
                })
                $.ajax({
                    type: "POST",
                    url: "problem_CoverSheet.aspx/ajaxSearchCoversheet",
                    data: "{searchData: " + JSON.stringify(searchData) + "}",
                    contentType: "application/json; charset=utf-8",
                    cache: false,
                    dataType: "JSON",
                    sync: false,                  
                    success: returnSearch,
                    error: function (response) {
                        alert(response);
                    }
                });
                return false;
            }
            function returnSearch(response) {
                _dataSearch = response.d;
                _dataSearch = $.parseJSON(_dataSearch);
                count = _dataCoversheet.length;
                $("#CountSum").html("จำนวนข้อมูลทั้งหมด :" + count);
                $(function () {
                    $("#jsGrid").jsGrid({
                        hight: "85%",
                        width: "100%",
                        sorting: true,
                        paging: true,
                        autoload: true,
                        data: _dataSearch,
                        pageIndex: 1,
                        pageSize: 11,
                        pageButtonCount: 3,
                        pagerFormat: "หน้าปัจจุบัน: {pageIndex} &nbsp;&nbsp; {first} {prev} {pages} {next} {last} &nbsp;&nbsp; หน้าทั้งหมด: {pageCount}",
                        pagePrevText: "<",
                        pageNextText: ">",
                        pageFirstText: "หน้าแรก  &nbsp;",
                        pageLastText: "หน้าสุดท้าย  &nbsp;",
                        pageNavigatorNextText: "&#8230;",
                        pageNavigatorPrevText: "&#8230;",

                        fields: [
                            { name: "NO", title: "No.", type: "text", width: "20%", css: "h6" },
                            { name: "DATE_PROBLEM", title: "Date.", type: "text", width: "20%", css: "h6" },
                            { name: "PROBLEM", title: "Problem (สภาพปัญหา).", type: "text", width: "70%", css: "h6" },
                            { name: "EMP_NO", title: "Person In Charge.", type: "text", width: "25%", css: "h6" },
                            { name: "PROBLEM_TYPE", title: "Problem Type.", type: "text", width: "20%", css: "h6" },
                            { name: "SCHEDULE", title: "Schedule.", type: "text", width: "20%", css: "h6" },
                            {
                                name: "status_Approved", title: "Status Check.", type: "text", width: "20%",
                                itemTemplate: function (value, item) {
                                    if(value == "Wait Check"){
                                        return "<div style='color: #FF8C00'>" + value + "</div>"
                                    } else if (value == "J3 Reject") {
                                        return "<div style='color: red'>" + value + "</div>"
                                    } else {
                                        return "<div style='color: #00635a'>" + value + "</div>"
                                    }
                               
                                }
                            },
                               {
                                   name: "PLACE", title: "Status Close.", type: "text", width: "20%",
                                   itemTemplate: function (value, item) {
                                       if (value == "Wait Close") {
                                           return "<div style='color: #FF8C00'>" + value + "</div>"                                  
                                       } else {
                                           return "<div style='color: #00635a'>" + value + "</div>"
                                       }

                                   }
                               },

                        ],
                        rowClick: function (args) {
                            var getData = args.item;
                            var keys = Object.keys(getData);
                            var text = [];
                            detailProblem(getData.NO)
                        }
                    });
                });
            }
            $("#LoadTool").html(" ");
            
        }
    </script>

    <%--Base 64--%>
    <script>
        var TempDataExcle;
        var excleProcess = {
            reduceResolutionPercent: 100,
            reduceResolution: function (val) { return ((val * this.reduceResolutionPercent) / 100); },
            checkFileExtension: function (element) {
                try {
                    var temp = element.value.split('.');
                    if (temp.length > 0)
                        if (element.accept.toUpperCase().indexOf(temp[temp.length - 1].toUpperCase()) !== -1)
                            return true;
                        else
                            return false;

                    else return false;
                } catch (ex) { return false; }
            },
            encodeImageFileAsURL: function (element) {
                //if (this.checkFileExtension(element)) {
                var delayInMilliseconds = 1000;
                var Defer = $.Deferred();
                var reader = new FileReader(); // load file
                reader.onloadend = function () {
                    //var img = new Image;
                    //img.onload = function () {
                    Defer.resolve(this);
                    TempDataExcle = reader.result;
                    setTimeout(function () {
                        $("#modalLoading").modal('toggle');
                        $("#modelDownload").modal();
                    }, delayInMilliseconds);
                    //};
                    //img.src = reader.result;
                }
                reader.onerror = function () { Defer.resolve(null); };
                reader.readAsDataURL(element.files[0]);
                return Defer.promise();
                //} else {
                //    alert('àÅ×Í¡ä´éà©¾ÒÐ .png, .jpg, .jpeg');
                //}
            },
            imageToDataUri: function (img, width, height) {
                var canvas = document.createElement('canvas'), ctx = canvas.getContext('2d');
                canvas.width = width;
                canvas.height = height;
                ctx.drawImage(img, 0, 0, width, height);
                return canvas.toDataURL();
            }
        }

        function ExcleLoad(myFileImage, imgObjId) {
            $("#modelDownload").modal('toggle');
            $("#modalLoading").modal({
                backdrop: 'static',
                keyboard: false,
            });
            $.when(excleProcess.encodeImageFileAsURL(myFileImage)).then(
                function (Data) {
                    var newDataUri = excleProcess.imageToDataUri(Data, excleProcess.reduceResolution(Data.width), excleProcess.reduceResolution(Data.height));
                    // Display('imgQuiz', newDataUri);                    
                });
        }

    </script>
    <%--Even modal--%>
    <script>
        var fileName = "";
        function openModalupload(NO_Cover) {
            $("#modalDetail").modal('toggle');
            $("#modelDownload").modal();
            fileName = NO_Cover;
        }

        function submitSave() {
            var NO_Cover = fileName;
            var TempDataExcleSendStr = String(TempDataExcle);
            TempDataExcleSend = {};
            $.extend(TempDataExcleSend, {
                fileName: NO_Cover,
                linkfile: TempDataExcleSendStr
            });
            $.ajax({
                type: "POST",
                url: "problem_CoverSheet.aspx/saveFileExcel",
                data: "{TempDataExcleSend: " + JSON.stringify(TempDataExcleSend) + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "JSON",
                async: false,
                success: function (response) {
                    _dataBack = response.d;
                    if (_dataBack == "SUCCESS") {
                        swal({
                            title: "เตือน !",
                            text: "ดำเนินการเพิ่มข้อมูลสำเร็จ!",
                            type: "success",
                            dangerMode: true,
                            animation: false,
                            customClass: "animated jackInTheBox",
                            confirmButtonClass: "btn-success",
                            confirmButtonText: "OK"
                        }, function () {
                            window.location.href = "problem_CoverSheet.aspx";
                        });
                    }
                    else {
                        alert("NO return");
                    }
                },
                error: function (response) {
                    alert("ERROR")
                }
            });
        }
    </script>


</asp:Content>
