﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master_Page.Master" AutoEventWireup="true" CodeBehind="problemDatabaseBorad.aspx.cs" Inherits="Jig_Checker_Problem_Data_Base.Pages.Borad" %>
<%@ OutputCache Duration="3600" Location="Server" VaryByParam="none" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">   
    <%-- Block Back Load--%>
    <script type="text/javascript">
        function noBack() {
            window.history.forward()
        }

        noBack();
        window.onload = noBack;
        window.onpageshow = function (evt) { if (evt.persisted) noBack() }
        window.onunload = function () { void (0) }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="app-title">
        <div>
            <h1><i class="fa fa-home fa-lg"></i>Borad</h1>
            <p>การาฟแสดงข้อมูล Problem Database.</p>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item">Borad</li>
            <li class="breadcrumb-item active"><a href="#">Cover Sheet</a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="tile">               
                <div class="embed-responsive embed-responsive-16by9">
                    <canvas class="embed-responsive-item" id="barChartDemo" width="800" height="450"></canvas>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="tile">
                <div class="embed-responsive embed-responsive-16by9">
                    <canvas class="embed-responsive-item" id="pieChartDemo" width="800" height="450"></canvas>
                </div>
            </div>
        </div>
    </div>
     <div class="row">
        <div class="col-md-6">
            <div class="tile">               
                <div class="embed-responsive embed-responsive-16by9">
                    <canvas class="embed-responsive-item" id="ajaxRaadProblemType" width="800" height="450"></canvas>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="tile">
                <div class="embed-responsive embed-responsive-16by9">
                    <canvas class="embed-responsive-item" id="ajaxRaadJigToolType" width="800" height="450"></canvas>
                </div>
            </div>
        </div>
    </div>
    <script src="Content/js/jquery-3.2.1.min.js"></script>



    <!-- Page specific javascripts-->
    <%--<script type="text/javascript" src="Content/js/plugins/chart.js"></script>--%>
    <script src="Content/js/Chart.min.js"></script>

    <script>$("#borad").addClass("active");</script>
    
    
    <script type="text/javascript">
        $.ajax({
            type: "POST",
            url: "problemDatabaseBorad.aspx/ajaxRaadstatusClose",
            async: false,
            data: '{}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: DataBind,
            error: function (response) {
                alert(response.responseText.Message);
            }
        });

        function DataBind(response) {
            _dataClose = response.d;
            _dataClose = $.parseJSON(_dataClose);
            count = _dataClose.length;
        }


        $.ajax({
            type: "POST",
            url: "problemDatabaseBorad.aspx/getStatusWaitclose",
            async: false,
            data: '{}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: DataBindWait,
            error: function (response) {
                alert(response.responseText.Message);
            }
        });

        function DataBindWait(response) {
            _dataCloseWait = response.d;
            _dataCloseWait = $.parseJSON(_dataCloseWait);
            countWait = _dataCloseWait.length;
        }


        $.ajax({
            type: "POST",
            url: "problemDatabaseBorad.aspx/getCountMount",
            async: false,
            data: '{}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: DataBindWaitMonth,
            error: function (response) {
                alert(response.responseText.Message);
            }
        });

        function DataBindWaitMonth(response) {
            _dataClosepermonth = response.d;
            _dataClosepermonth = $.parseJSON(_dataClosepermonth);
            
        }

        $.ajax({
            type: "POST",
            url: "problemDatabaseBorad.aspx/getCountMountWait",
            async: false,
            data: '{}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: DataBindWaitCloseMonth,
            error: function (response) {
                alert(response.responseText.Message);
            }
        });

        function DataBindWaitCloseMonth(response) {
            _dataClosepermonthWait = response.d;
            _dataClosepermonthWait = $.parseJSON(_dataClosepermonthWait);      
       }

        var resultWait = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        var resultClose = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        for (var k in _dataClosepermonthWait) {           
            var month = _dataClosepermonthWait[k].MOUNT_BACK;
            var toArray = _dataClosepermonthWait[k].BACK_RESULT;
            resultWait[month - 1] = toArray;
        }
        for (var l in _dataClosepermonth) {
            var month = _dataClosepermonth[l].MOUNT_BACK;
            var toArray = _dataClosepermonth[l].BACK_RESULT;
            resultClose[month - 1] = toArray;
        }
           
        
        $.ajax({
            type: "POST",
            url: "problemDatabaseBorad.aspx/ajaxRaadJigToolType",
            async: false,
            data: '{}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: DataBindJigtool,
            error: function (response) {
                alert(response.responseText.Message);
            }
        });

        function DataBindJigtool(response) {
            _dataJigTool = response.d;
            _dataJigTool = $.parseJSON(_dataJigTool);
        }

        var dynamicColors = function () {
            var r = Math.floor(Math.random() * 255);
            var g = Math.floor(Math.random() * 255);
            var b = Math.floor(Math.random() * 255);
            return "rgb(" + r + "," + g + "," + b + ")";
        }

        var ArraySumJig = [];
        var ArrayJigtype = [];
        var ArrayColor = [];
        for (var inFor in _dataJigTool) {
            var Jigtype = _dataJigTool[inFor].MOUNT_BACK;
            var sumJigType = _dataJigTool[inFor].BACK_RESULT;
            ArraySumJig.push(sumJigType);
            ArrayJigtype.push(Jigtype);
            ArrayColor.push(dynamicColors());
        }
      

        $.ajax({
            type: "POST",
            url: "problemDatabaseBorad.aspx/ajaxRaadProblemType",
            async: false,
            data: '{}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: DataBindProblemType,
            error: function (response) {
                alert(response.responseText.Message);
            }
        });

        function DataBindProblemType(response) {
            _dataProblem = response.d;
            _dataProblem = $.parseJSON(_dataProblem);
        }

        var ArraySumProblem = [];
        var ArrayProblemType = [];
        var ArrayColorProblem = [];
        for (var inFor in _dataProblem) {
            var Problemtype = _dataProblem[inFor].MOUNT_BACK;
            var sumProblemType = _dataProblem[inFor].BACK_RESULT;
            ArraySumProblem.push(sumProblemType);
            ArrayProblemType.push(Problemtype);
            ArrayColorProblem.push(dynamicColors());
        }

            var barChart = new Chart(document.getElementById("barChartDemo"), {
                type: 'bar',
                data: {
                    labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                    datasets: [
                      {
                          label: "Status Wait Close",
                          backgroundColor: "#ffa7a9",
                          data: resultWait
                      }, {
                          label: "Status Close Problem",
                          backgroundColor: "#3e95cd",
                          data: resultClose
                      }
                    ]
                },
                options: {
                    title: {
                        display: true,
                        text: 'Summary Status Per Month.'
                    }
                }
            });
         
            var pieChart = new Chart(document.getElementById("pieChartDemo"), {
                type: 'pie',
                data: {
                    labels: ["Close Problem %", "Wait Close Problem %"],
                    datasets: [{
                        label: "%",
                        backgroundColor: ["#3e95cd", "#ffa7a9"],
                        data: [count, countWait]
                    }]
                },
                options: {
                    title: {
                        display: true,
                        text: "Summary Status All."
                    },
                    tooltips: {
                        callbacks: {
                            label: function (tooltipItem, data) {
                                var allData = data.datasets[tooltipItem.datasetIndex].data;
                                var tooltipLabel = data.labels[tooltipItem.index];
                                var tooltipData = allData[tooltipItem.index];
                                var total = 0;
                                for (var i in allData) {
                                    total += allData[i];
                                }
                                var tooltipPercentage = Math.round((tooltipData / total) * 100);
                                return tooltipLabel + ': ' + tooltipData + ' (' + tooltipPercentage + '%)';
                            }
                        }
                    }
                },              
            });


            var pieChartJigtool = new Chart(document.getElementById("ajaxRaadJigToolType"), {
                type: 'pie',
                data: {
                    labels: ArrayJigtype,
                    datasets: [{
                        label: "%",
                        backgroundColor: ArrayColor,
                        data: ArraySumJig
                    }]
                },
                options: {
                    title: {
                        display: true,
                        text: "Summary Jig Tool Type Problem."
                    },
                    tooltips: {
                        callbacks: {
                            label: function (tooltipItem, data) {
                                var allData = data.datasets[tooltipItem.datasetIndex].data;
                                var tooltipLabel = data.labels[tooltipItem.index];
                                var tooltipData = allData[tooltipItem.index];
                                var total = 0;
                                for (var i in allData) {
                                    total += allData[i];
                                }
                                var tooltipPercentage = Math.round((tooltipData / total) * 100);
                                return tooltipLabel + ': ' + tooltipData + ' (' + tooltipPercentage + '%)';
                            }
                        }
                    }
                },
            });

            var pieChartJigtool = new Chart(document.getElementById("ajaxRaadProblemType"), {
                type: 'pie',
                data: {
                    labels: ArrayProblemType,
                    datasets: [{
                        label: "%",
                        backgroundColor: ArrayColorProblem,
                        data: ArraySumProblem
                    }]
                },
                options: {
                    title: {
                        display: true,
                        text: "Summary Jig Tool Type Problem."
                    },
                    tooltips: {
                        callbacks: {
                            label: function (tooltipItem, data) {
                                var allData = data.datasets[tooltipItem.datasetIndex].data;
                                var tooltipLabel = data.labels[tooltipItem.index];
                                var tooltipData = allData[tooltipItem.index];
                                var total = 0;
                                for (var i in allData) {
                                    total += allData[i];
                                }
                                var tooltipPercentage = Math.round((tooltipData / total) * 100);
                                return tooltipLabel + ': ' + tooltipData + ' (' + tooltipPercentage + '%)';
                            }
                        }
                    }
                },
            });
            
    </script>
</asp:Content>
