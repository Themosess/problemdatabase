﻿using Jig_Checker_Problem_Data_Base.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Reflection;
using System.Web;
using System.Web.Services;
using Excel = Microsoft.Office.Interop.Excel;
namespace Jig_Checker_Problem_Data_Base
{
    public partial class problem_CoverSheet : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        public static DataTable dtALL;
        public static string getNumberCoversheet()
        {
            ManageDatabase VT = new ManageDatabase();
            string numberMax = VT.getNumbercoverSheet();
            var forsum = numberMax.Split('-');
            string B = forsum[1];
            int numberConver = Int32.Parse(B);
            int numberConverRe = numberConver + 1;
            string formatRe = String.Format("{0:0000}", numberConverRe);
            string returnNO = formatRe.ToString();
            string year = DateTime.Now.ToString("yy");
            string returnString = year + "-" + returnNO;
            return returnString;
        }
        [WebMethod]
        public static string ajaxRaadCoversheet()
        {
            ManageDatabase VT = new ManageDatabase();
            var DataTool = VT.GetCoverSheet();
            ListtoDataTableConverter converter = new ListtoDataTableConverter();
            dtALL = converter.ToDataTable(DataTool);
            var data = JsonConvert.SerializeObject(DataTool, Formatting.Indented);
            return data;

        }
        [WebMethod]
        public static string ajaxRaadDetailCoversheet(string number)
        {
            ManageDatabase VT = new ManageDatabase();
            var DataTool = VT.GetDetailCoverSheet(number);
            var data = JsonConvert.SerializeObject(DataTool, Formatting.Indented);
            return data;

        }
        [WebMethod]
        public static string ajaxSearchCoversheet(property_Search searchData)
        {
            ManageDatabase VT = new ManageDatabase();
            var DataTool = VT.GetSearchCoverSheet(searchData);
            ListtoDataTableConverter converter = new ListtoDataTableConverter();
            dtALL = converter.ToDataTable(DataTool);
            var data = JsonConvert.SerializeObject(DataTool, Formatting.Indented);
            return data;
        }
        [WebMethod]
        public static string sendToExportTemplate(property_CoverSheet Coversheet_SendtoExport)
        {

            var DeleteOldFileEXE = HttpContext.Current.Server.MapPath("~/Content/Template/TemplateExport.xlsx");
            File.Delete(DeleteOldFileEXE);
            ManageDatabase VT = new ManageDatabase();
            var DataTool = VT.getTemplateExcel(Coversheet_SendtoExport);
            ListtoDataTableConverter converter = new ListtoDataTableConverter();
            dtALL = converter.ToDataTable(DataTool);
            var data = JsonConvert.SerializeObject(DataTool, Formatting.Indented);
            var strTemplate1 = HttpContext.Current.Server.MapPath("~/Content/Template/TemplateExport.xlsx");
            string strfile = strTemplate1;
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            object mis = Type.Missing;
            var strTemplate = HttpContext.Current.Server.MapPath("~/Content/Template/tempExport.xlsx");
            DateTime fileCreatedDate = File.GetCreationTime(strTemplate);
            xlApp = new Excel.Application();
            try
            {
                xlWorkBook = xlApp.Workbooks.Open(strTemplate, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                //xlWorkSheet.Name = "Sheet1";
                xlWorkSheet.Cells[3, 8] = "[" + dtALL.Rows[0]["NO"].ToString() + "]";
                xlWorkSheet.Cells[6, 4] = dtALL.Rows[0]["JIG_TOOL_NAME"].ToString();
                xlWorkSheet.Cells[6, 14] = dtALL.Rows[0]["EMP_NO"].ToString();
                xlWorkSheet.Cells[7, 14] = dtALL.Rows[0]["DATE_PROBLEM"].ToString();
                xlWorkSheet.Cells[7, 4] = dtALL.Rows[0]["JIG_TOOL_CODE"].ToString();
                xlWorkSheet.Cells[8, 4] = dtALL.Rows[0]["MODEL"].ToString();
                xlWorkSheet.Cells[8, 8] = dtALL.Rows[0]["DATE_PROBLEM"].ToString();
                xlWorkSheet.Cells[8, 11] = dtALL.Rows[0]["PLACE"].ToString();
                xlWorkSheet.Cells[10, 4] = dtALL.Rows[0]["PROBLEM"].ToString();
                xlWorkSheet.Cells[20, 4] = dtALL.Rows[0]["CAUSE"].ToString();
                string checkStatus = dtALL.Rows[0]["CHECK_STATUS"].ToString();
                string closeStatus = dtALL.Rows[0]["APPROVE_STATUS"].ToString();
                if (checkStatus == "J3 Check OK")
                {
                    xlWorkSheet.Cells[6, 15] = dtALL.Rows[0]["CHECK_NAME"].ToString();
                    xlWorkSheet.Cells[7, 15] = dtALL.Rows[0]["CHECK_DATE"].ToString();
                }
                if (closeStatus == "Close Problem")
                {
                    xlWorkSheet.Cells[6, 16] = dtALL.Rows[0]["APPROVE_NAME"].ToString();
                    xlWorkSheet.Cells[7, 16] = dtALL.Rows[0]["APPROVE_DATE"].ToString();
                }
                // xlWorkSheet.Cells[9, 4] = dtALL.Rows[0]["TOOL_KEY"].ToString();
                //xlWorkSheet.Cells[10, 4] = dtALL.Rows[0]["PROBLEM_KEY"].ToString();

                xlWorkBook.Saved = true;
                xlWorkBook.SaveAs(strfile);
                xlWorkBook.Close();
                xlApp.Quit();

                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlApp);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlWorkBook);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlWorkSheet);

                xlApp = null;
                xlWorkBook = null;
                xlWorkSheet = null;
                GC.Collect();
                return data;
            }
            catch (Exception ex)
            {
                string Exback = ex.ToString();
                return "Failed";
            }

        }


        public static DataTable ExcelSelect(string path)
        {

            DataTable exDT = new DataTable();

            //Add Reference Excel 12.0
            string connStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties=\"Excel 12.0;HDR=No;IMEX=1;Header=True;\"";
            //string connStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=\"Excel 12.0;HDR=No;IMEX=1;Header=True;\"";
            //connStr = Provider=Microsoft.ACE.OLEDB.12.0;Data Source=D:\work\ztest.xlsx;Extended Properties="Excel 12.0;HDR=No;IMEX=1;Header=True;"
            //DataTable dt = new DataTable("dtExcel");

            DataSet ds = new DataSet();
            using (OleDbConnection excelCon = new OleDbConnection(connStr))
            {
                try
                {
                    excelCon.Open();
                    //string strQuery = "SELECT * From [" + sheetNames[0] + "B1:B5]";
                    string strQuery = "SELECT * From [Sheet1$]";//Input 1-1 ??? sheet

                    using (OleDbDataAdapter exDA = new OleDbDataAdapter(strQuery, excelCon))
                    {

                        exDA.Fill(exDT);

                    }
                }
                catch (OleDbException oledb)
                {
                    HttpContext.Current.Response.Write("<script>alert('" + oledb.Message.ToString() + "');</script>");
                }
                finally
                {
                    excelCon.Close();
                }
            }

            return exDT;
        }


        [WebMethod]
        public static string saveFileExcel(property_Savefile TempDataExcleSend)
        {
            String[] arrBase64PDF = TempDataExcleSend.linkfile.Split(',');
            string time = "_" + DateTime.Now.ToString("ss");
            var filenameEXE = TempDataExcleSend.fileName;
            var mapExcle = HttpContext.Current.Server.MapPath("~/Content/FilePresent");
            File.WriteAllBytes(mapExcle + "/" + filenameEXE + time + ".xlsx", Convert.FromBase64String(arrBase64PDF[1]));
            string strfile = HttpContext.Current.Server.MapPath("~/Content/FilePresent/" + filenameEXE + time + ".xlsx");
            var dataget = ExcelSelect(strfile);
            int numFor = dataget.Rows.Count;
            List<string> AuthorList = new List<string>();
            AuthorList.Add(dataget.Rows[2][6].ToString());
            AuthorList.Add(dataget.Rows[5][2].ToString());
            AuthorList.Add(dataget.Rows[6][2].ToString());
            AuthorList.Add(dataget.Rows[7][2].ToString());
            AuthorList.Add(dataget.Rows[7][6].ToString());
            AuthorList.Add(dataget.Rows[7][9].ToString());
           
            for (int i = 0; i < numFor; i++)
            {
                if ("Problem :  " == dataget.Rows[i][0].ToString())
                {

                    AuthorList.Add(dataget.Rows[i][2].ToString());
                }
            }
            for (int j = 0; j < numFor; j++)
            {
                if ("Cause :  " == dataget.Rows[j][0].ToString())
                {
                    AuthorList.Add(dataget.Rows[j][2].ToString());
                }
            }
            for (int k = 0; k < numFor; k++)
            {
                if ("Temp action :  " == dataget.Rows[k][0].ToString())
                {
                    AuthorList.Add(dataget.Rows[k][2].ToString());
                }
            }
            for (int l = 0; l < numFor; l++)
            {
                if ("Permanent action :  " == dataget.Rows[l][0].ToString())
                {
                    AuthorList.Add(dataget.Rows[l][3].ToString());
                }
            }
            property_CoverSheet dataUpdate = new property_CoverSheet();
            string noStr = AuthorList[0].ToString();
            noStr = noStr.Replace("[", "");
            noStr = noStr.Replace("]", "");
            dataUpdate.NO = noStr;
            dataUpdate.JIG_TOOL_NAME = AuthorList[1].ToString();
            dataUpdate.JIG_TOOL_CODE = AuthorList[2].ToString();
            dataUpdate.MODEL = AuthorList[3].ToString();
            dataUpdate.DATE_PROBLEM = AuthorList[4].ToString();
            dataUpdate.PLACE = AuthorList[5].ToString();
            dataUpdate.PROBLEM = AuthorList[6].ToString();
            dataUpdate.CAUSE = AuthorList[7].ToString();
            dataUpdate.TEMP_ACTION = AuthorList[8].ToString();
            dataUpdate.PERMANENT_ACTION = AuthorList[9].ToString();

            ManageDatabase VT = new ManageDatabase();
            var link = VT.getLinkforDelete(noStr);
            if (link[0].LINK_FILE == "")
            {
                dataUpdate.LINK_FILE = "/Content/FilePresent/" + filenameEXE + time + ".xlsx";
                VT.updateDBFormfile(dataUpdate);
            }
            else
            {

                var DeleteOldFileEXE = HttpContext.Current.Server.MapPath(link[0].LINK_FILE);
                File.Delete(DeleteOldFileEXE);
                dataUpdate.LINK_FILE = "/Content/FilePresent/" + filenameEXE + time + ".xlsx";
                VT.updateDBFormfile(dataUpdate);

            }


            return "SUCCESS";

        }

        [WebMethod]
        public static string sendToExportEnd(property_CoverSheet Coversheet_SendtoExport)
        {
            var DeleteOldFileEXE = HttpContext.Current.Server.MapPath("~/Content/Template/tempForRead.xlsx");
            File.Delete(DeleteOldFileEXE);
            ManageDatabase VT = new ManageDatabase();
            var DataTool = VT.getTemplateExcel(Coversheet_SendtoExport);
            ListtoDataTableConverter converter = new ListtoDataTableConverter();
            dtALL = converter.ToDataTable(DataTool);
            var data = JsonConvert.SerializeObject(DataTool, Formatting.Indented);
            var strTemplate1 = HttpContext.Current.Server.MapPath("~/Content/Template/tempForRead.xlsx");
            string strfile = strTemplate1;
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            object mis = Type.Missing;
            var strTemplate = HttpContext.Current.Server.MapPath(DataTool[0].LINK_FILE);
            DateTime fileCreatedDate = File.GetCreationTime(strTemplate);
            xlApp = new Excel.Application();
            try
            {
                xlWorkBook = xlApp.Workbooks.Open(strTemplate, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                string checkStatus = dtALL.Rows[0]["CHECK_STATUS"].ToString();
                string closeStatus = dtALL.Rows[0]["APPROVE_STATUS"].ToString();
                if (checkStatus == "J3 Check OK")
                {
                    xlWorkSheet.Cells[6, 15] = dtALL.Rows[0]["CHECK_NAME"].ToString();
                    xlWorkSheet.Cells[7, 15] = dtALL.Rows[0]["CHECK_DATE"].ToString();
                }
                if (closeStatus == "Close Problem")
                {
                    xlWorkSheet.Cells[6, 16] = dtALL.Rows[0]["APPROVE_NAME"].ToString();
                    xlWorkSheet.Cells[7, 16] = dtALL.Rows[0]["APPROVE_DATE"].ToString();
                }
                // xlWorkSheet.Cells[9, 4] = dtALL.Rows[0]["TOOL_KEY"].ToString();
                //xlWorkSheet.Cells[10, 4] = dtALL.Rows[0]["PROBLEM_KEY"].ToString();

                xlWorkBook.Saved = true;
                xlWorkBook.SaveAs(strfile);
                xlWorkBook.Close();
                xlApp.Quit();

                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlApp);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlWorkBook);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlWorkSheet);

                xlApp = null;
                xlWorkBook = null;
                xlWorkSheet = null;
                GC.Collect();
                return data;
            }
            catch (Exception ex)
            {
                string Ex = ex.ToString();
                return "Failed";
            }

        }



        public class ListtoDataTableConverter
        {
            public DataTable ToDataTable<T>(List<T> items)
            {
                DataTable dataTable = new DataTable(typeof(T).Name);
                //Get all the properties
                PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo prop in Props)
                {
                    //Setting column names as Property names
                    dataTable.Columns.Add(prop.Name);
                }

                foreach (T item in items)
                {
                    var values = new object[Props.Length];
                    for (int i = 0; i < Props.Length; i++)
                    {
                        //inserting property values to datatable rows
                        values[i] = Props[i].GetValue(item, null);
                    }

                    dataTable.Rows.Add(values);

                }
                //put a breakpoint here and check datatable
                return dataTable;
            }
        }
    }

}