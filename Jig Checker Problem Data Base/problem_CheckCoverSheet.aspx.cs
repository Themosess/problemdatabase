﻿using Jig_Checker_Problem_Data_Base.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Excel = Microsoft.Office.Interop.Excel;
namespace Jig_Checker_Problem_Data_Base
{
    public partial class problem_CheckCoverSheet : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public static DataTable dtALL;
        [WebMethod]
        public static string ajaxRaad_J3CheckByName(string name)
        {
            ManageDatabase VT = new ManageDatabase();
            var DataTool = VT.Get_J3Check(name);
            ListtoDataTableConverter converter = new ListtoDataTableConverter();
            dtALL = converter.ToDataTable(DataTool);
            var data = JsonConvert.SerializeObject(DataTool, Formatting.Indented);
            return data;

        }

        [WebMethod]
        public static string ajaxRaad_J3CheckAll()
        {
            ManageDatabase VT = new ManageDatabase();
            var DataTool = VT.Get_J3CheckAll();
            ListtoDataTableConverter converter = new ListtoDataTableConverter();
            dtALL = converter.ToDataTable(DataTool);
            var data = JsonConvert.SerializeObject(DataTool, Formatting.Indented);
            return data;

        }

        [WebMethod]
        public static string checkOk(property_CheckReturn J3_CheckOK)
        {
            ManageDatabase VT = new ManageDatabase();
            //Add TEXT//

            try { 
                VT.UpdatetoCheckOk(J3_CheckOK);
                var JsonBack = "UPDATE SUCCESS";
                var dataRe = JsonConvert.SerializeObject(JsonBack, Formatting.Indented);            
                return dataRe;
            }
            catch (Exception ex)
            {
                string tempEX = ex.ToString();
                var JsonBack = "UPDATE DATA FAILED";
                var dataRe = JsonConvert.SerializeObject(JsonBack, Formatting.Indented);
                return dataRe;
            }

        }

        [WebMethod]
        public static string checkReject(property_CheckReturn J3_CheckReject)
        {
            ManageDatabase VT = new ManageDatabase();
            //Add TEXT//
            try
            {
                VT.UpdatetoCheckReject(J3_CheckReject);
                var JsonBack = "UPDATE SUCCESS";
                var data = JsonConvert.SerializeObject(JsonBack, Formatting.Indented);
                return data;

            }
            catch (Exception)
            {
                var JsonBack = "UPDATE DATA FAILED";
                var data = JsonConvert.SerializeObject(JsonBack, Formatting.Indented);
                return data;

            }

        }

      


        public class ListtoDataTableConverter
        {
            public DataTable ToDataTable<T>(List<T> items)
            {
                DataTable dataTable = new DataTable(typeof(T).Name);
                //Get all the properties
                PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo prop in Props)
                {
                    //Setting column names as Property names
                    dataTable.Columns.Add(prop.Name);
                }

                foreach (T item in items)
                {
                    var values = new object[Props.Length];
                    for (int i = 0; i < Props.Length; i++)
                    {
                        //inserting property values to datatable rows
                        values[i] = Props[i].GetValue(item, null);
                    }

                    dataTable.Rows.Add(values);

                }
                //put a breakpoint here and check datatable
                return dataTable;
            }
        }
    }
}