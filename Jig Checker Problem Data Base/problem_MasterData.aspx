﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master_Page.Master" AutoEventWireup="true" CodeBehind="problem_MasterData.aspx.cs" Inherits="Jig_Checker_Problem_Data_Base.problem_MasterData" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <%-- Block Back Load--%>
    <script type="text/javascript">
        function noBack() {
            window.history.forward()
        }

        noBack();
        window.onload = noBack;
        window.onpageshow = function (evt) { if (evt.persisted) noBack() }
        window.onunload = function () { void (0) }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="app-title">
        <div>
            <h1><i class="fa fa-database"></i>&nbsp;Master Data</h1>
            <p>จัดการข้อมูล Problem Database.</p>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
            <li class="breadcrumb-item"><i class="fa fa-database"></i></li>
            <li class="breadcrumb-item">Page</li>
            <li class="breadcrumb-item active"><a href="#">Master Data</a></li>
        </ul>
    </div>
     <div class="row">
        <div class="col-md-6 col-lg-3">
          <div class="widget-small info coloured-icon"><i class="icon fa fa-users fa-3x"></i>
            <div class="info">
              <h4><a href="problem_MasterUser.aspx" >Users Manament</a></h4>       
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-3">
          <div class="widget-small info coloured-icon"><i class="icon  fa fa-file-text-o fa-3x"></i>
            <div class="info">
                 <h4><a href="#" onclick="openCoversheet()">Coversheet Managment</a></h4>                       
            </div>
          </div>
        </div>
         <div class="col-md-6 col-lg-3">
             <div class="widget-small warning coloured-icon">
                 <i class="icon  fa fa-check fa-3x"></i>
                 <div class="info">
                     <h4><a href="problem_CheckCoverSheet.aspx" >Wait Check Coversheet Managment</a></h4>
                 </div>
             </div>
         </div>
         <div class="col-md-6 col-lg-3">
             <div class="widget-small danger coloured-icon">
                 <i class="icon  fa fa-undo fa-3x"></i>
                 <div class="info">
                     <h4><a href="problem_RejectCoverSheet.aspx">Reject Check Coversheet Managment</a></h4>
                 </div>
             </div>
         </div>
     </div>
    <div  class="row">
         <div class="col-md-6 col-lg-3">
             <div class="widget-small primary coloured-icon">
                 <i class="icon fa fa-thumbs-o-up fa-3x"></i>
                 <div class="info">
                     <h4><a href="problem_CloseCoverSheet.aspx">Close Check Coversheet Managment</a></h4>
                 </div>
             </div>
         </div>
    </div>
        <%-- model add & Del model --%>
        <div class="modal fade" id="modalManagment" style="border: 1px solid #ddd;">
            <div class="modal-dialog" style=" width:1200px;">
                <div class="modal-content">
                    <div class="modal-header" style="background-image: linear-gradient(#cedbdb, #e0ffe0);">
                        <h3 class="modal-title" style="font-family: Trocchi, serif; text-align: center;">Coversheet Management</h3>   
                        <a href="#" class="close"  data-dismiss="modal">&times;</a>                                       
                    </div>
                    <div class="modal-body">
                        <div class="tile">
                            <div><strong style="font-size: 25px"> Add </strong></div><br />
                            <label for="dropDownModelcode"><strong>Model</strong></label>
                            <div class="row col-sm-12">
                                <select class="required form-selectNew" id="dropDownModelcode">
                                    <option value="">Code.</option>
                                    <option value="A">A</option>
                                    <option value="B">B</option>
                                    <option value="C">C</option>
                                    <option value="D">D</option>
                                    <option value="E">E</option>
                                    <option value="F">F</option>
                                    <option value="G">G</option>
                                    <option value="H">H</option>
                                    <option value="I">I</option>
                                    <option value="J">J</option>
                                    <option value="K">K</option>
                                    <option value="L">L</option>
                                    <option value="M">M</option>
                                    <option value="N">N</option>
                                    <option value="O">O</option>
                                    <option value="P">P</option>
                                    <option value="Q">Q</option>
                                    <option value="R">R</option>
                                    <option value="S">S</option>
                                    <option value="T">T</option>
                                    <option value="U">U</option>
                                    <option value="V">V</option>
                                    <option value="W">W</option>
                                    <option value="X">X</option>
                                    <option value="Y">A</option>
                                    <option value="Z">Z</option>
                                </select>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                     
                            <select class="required form-selectNew" id="dropDownModelnumber" >                                                           
                            </select>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                             <a class="btn btn-primary" style="color: #ffffff" id="addModelmaster" ><i class="fa fa-plus"></i>เพิ่ม</a>
                            </div>
                            <label for="exampleSelect1"><strong>Tool Type.</strong></label>
                            <div class="row col-sm-12">
                                <input class="form-control" style="width:66%" type="text" id="tool_Key" placeholder="เพิ่มชนิดเครื่องมือ"/>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                             <a class="btn btn-primary" style="color: #ffffff" id="addTool_key"><i class="fa fa-plus"></i>เพิ่ม</a>
                            </div>
                            <label for="exampleSelect1"><strong>Problem Type.</strong></label>
                            <div class="row col-sm-12">
                                <input class="form-control" style="width:66%" type="text" id="problem_Key" placeholder="เพิ่มชนิดปัญหา"/>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                             <a class="btn btn-primary" style="color: #ffffff" id="addProblem_key"><i class="fa fa-plus"></i>เพิ่ม</a>
                            </div>
                        </div>
                        <div class="tile">
                              <div><strong style="font-size: 25px"> Delete </strong></div><br />
                            <label for="selectDelModel"><strong>Model</strong></label>
                            <div class="row col-sm-12">
                                <select class="form-selectNew"  id="selectDelModel" >
                                   <option></option>
                                </select>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                             <a class=" btn btn-primary" style="color: #ffffff"  id="delModelMaster"><i class="fa fa-minus"></i>ลบ</a>
                            </div>
                            <label for="selectDelToolType"><strong>Tool Type.</strong></label>
                               <div class="row col-sm-12">
                                <select class="form-selectNew" style="width:66%" id="selectDelToolType">                                 
                                </select>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                             <a class="btn btn-primary" style="color: #ffffff" id="delToolKey"><i class="fa fa-minus"></i>ลบ</a>
                            </div>
                                 <label for="selectDelProblemType"><strong>Problem Type.</strong></label>
                               <div class="row col-sm-12">
                                <select class="form-selectNew" style="width:66%" id="selectDelProblemType">                                
                                </select>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                             <a class="btn btn-primary" style="color: #ffffff" id="delProblemKey"><i class="fa fa-minus"></i>ลบ</a>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" style="background-image: linear-gradient(#cedbdb, #e0ffe0);">
                        <a href="#" id="closeBtn" class="btn btn-secondary"  onclick="closeFromMaster()" data-dismiss="modal">Close</a>
                    </div>
                </div>
            </div>
        </div>

       <%-- model Seting Master User Data--%>
        <div class="modal fade bd-example-modal-lg" id="modalManagmentUser" style="border: 1px solid #ddd;">
            <div class="modal-dialog" >
                <div class="modal-content">
                    <div class="modal-header" style="background-image: linear-gradient(#cedbdb, #e0ffe0);">
                        <h3 class="modal-title" style="font-family: Trocchi, serif; text-align: center;">User Management</h3>   
                        <a href="#" class="close"  data-dismiss="modal">&times;</a>                                       
                    </div>
                    <div class="modal-body">
                        <div class="tile">
                            <label for="dropDownAppUser"><strong>User Approve.</strong></label>
                            <div class="row col-sm-12">
                                <select class="form-selectNew" id="dropDownAppUser">
                                </select>                          
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                             <a class="btn btn-primary" style="color: #ffffff" id="appUser" ><i class="fa fa-plus"></i>Approve</a>
                            </div>
                            <label for="dropDownEmpIDSelectBrand"><strong>Chang Brand.</strong></label>
                            <div class="row col-sm-12">
                                <select class="form-selectNew" id="dropDownEmpIDSelectBrand">
                                </select>  
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <select class="form-selectNew" id="dropDownSelectBrand">
                                    <option value="">เลือก Brand</option>
                                    <option value="ADMIN">Admin</option>
                                    <option value="M1">M1</option>
                                    <option value="J4">J4</option>
                                    <option value="J3">J3</option>
                                    <option value="J2">J2</option>
                                    <option value="J1">J1</option>
                                </select> 
                                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                             <a class="btn btn-primary" style="color: #ffffff" id="change_Brand"><i class="fa fa-user"></i>Change.</a>
                            </div>
                            <label for="dropDownEmpIDSelectNAMELASTNAME"><strong>Chang Name & LastName.</strong></label>
                            <div class="row col-sm-12">
                                <select class="form-selectNew" id="dropDownEmpIDSelectNAMELASTNAME">
                                </select>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                 <a class="btn btn-primary" style="color: #ffffff" id="select_NameLastName"><i class="fa fa-check"></i>Select.</a>
                            </div>
                            <br />
                             <input class="form-control" style="width: 50%; font-size: initial" type="text" id="name_Change" placeholder="Name." />
                                <br />
                            <div class="row col-sm-12">                               
                                <input class="form-control" style="width: 52%; font-size: initial" type="text" id="lastName_Change" placeholder="Last Name."  />
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                   <a class="btn btn-primary" style="color: #ffffff" id="change_NameLastName" ><i class="fa fa-user"></i>Change.</a>
                            </div>
                            <label for="dropDownEmpIDSelectDept"><strong>Chang Dept.</strong></label>
                            <div class="row col-sm-12">
                                <select class="form-selectNew" id="dropDownEmpIDSelectDept">
                                </select>  
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <select class="form-selectNew" id="dropDownSelectDepts">
                                    <option value="">เลือก Dept</option>                                   
                                    <option value="PDE-J">PDE-J HT</option>
                                    <option value="PDE-J RA">PDE-J RA</option>                                   
                                </select> 
                                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                             <a class="btn btn-primary" style="color: #ffffff" id="change_Depts"><i class="fa fa-cog"></i>Change.</a>
                            </div>
                             <label for="dropDownEmpIDSelectEmail"><strong>Chang Email.</strong></label>
                              <div class="row col-sm-12">
                                <select class="form-selectNew" id="dropDownEmpIDSelectEmail">
                                </select>
                                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                 <a class="btn btn-primary" style="color: #ffffff" id="selectEmail"><i class="fa fa-check"></i>Select.</a>
                              </div>
                            <br />
                            <div class="row col-sm-12">                               
                                <input class="form-control" style="width: 70%; font-size: initial" type="text"  id="email_Change" placeholder="Email."  />
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                   <a class="btn btn-primary" style="color: #ffffff" id="update_email" ><i class="fa fa-envelope-o"></i>Change.</a>
                            </div>
                              <label for="dropDownEmpIDpiture"><strong>Change Piture.</strong></label>
                              <div class="row col-lg-12">
                                <select class="form-selectNew" id="dropDownEmpIDpiture">
                                </select> 
                                  &nbsp;
                                   <div class="col-md-8">
                                       <input class="form-control" id="filePiture" type="file"  onchange="imgLoad(this, 'img');"/>
                                   </div>                                                                
                              </div>
                            <br />   
                            <div class="btn-group">
                                <a class="btn btn-primary" style="color: #ffffff" id="change_piture"><i class="fa fa-lg fa-edit"></i>Change.</a>
                            </div>                                                                         
                        </div>
                </div>
                    <div class="modal-footer" style="background-image: linear-gradient(#cedbdb, #e0ffe0);">
                        <a href="#" id="closeBtnUser" class="btn btn-secondary"  onclick="closeFromUsermaster()" data-dismiss="modal">Close</a>
                    </div>
                </div>
            </div>
        </div>

      <!-- The Modal Loading -->
        <div class="modal fade" id="modalLoading" >
            <div class="modal-dialog" role="document">
                <div class="modal-content"> 
                    <div style="text-align:center;">                      
                      <img src="Content/loadingFile.gif" />
                    </div>                     
                </div>
            </div>
        </div>

    <script src="Content/js/jquery-3.2.1.min.js"></script>
    <script>$("#masterData").addClass("active");</script>
    <script src="Content/js/plugins/sweetalert.min.js"></script>


    <script>
        function closeFromUsermaster() {
            setApproved();
            setUserChangeBrand();
            setUserChangeNameLastname();
            $("#name_Change").val("");
            $("#lastName_Change").val("");
            $("#dropDownSelectDepts").val("");
            setUserChangeDept();
            setUserChangeEmail();
            $("#dropDownEmpIDSelectEmail").val("");
            setUserChangePiture();
            $("#email_Change").val("");

        }
        function closeFromMaster() {
            setRevision();
            setDataDel();
            setDataDelToolKey();
            setDataDelProblemKey();
        }
        function openCoversheet() {           
            $("#modalManagment").modal({
                backdrop: 'static',
                keyboard: false
            });          
        }
        function openUser() {
            $("#modalManagmentUser").modal({
                backdrop: 'static',
                keyboard: false
            });
        }
    </script>
     <%--Droup Down List model Master Number--%>
    <script>
        $(document).ready(function () {
            setRevision();
        });       
        function setRevision() {
            $("#dropDownModelnumber").empty();
            $("#dropDownModelnumber").append('<option value=' + "" + '>' + "Number." + '</option>');
            for (var i = 0; i < 100; i++) {
                //  $("#DdrownRev").append('<option value=' + numeral(i).format('00') + '>' + numeral(i).format('00') + '</option>');
                $("#dropDownModelnumber").append('<option value=' + numeral(i).format('00') + '>' + numeral(i).format('00') + '</option>');
            }
        }
    </script>
    <%-- Add Model master function--%>
    <script>
        $('#addModelmaster').click(function (e) {
            var isValid = true;
            $('select.required').each(function () {
                if ($.trim($(this).val()) == '') {
                    isValid = false;
                    $(this).css({
                        "border": "1px solid red",
                        "background": "#FFCECE",
                    });
                }
                else {
                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                }
            });
            if (isValid == false) {
                e.preventDefault();
            }
            else {
                addModelmaster();
            }

        });
        function addModelmaster() {
             code = document.getElementById("dropDownModelcode").value;
             num = document.getElementById("dropDownModelnumber").value;
             MODEL_MASTER = code + num;
             Models = {};
             $.extend(Models, {
                 MODEL: MODEL_MASTER,
                 MODEL_DATE : ""
             })
             $.ajax({
                 type: "POST",
                 url: "problem_MasterData.aspx/InsertModel",
                 data: "{Models: " + JSON.stringify(Models) + "}",
                 contentType: "application/json; charset=utf-8",
                 cache: false,
                 dataType: "JSON",
                 //sync: false,
                 //beforeSend: function () {
                 //    $("#MyModal").modal('toggle');
                 //    $("#modalLoading").modal({
                 //        backdrop: 'static',
                 //        keyboard: false,
                 //    });
                 //},
                 success: function (response) {
                     _dataModel = response.d;
                     _dataModel = $.parseJSON(_dataModel);                
                     if (_dataModel == "SUCCESS") {                      
                         swal({
                             title: "เตือน !",
                             text: "ดำเนินการเพิ่มข้อมูลสำเร็จ!",
                             type: "success",
                             dangerMode: true,
                             animation: false,
                             customClass: "animated jackInTheBox",
                             confirmButtonClass: "btn-success",
                             confirmButtonText: "OK"
                         }, function () {
                             setDataDel();
                             setRevision();
                         });
                       
                     } else if (_dataModel == "SAVE DATA FAILED") {
                         toastr.error("Model ไม่สามารถเพิ่มซ้ำได้", "แจ้งเตือน")
                         toastr.options = {
                             "closeButton": true,
                             "debug": false,
                             "newestOnTop": false,
                             "progressBar": false,
                             "positionClass": "toast-top-right",
                             "preventDuplicates": false,
                             "onclick": null,
                             "showDuration": "300",
                             "hideDuration": "1000",
                             "timeOut": "3000",
                             "extendedTimeOut": "1000",
                             "showEasing": "swing",
                             "hideEasing": "linear",
                             "showMethod": "fadeIn",
                             "hideMethod": "fadeOut"
                         }
                     }
                     else if (_dataModel == "SAVD TOOLID DUPLICATE") {
                         swal({
                             title: "เตือน !",
                             text: "TOOL CODE ซ้ำกรุณาตรวจสอบ!",
                             type: "warning",
                             dangerMode: true,
                             animation: false,
                             customClass: "animated tada",
                             confirmButtonClass: "btn-danger",
                             confirmButtonText: "OK"
                         });                    
                     }
                 },
                 error: function (response) {
                     alert(response);
                 }
             });
        }
    </script>
    <%--Del Model master function--%>
    <script>    
        $(document).ready(function () {
            setDataDel();
        });
        function setDataDel() {
            var _dataDel;
            $("#selectDelModel").empty();
            $.ajax({
                type: "POST",
                url: "problem_MasterData.aspx/AjaxGetModelDelete",
                async: false,
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    _dataDel = response.d;
                    _dataDel = $.parseJSON(_dataDel);
                    $("#selectDelModel").append('<option value=' + "" + '>' + "เลือก Model" + '</option>');
                    $.each(_dataDel, function (_dataDel, value) {
                        $("#selectDelModel").append("<option value=" + '"' + value.MODEL + '"' + ">" + value.MODEL + "</option>");
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }
            });
        }
        $('#delModelMaster').click(function (e) {
            var isValid = true;
            $('#selectDelModel').each(function () {
                if ($.trim($(this).val()) == '') {
                    isValid = false;
                    $(this).css({
                        "border": "1px solid red",
                        "background": "#FFCECE",
                    });
                }
                else {
                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                }
            });
            if (isValid == false) {
                e.preventDefault();
            }
            else {
                delModel();
            }

        });
        function delModel() {
            Models = {};
            $.extend(Models, {
                MODEL: $("#selectDelModel").val(),
                MODEL_DATE: ""
            })
            $.ajax({
                type: "POST",
                url: "problem_MasterData.aspx/deleteModels",
                data: "{Models:" + JSON.stringify(Models) + "}",
                contentType: "application/json; charset=utf-8",
                cache: false,
                dataType: "JSON",
                async: false,
                success: function (response) {
                    _data = response.d;
                    _data = $.parseJSON(_data);
                    if (_data == "Delete Success") {
                        swal({
                            title: "เตือน !",
                            text: "ดำเนินการลบข้อมูลสำเร็จ!",
                            type: "success",
                            dangerMode: true,
                            animation: false,
                            customClass: "animated jackInTheBox",
                            confirmButtonClass: "btn-success",
                            confirmButtonText: "OK"
                        },
                     //alert("ดำเนินการเพิ่มข้อมูลเสร็จสิ้น");
                     function () {
                         setDataDel();
                     });
                    } else if (_data == "Delete Failed") {
                        toastr.error("Model ไม่สามารถเพิ่มซ้ำได้", "แจ้งเตือน")
                        toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "3000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
                    }
                },
                error: function (response) {
                    alert("ERROR")
                }
            });
        }
    </script>
    <%-- Add tool key--%>
    <script>       
        $('#addTool_key').click(function (e) {
                var isValid = true;
                $('#tool_Key').each(function () {
                    if ($.trim($(this).val()) == '') {
                        isValid = false;
                        $(this).css({
                            "border": "1px solid red",
                            "background": "#FFCECE",
                        });
                    }
                    else {
                        $(this).css({
                            "border": "",
                            "background": ""
                        });
                    }
                });
                if (isValid == false) {
                    e.preventDefault();
                }
                else {
                    addToolKey();
                }

            });
        function addToolKey() {
            toolkey = {};
            $.extend(toolkey, {               
                TOOL_KEY:"",
                TOOL_KEY_DATE: "",
                TOOL_KEY_NAME: $("#tool_Key").val()
            })
            $.ajax({
                type: "POST",
                url: "problem_MasterData.aspx/Inserttoolkey",
                data: "{toolkey: " + JSON.stringify(toolkey) + "}",
                contentType: "application/json; charset=utf-8",
                cache: false,
                dataType: "JSON",
                //sync: false,
                //beforeSend: function () {
                //    $("#MyModal").modal('toggle');
                //    $("#modalLoading").modal({
                //        backdrop: 'static',
                //        keyboard: false,
                //    });
                //},
                success: function (response) {
                    _datatoolkey = response.d;
                    _datatoolkey = $.parseJSON(_datatoolkey);
                    if (_datatoolkey == "SUCCESS") {
                        swal({
                            title: "เตือน !",
                            text: "ดำเนินการเพิ่มข้อมูลสำเร็จ!",
                            type: "success",
                            dangerMode: true,
                            animation: false,
                            customClass: "animated jackInTheBox",
                            confirmButtonClass: "btn-success",
                            confirmButtonText: "OK"
                        }, function () {
                            setDataDel();
                            setRevision();
                            setDataDelToolKey();
                            $("#tool_Key").val("");
                        });

                    } else if (_datatoolkey == "SAVE DATA FAILED") {
                        toastr.error("TOOL KEY ไม่สามารถเพิ่มซ้ำได้", "แจ้งเตือน")
                        toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "3000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
                    }
                    else if (_datatoolkey == "SAVD TOOLID DUPLICATE") {
                        swal({
                            title: "เตือน !",
                            text: "TOOL CODE ซ้ำกรุณาตรวจสอบ!",
                            type: "warning",
                            dangerMode: true,
                            animation: false,
                            customClass: "animated tada",
                            confirmButtonClass: "btn-danger",
                            confirmButtonText: "OK"
                        });
                    }
                },
                error: function (response) {
                    alert(response);
                }
            });
        }
    </script>
   <%-- Del tool key--%>
    <script>
        $(document).ready(function () {
            setDataDelToolKey();
        });
        function setDataDelToolKey() {
            var _dataDelTool;
            $("#selectDelToolType").empty();
            $.ajax({
                type: "POST",
                url: "problem_MasterData.aspx/AjaxGetToolKeyDelete",
                async: false,
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    _dataDelTool = response.d;
                    _dataDelTool = $.parseJSON(_dataDelTool);
                    $("#selectDelToolType").append('<option value=' + "" + '>' + "เลือก Tool Key" + '</option>');
                    $.each(_dataDelTool, function (_dataDelTool, value) {
                        $("#selectDelToolType").append("<option value=" + '"' + value.TOOL_KEY + '"' + ">" + value.TOOL_KEY_NAME + "</option>");
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }
            });
        }
        $('#delToolKey').click(function (e) {
            var isValid = true;
            $('#selectDelToolType').each(function () {
                if ($.trim($(this).val()) == '') {
                    isValid = false;
                    $(this).css({
                        "border": "1px solid red",
                        "background": "#FFCECE",
                    });
                }
                else {
                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                }
            });
            if (isValid == false) {
                e.preventDefault();
            }
            else {
                delToolKey();
            }
        });
        function delToolKey() {
            ToolKey = {};
            $.extend(ToolKey, {
                TOOL_KEY: $("#selectDelToolType").val(),
                TOOL_KEY_DATE: "",
                TOOL_KEY_NAME: "",
            })
            $.ajax({
                type: "POST",
                url: "problem_MasterData.aspx/deleteToolsKey",
                data: "{ToolKey:" + JSON.stringify(ToolKey) + "}",
                contentType: "application/json; charset=utf-8",
                cache: false,
                dataType: "JSON",
                async: false,
                success: function (response) {
                    _dataTool = response.d;
                    _dataTool = $.parseJSON(_dataTool);
                    if (_dataTool == "Delete Success") {
                        swal({
                            title: "เตือน !",
                            text: "ดำเนินการลบข้อมูลสำเร็จ!",
                            type: "success",
                            dangerMode: true,
                            animation: false,
                            customClass: "animated jackInTheBox",
                            confirmButtonClass: "btn-success",
                            confirmButtonText: "OK"
                        },
                     //alert("ดำเนินการเพิ่มข้อมูลเสร็จสิ้น");
                     function () {
                         setDataDelToolKey();
                     });
                    } else if (_dataTool == "Delete Failed") {
                        toastr.error("Model ไม่สามารถเพิ่มซ้ำได้", "แจ้งเตือน")
                        toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "3000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
                    }
                },
                error: function (response) {
                    alert("ERROR")
                }
            });
        }
    </script>
    <%-- Add Problem key--%>
    <script>       
        $('#addProblem_key').click(function (e) {
                var isValid = true;
                $('#problem_Key').each(function () {
                    if ($.trim($(this).val()) == '') {
                        isValid = false;
                        $(this).css({
                            "border": "1px solid red",
                            "background": "#FFCECE",
                        });
                    }
                    else {
                        $(this).css({
                            "border": "",
                            "background": ""
                        });
                    }
                });
                if (isValid == false) {
                    e.preventDefault();
                }
                else {
                    addProblemKey();
                }

            });
        function addProblemKey() {
            problemkey = {};
            $.extend(problemkey, {
                PROBLEM_KEY: "",
                PROBLEM_KEY_DATE: "",
                PROBLEM_KEY_NAME: $("#problem_Key").val()
            })
            $.ajax({
                type: "POST",
                url: "problem_MasterData.aspx/InsertProblemKey",
                data: "{problemkey: " + JSON.stringify(problemkey) + "}",
                contentType: "application/json; charset=utf-8",
                cache: false,
                dataType: "JSON",
                //sync: false,
                //beforeSend: function () {
                //    $("#MyModal").modal('toggle');
                //    $("#modalLoading").modal({
                //        backdrop: 'static',
                //        keyboard: false,
                //    });
                //},
                success: function (response) {
                    _dataproblemkey = response.d;
                    _dataproblemkey = $.parseJSON(_dataproblemkey);
                    if (_dataproblemkey == "SUCCESS") {
                        swal({
                            title: "เตือน !",
                            text: "ดำเนินการเพิ่มข้อมูลสำเร็จ!",
                            type: "success",
                            dangerMode: true,
                            animation: false,
                            customClass: "animated jackInTheBox",
                            confirmButtonClass: "btn-success",
                            confirmButtonText: "OK"
                        }, function () {
                            setDataDel();
                            setRevision();
                            $("#problem_Key").val("");
                            setDataDelProblemKey();
                           
                        });

                    } else if (_dataproblemkey == "SAVE DATA FAILED") {
                        toastr.error("PROBLEM KEY ไม่สามารถเพิ่มซ้ำได้", "แจ้งเตือน")
                        toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "3000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
                    }
                    else if (_dataproblemkey == "SAVD TOOLID DUPLICATE") {
                        swal({
                            title: "เตือน !",
                            text: "TOOL CODE ซ้ำกรุณาตรวจสอบ!",
                            type: "warning",
                            dangerMode: true,
                            animation: false,
                            customClass: "animated tada",
                            confirmButtonClass: "btn-danger",
                            confirmButtonText: "OK"
                        });
                    }
                },
                error: function (response) {
                    alert(response);
                }
            });
        }
    </script>
   <%-- Del problem key--%>
    <script>
        $(document).ready(function () {
            setDataDelProblemKey();
        });
        function setDataDelProblemKey() {
            var _dataDelProblem;
            $("#selectDelProblemType").empty();
            $.ajax({
                type: "POST",
                url: "problem_MasterData.aspx/AjaxGetProblemKeyDelete",
                async: false,
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    _dataDelProblem = response.d;
                    _dataDelProblem = $.parseJSON(_dataDelProblem);
                    $("#selectDelProblemType").append('<option value=' + "" + '>' + "เลือก Problem Key" + '</option>');
                    $.each(_dataDelProblem, function (_dataDelProblem, value) {
                        $("#selectDelProblemType").append("<option value=" + '"' + value.PROBLEM_KEY + '"' + ">" + value.PROBLEM_KEY_NAME + "</option>");
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }
            });
        }
        $('#delProblemKey').click(function (e) {
            var isValid = true;
            $('#selectDelProblemType').each(function () {
                if ($.trim($(this).val()) == '') {
                    isValid = false;
                    $(this).css({
                        "border": "1px solid red",
                        "background": "#FFCECE",
                    });
                }
                else {
                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                }
            });
            if (isValid == false) {
                e.preventDefault();
            }
            else {
                delProblemKey();
            }
        });
        function delProblemKey() {
            ProblemKey = {};
            $.extend(ProblemKey, {
                PROBLEM_KEY: $("#selectDelProblemType").val(),
                PROBLEM_KEY_DATE: "",
                PROBLEM_KEY_NAME:""
            })
            $.ajax({
                type: "POST",
                url: "problem_MasterData.aspx/deleteProblemKey",
                data: "{ProblemKey:" + JSON.stringify(ProblemKey) + "}",
                contentType: "application/json; charset=utf-8",
                cache: false,
                dataType: "JSON",
                async: false,
                success: function (response) {
                    _dataProblem = response.d;
                    _dataProblem = $.parseJSON(_dataProblem);
                    if (_dataProblem == "Delete Success") {
                        swal({
                            title: "เตือน !",
                            text: "ดำเนินการลบข้อมูลสำเร็จ!",
                            type: "success",
                            dangerMode: true,
                            animation: false,
                            customClass: "animated jackInTheBox",
                            confirmButtonClass: "btn-success",
                            confirmButtonText: "OK"
                        },
                     //alert("ดำเนินการเพิ่มข้อมูลเสร็จสิ้น");
                     function () {
                         setDataDelProblemKey();
                     });
                    } else if (_dataProblem == "Delete Failed") {
                        toastr.error("Problem Key ไม่สามารถเพิ่มซ้ำได้", "แจ้งเตือน")
                        toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "3000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
                    }
                },
                error: function (response) {
                    alert("ERROR")
                }
            });
        }
    </script>
   <%-- get User not Apporved and Approved User Master By Admin--%>
    <script>
        $(document).ready(function () {
            setApproved();
        });
        function setApproved() {
            var _dataApproved;
            $("#dropDownAppUser").empty();
            $.ajax({
                type: "POST",
                url: "problem_MasterData.aspx/AjaxGetUserApproved",
                async: false,
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    _dataApproved = response.d;
                    _dataApproved = $.parseJSON(_dataApproved);
                    $("#dropDownAppUser").append('<option value=' + "" + '>' + "เลือก User" + '</option>');
                    $.each(_dataApproved, function (_dataApproved, value) {
                        $("#dropDownAppUser").append("<option value=" + '"' + value.EMP_NO + '"' + ">" + value.EMP_NO + "</option>");
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }
            });
        }
        $('#appUser').click(function (e) {
            var isValid = true;
            $('#dropDownAppUser').each(function () {
                if ($.trim($(this).val()) == '') {
                    isValid = false;
                    $(this).css({
                        "border": "1px solid red",
                        "background": "#FFCECE",
                    });
                }
                else {
                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                }
            });
            if (isValid == false) {
                e.preventDefault();
            }
            else {
                approvedUser();
            }
        });
        function approvedUser() {
            userApproved = {};
            $.extend(userApproved, {
                EMP_NO: $("#dropDownAppUser").val(),
                EMP_NAME: "",
                EMP_LASTNAME: "",
                POSITION_ID: "",
                EMAIL: "",
                PASSWORD: "",
                STATUS: "",
                PITURE: "",
                DEPTS: "",
            });
            $.ajax({
                type: "POST",
                url: "problem_MasterData.aspx/CheckApproved",
                data: "{userApproved: " + JSON.stringify(userApproved) + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "JSON",
                async: false,
                success: function (response) {
                    msg_User = response.d;
                    msg_User = $.parseJSON(msg_User);
                    toastr.success("Approved สำเร็จ", "แจ้งเตือน")
                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": true,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "3000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    }
                },
                error: function (response) {
                    alert(response);
                }
            });
        }
    </script>
    <%-- Change Brand By Admin--%>
    <script>
        $(document).ready(function () {
            setUserChangeBrand();          
        });
        function setUserChangeBrand() {
            var _dataChenge;
            $("#dropDownEmpIDSelectBrand").empty();
            $.ajax({
                type: "POST",
                url: "problem_MasterData.aspx/AjaxGetUserChangeBrand",
                async: false,
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    _dataChenge = response.d;
                    _dataChenge = $.parseJSON(_dataChenge);
                    $("#dropDownEmpIDSelectBrand").append('<option value=' + "" + '>' + "เลือก User" + '</option>');
                    $.each(_dataChenge, function (_dataChenge, value) {
                        $("#dropDownEmpIDSelectBrand").append("<option value=" + '"' + value.EMP_NO + '"' + ">" + value.EMP_NO + "</option>");
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }
            });
        }
        $('#change_Brand').click(function (e) {
                var isValid = true;
                $('#dropDownEmpIDSelectBrand').each(function () {
                    if ($.trim($(this).val()) == '') {
                        isValid = false;
                        $(this).css({
                            "border": "1px solid red",
                            "background": "#FFCECE",
                        });
                    }
                    else {
                        $(this).css({
                            "border": "",
                            "background": ""
                        });
                    }
                });
                if (isValid == false) {
                    e.preventDefault();
                }
                else {
                    $('#dropDownSelectBrand').each(function () {
                        if ($.trim($(this).val()) == '') {
                            isValid = false;
                            $(this).css({
                                "border": "1px solid red",
                                "background": "#FFCECE",
                            });
                        }
                        else {
                            $(this).css({
                                "border": "",
                                "background": ""
                            });
                        }
                    });
                    if (isValid == false) {
                        e.preventDefault();
                    }
                    else {
                        updateBrand();
                    }
                }
            });
            function updateBrand() {
                userChangeBrand = {};
                $.extend(userChangeBrand, {
                    EMP_NO: $("#dropDownEmpIDSelectBrand").val(),
                    EMP_NAME: "",
                    EMP_LASTNAME: "",
                    POSITION_ID: $("#dropDownSelectBrand").val(),
                    EMAIL: "",
                    PASSWORD: "",
                    STATUS: "",
                    PITURE: "",
                    DEPTS: "",
                });
                $.ajax({
                    type: "POST",
                    url: "problem_MasterData.aspx/changeBrand",
                    data: "{userChangeBrand: " + JSON.stringify(userChangeBrand) + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "JSON",
                    async: false,
                    success: function (response) {
                        msg_Brand = response.d;
                        msg_Brand = $.parseJSON(msg_Brand);
                        toastr.success("เปลี่ยน Brand สำเร็จ", "แจ้งเตือน")
                        toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": true,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "3000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
                    },
                    error: function (response) {
                        alert(response);
                    }
                });
            }   
    </script>
  <%--  Change Name Last Name--%>
    <script>  
        $(document).ready(function () {
            setUserChangeNameLastname();
        });
        function setUserChangeNameLastname() {
            var _dataChenge;
            $("#dropDownEmpIDSelectNAMELASTNAME").empty();
            $.ajax({
                type: "POST",
                url: "problem_MasterData.aspx/AjaxGetUserChangeBrand",
                async: false,
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    _dataChenge = response.d;
                    _dataChenge = $.parseJSON(_dataChenge);
                    $("#dropDownEmpIDSelectNAMELASTNAME").append('<option value=' + "" + '>' + "เลือก User" + '</option>');
                    $.each(_dataChenge, function (_dataChenge, value) {
                        $("#dropDownEmpIDSelectNAMELASTNAME").append("<option value=" + '"' + value.EMP_NO + '"' + ">" + value.EMP_NO + "</option>");
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }
            });
        }
        $('#select_NameLastName').click(function (e) {
            var isValid = true;
            $('#dropDownEmpIDSelectNAMELASTNAME').each(function () {
                if ($.trim($(this).val()) == '') {
                    isValid = false;
                    $(this).css({
                        "border": "1px solid red",
                        "background": "#FFCECE",
                    });
                }
                else {
                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                }
            });
            if (isValid == false) {
                e.preventDefault();
            }
            else {
                onClickUserChange();
            }
        });
        function onClickUserChange() {
            var name_lastName = {};
            $.extend(name_lastName, {
                EMP_NO: $("#dropDownEmpIDSelectNAMELASTNAME").val(),
                EMP_NAME: "",
                EMP_LASTNAME: "",
                POSITION_ID: "",
                EMAIL: "",
                PASSWORD: "",
                STATUS: "",
                PITURE: "",
                DEPTS: "",
            });
            $.ajax({
                type: "POST",
                url: "problem_MasterData.aspx/GetUserNameLastNameUpdate",
                data: "{name_lastName: " + JSON.stringify(name_lastName) + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "JSON",
                async: false,
                success: function (response) {
                    msg_User = response.d;
                    msg_User = $.parseJSON(msg_User);
                    $("#name_Change").val(msg_User[0]["EMP_NAME"]);
                    $("#lastName_Change").val(msg_User[0]["EMP_LASTNAME"]);
                },
                error: function (response) {
                    alert(response);
                }
            });
        }
        $('#change_NameLastName').click(function (e) {
            var isValid = true;
            $('#name_Change').each(function () {
                if ($.trim($(this).val()) == '') {
                    isValid = false;
                    $(this).css({
                        "border": "1px solid red",
                        "background": "#FFCECE",
                    });
                }
                else {
                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                }
            });
            if (isValid == false) {
                e.preventDefault();
            }
            else {
                $('#lastName_Change').each(function () {
                    if ($.trim($(this).val()) == '') {
                        isValid = false;
                        $(this).css({
                            "border": "1px solid red",
                            "background": "#FFCECE",
                        });
                    }
                    else {
                        $(this).css({
                            "border": "",
                            "background": ""
                        });
                    }
                });
                if (isValid == false) {
                    e.preventDefault();
                }
                else {
                    updateUser();
                }
            }
        });

        function updateUser() {
            var name_lastName = {};
            $.extend(name_lastName, {
                EMP_NO: $("#dropDownEmpIDSelectNAMELASTNAME").val(),
                EMP_NAME: $("#name_Change").val(),
                EMP_LASTNAME: $("#lastName_Change").val(),
                POSITION_ID: "",
                EMAIL: "",
                PASSWORD: "",
                STATUS: "",
                PITURE: "",
                DEPTS: "",
            });
            $.ajax({
                type: "POST",
                url: "problem_MasterData.aspx/userNameLastNameUpdate",
                data: "{name_lastName: " + JSON.stringify(name_lastName) + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "JSON",
                async: false,
                success: function (response) {
                    msg_UserUpdate = response.d;
                    msg_UserUpdate = $.parseJSON(msg_UserUpdate);
                    if (msg_UserUpdate == "UPDATE SUCCESS") {
                        swal({
                            title: "เตือน !",
                            text: "ดำเนินการแก้ไขข้อมูลสำเร็จ!",
                            type: "success",
                            dangerMode: true,
                            animation: false,
                            customClass: "animated jackInTheBox",
                            confirmButtonClass: "btn-success",
                            confirmButtonText: "OK"
                        });
                    } else if (msg_UserUpdate == "UPDATE DATA FAILED") {
                        toastr.error("ไม่สามารถแก้ไขได้กรุณาตรวจสอบ", "แจ้งเตือน")
                        toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "3000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
                    }                  
                },
                error: function (response) {
                    alert(response);
                }
            });
        }
    </script>
   <%-- Chang Depts--%>
    <script>
        $(document).ready(function () {
            setUserChangeDept();
        });
        function setUserChangeDept() {
            var _dataChenge;
            $("#dropDownEmpIDSelectDept").empty();
            $.ajax({
                type: "POST",
                url: "problem_MasterData.aspx/AjaxGetUserChangeBrand",
                async: false,
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    _dataChenge = response.d;
                    _dataChenge = $.parseJSON(_dataChenge);
                    $("#dropDownEmpIDSelectDept").append('<option value=' + "" + '>' + "เลือก User" + '</option>');
                    $.each(_dataChenge, function (_dataChenge, value) {
                        $("#dropDownEmpIDSelectDept").append("<option value=" + '"' + value.EMP_NO + '"' + ">" + value.EMP_NO + "</option>");
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }
            });
        }
        $('#change_Depts').click(function (e) {
            var isValid = true;
            $('#dropDownEmpIDSelectDept').each(function () {
                if ($.trim($(this).val()) == '') {
                    isValid = false;
                    $(this).css({
                        "border": "1px solid red",
                        "background": "#FFCECE",
                    });
                }
                else {
                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                }
            });
            if (isValid == false) {
                e.preventDefault();
            }
            else {
                $('#dropDownSelectDepts').each(function () {
                    if ($.trim($(this).val()) == '') {
                        isValid = false;
                        $(this).css({
                            "border": "1px solid red",
                            "background": "#FFCECE",
                        });
                    }
                    else {
                        $(this).css({
                            "border": "",
                            "background": ""
                        });
                    }
                });
                if (isValid == false) {
                    e.preventDefault();
                }
                else {
                    updateDepts();
                }
            }
        });
        function updateDepts() {
            userChangeDepts = {};
            $.extend(userChangeDepts, {
                EMP_NO: $("#dropDownEmpIDSelectDept").val(),
                EMP_NAME: "",
                EMP_LASTNAME: "",
                POSITION_ID: "",
                EMAIL: "",
                PASSWORD: "",
                STATUS: "",
                PITURE: "",
                DEPTS: $("#dropDownSelectDepts").val(),
            });
            $.ajax({
                type: "POST",
                url: "problem_MasterData.aspx/changeDepts",
                data: "{userChangeDepts: " + JSON.stringify(userChangeDepts) + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "JSON",
                async: false,
                success: function (response) {
                    msg_Dept = response.d;
                    msg_Dept = $.parseJSON(msg_Dept);
                    toastr.success("เปลี่ยน Dept สำเร็จ", "แจ้งเตือน")
                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": true,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "3000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    }
                },
                error: function (response) {
                    alert(response);
                }
            });
        }
    </script>
    <%-- Chang Email--%>
    <script>
        $(document).ready(function () {
            setUserChangeEmail();
        });
        function setUserChangeEmail() {
            var _dataChenge;
            $("#dropDownEmpIDSelectEmail").empty();
            $.ajax({
                type: "POST",
                url: "problem_MasterData.aspx/AjaxGetUserChangeBrand",
                async: false,
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    _dataChenge = response.d;
                    _dataChenge = $.parseJSON(_dataChenge);
                    $("#dropDownEmpIDSelectEmail").append('<option value=' + "" + '>' + "เลือก User" + '</option>');
                    $.each(_dataChenge, function (_dataChenge, value) {
                        $("#dropDownEmpIDSelectEmail").append("<option value=" + '"' + value.EMP_NO + '"' + ">" + value.EMP_NO + "</option>");
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }
            });
        }
        $('#selectEmail').click(function (e) {
            var isValid = true;
            $('#dropDownEmpIDSelectEmail').each(function () {
                if ($.trim($(this).val()) == '') {
                    isValid = false;
                    $(this).css({
                        "border": "1px solid red",
                        "background": "#FFCECE",
                    });
                }
                else {
                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                }
            });
            if (isValid == false) {
                e.preventDefault();
            }
            else {
                getUserEmail();
            }
        });
        function getUserEmail() {          
                userChangeEmail = {};
                $.extend(userChangeEmail, {
                    EMP_NO: $("#dropDownEmpIDSelectEmail").val(),
                    EMP_NAME: "",
                    EMP_LASTNAME: "",
                    POSITION_ID: "",
                    EMAIL: "",
                    PASSWORD: "",
                    STATUS: "",
                    PITURE: "",
                    DEPTS: ""
                });
                $.ajax({
                    type: "POST",
                    url: "problem_MasterData.aspx/GetEmailUpdate",
                    data: "{userChangeEmail: " + JSON.stringify(userChangeEmail) + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "JSON",
                    async: false,
                    success: function (response) {
                        msg_User = response.d;
                        msg_User = $.parseJSON(msg_User);
                        $("#email_Change").val(msg_User[0]["EMAIL"]);                     
                    },
                    error: function (response) {
                        alert(response);
                    }
                });
        }

        $('#update_email').click(function (e) {
            var isValid = true;
            $('#email_Change').each(function () {
                if ($.trim($(this).val()) == '') {
                    isValid = false;
                    $(this).css({
                        "border": "1px solid red",
                        "background": "#FFCECE",
                    });
                }
                else {
                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                }
            });
            if (isValid == false) {
                e.preventDefault();
            }
            else {
                updateEmail();
            }
        });
        function updateEmail() {
                userChangeEmail = {};
                $.extend(userChangeEmail, {
                    EMP_NO: $("#dropDownEmpIDSelectEmail").val(),
                    EMP_NAME: "",
                    EMP_LASTNAME: "",
                    POSITION_ID: "",
                    EMAIL: $("#email_Change").val(),
                    PASSWORD: "",
                    STATUS: "",
                    PITURE: "",
                    DEPTS: ""
                });
                $.ajax({
                    type: "POST",
                    url: "problem_MasterData.aspx/changeEmail",
                    data: "{userChangeEmail: " + JSON.stringify(userChangeEmail) + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "JSON",
                    async: false,
                    success: function (response) {
                        msg_Dept = response.d;
                        msg_Dept = $.parseJSON(msg_Dept);
                        toastr.success("เปลี่ยน Email สำเร็จ", "แจ้งเตือน")
                        toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": true,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "3000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
                    },
                    error: function (response) {
                        alert(response);
                    }
                });
            }
        
    </script>
    <%--Base 64--%>
    <script>
        var TempDataIMG;
        var imgProcess = {
            reduceResolutionPercent: 100,
            reduceResolution: function (val) { return ((val * this.reduceResolutionPercent) / 100); },
            checkFileExtension: function (element) {
                try {
                    var temp = element.value.split('.');
                    if (temp.length > 0)
                        if (element.accept.toUpperCase().indexOf(temp[temp.length - 1].toUpperCase()) !== -1)
                            return true;
                        else
                            return false;

                    else return false;
                } catch (ex) { return false; }
            },
            encodeImageFileAsURL: function (element) {
                //if (this.checkFileExtension(element)) {
                var delayInMilliseconds = 1000;
                var Defer = $.Deferred();
                var reader = new FileReader(); // load file
                reader.onloadend = function () {
                    //var img = new Image;
                    //img.onload = function () {
                    Defer.resolve(this);
                    TempDataIMG = reader.result;
                    setTimeout(function () {
                        $("#modalLoading").modal('toggle');
                        $("#modalManagmentUser").modal();
                    }, delayInMilliseconds);
                    //};
                    //img.src = reader.result;
                }
                reader.onerror = function () { Defer.resolve(null); };
                reader.readAsDataURL(element.files[0]);
                return Defer.promise();
                //} else {
                //    alert('àÅ×Í¡ä´éà©¾ÒÐ .png, .jpg, .jpeg');
                //}
            },
            imageToDataUri: function (img, width, height) {
                var canvas = document.createElement('canvas'), ctx = canvas.getContext('2d');
                canvas.width = width;
                canvas.height = height;
                ctx.drawImage(img, 0, 0, width, height);
                return canvas.toDataURL();
            }
        }

        function imgLoad(myFileImage, imgObjId) {
            $("#modalManagmentUser").modal('toggle');
            $("#modalLoading").modal({
                backdrop: 'static',
                keyboard: false,
            });
            $.when(imgProcess.encodeImageFileAsURL(myFileImage)).then(
                function (Data) {
                    var newDataUri = imgProcess.imageToDataUri(Data, imgProcess.reduceResolution(Data.width), imgProcess.reduceResolution(Data.height));
                    // Display('imgQuiz', newDataUri);                    
                });
        }
    </script>
  
     <%-- Change Piture--%>
    <script>
        $(document).ready(function () {
            setUserChangePiture();
        });
        function setUserChangePiture() {
            var _dataChenge;
            $("#dropDownEmpIDpiture").empty();
            $.ajax({
                type: "POST",
                url: "problem_MasterData.aspx/AjaxGetUserChangeBrand",
                async: false,
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    _dataChenge = response.d;
                    _dataChenge = $.parseJSON(_dataChenge);
                    $("#dropDownEmpIDpiture").append('<option value=' + "" + '>' + "เลือก User" + '</option>');
                    $.each(_dataChenge, function (_dataChenge, value) {
                        $("#dropDownEmpIDpiture").append("<option value=" + '"' + value.EMP_NO + '"' + ">" + value.EMP_NO + "</option>");
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }
            });
        }
        $('#change_piture').click(function (e) {
            var isValid = true;
            $('#dropDownEmpIDpiture').each(function () {
                if ($.trim($(this).val()) == '') {
                    isValid = false;
                    $(this).css({
                        "border": "1px solid red",
                        "background": "#FFCECE",
                    });
                }
                else {
                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                }
            });
            if (isValid == false) {
                e.preventDefault();
            }
            else {
                $('#filePiture').each(function () {
                    if ($.trim($(this).val()) == '') {
                        isValid = false;
                        $(this).css({
                            "border": "1px solid red",
                            "background": "#FFCECE",
                        });
                    }
                    else {
                        $(this).css({
                            "border": "",
                            "background": ""
                        });
                    }
                });
                if (isValid == false) {
                    e.preventDefault();
                }
                else {
                    changePiture();
                }
            }
        });
        function changePiture() {
            userChangeLink = {};
            $.extend(userChangeLink, {
                EMP_NO: $("#dropDownEmpIDpiture").val(),
                EMP_NAME: "",
                EMP_LASTNAME: "",
                POSITION_ID: "",
                EMAIL: "",
                PASSWORD: "",
                STATUS: "",
                PITURE: "",
                DEPTS: ""
            });
            $.ajax({
                type: "POST",
                url: "problem_MasterData.aspx/GetLink",
                data: "{userChangeLink: " + JSON.stringify(userChangeLink) + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "JSON",
                async: false,
                success: function (response) {
                    msg_User = response.d;
                    msg_User = $.parseJSON(msg_User);
                    var linkOld = msg_User[0]["PITURE"]
                    UpdateLink(linkOld);
                },
                error: function (response) {
                    alert(response);
                }
            });
            function UpdateLink(linkOld) {
                var linkOld2 = linkOld;
                var TempDataIMGSend = String(TempDataIMG);
                userChangePiture = {};
                $.extend(userChangePiture, {
                    EMP_NO: $("#dropDownEmpIDpiture").val(),
                    EMP_NAME: "",
                    EMP_LASTNAME: "",
                    POSITION_ID: "",
                    EMAIL: "",
                    PASSWORD: "",
                    STATUS: "",
                    PITURE: linkOld2,
                    DEPTS: "",
                    LinkDitinationPiture: TempDataIMGSend
                });
                $.ajax({
                    type: "POST",
                    url: "problem_MasterData.aspx/changePiture",
                    data: "{userChangePiture: " + JSON.stringify(userChangePiture) + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "JSON",
                    async: false,
                    success: function (response) {
                        msg_Piture = response.d;
                        msg_Piture = $.parseJSON(msg_Piture);
                        toastr.success("เปลี่ยน Piture สำเร็จ", "แจ้งเตือน")
                        toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": true,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "3000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
                    },
                    error: function (response) {
                        alert(response);
                    }
                });
            }          
        }
    </script>
</asp:Content>
