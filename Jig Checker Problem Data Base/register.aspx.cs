﻿using Jig_Checker_Problem_Data_Base.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Jig_Checker_Problem_Data_Base
{
    public partial class register : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static string addUser(property_User singupList)
        {
            ManageDatabase VT = new ManageDatabase();            
            //Add TEXT//
            try
            {
                var AddBack = VT.singupUser(singupList);
                if (AddBack == true)
                {
                    var JsonBack = "SUCCESS";
                    var data = JsonConvert.SerializeObject(JsonBack, Formatting.Indented);
                    return data;
                }
                else
                {
                    var JsonBack = "SAVD TOOLID DUPLICATE";
                    var data = JsonConvert.SerializeObject(JsonBack, Formatting.Indented);
                    return data;
                }
            }
            catch (Exception ex)
            {
                string Exback = ex.ToString();
                var JsonBack = "SAVE DATA FAILED";
                var data = JsonConvert.SerializeObject(JsonBack, Formatting.Indented);
                return data;
            }
        }

    }
}