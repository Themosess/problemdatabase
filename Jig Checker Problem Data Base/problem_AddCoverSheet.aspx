﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master_Page.Master" AutoEventWireup="true" CodeBehind="problem_AddCoverSheet.aspx.cs" Inherits="Jig_Checker_Problem_Data_Base.problem_AddCoverSheet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">     
    <link href="Content/JsGrid/jsgrid-theme.css" rel="stylesheet" />
    <link href="Content/JsGrid/jsgrid-theme.min.css" rel="stylesheet" />
    <link href="Content/JsGrid/jsgrid.css" rel="stylesheet" />
    <link href="Content/JsGrid/jsgrid.min.css" rel="stylesheet" />
    <script src="Content/js/jquery-3.2.1.min.js"></script>
    <!-- Data table plugin-->
    <script src="Content/JsGrid/jsgrid.js"></script>
    <script src="Content/JsGrid/jsgrid.min.js"></script>
      <%-- Block Back Load--%>
    <script type="text/javascript">
        function noBack() {
            window.history.forward()
        }

        noBack();
        window.onload = noBack;
        window.onpageshow = function (evt) { if (evt.persisted) noBack() }
        window.onunload = function () { void (0) }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="app-title">
        <div>
            <h1><i class="fa fa-pied-piper-pp"></i>&nbsp;Cover Sheet</h1>
            <p>From Add Problem Database.</p>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
            <li class="breadcrumb-item"><i class="fa fa-pied-piper-pp"></i></li>
            <li class="breadcrumb-item">From</li>
            <li class="breadcrumb-item active"><a href="#">Cover Sheet</a></li>
        </ul>
    </div>
    <div class="col-md-12">
        <!-- The Modal Loading -->
        <div class="modal fade" id="modalLoading" >
            <div class="modal-dialog" role="document">
                <div class="modal-content"> 
                    <div style="text-align:center;">                      
                      <img src="Content/loadingFile.gif" />
                    </div>                     
                </div>
            </div>
        </div>
        <!-- The Modal Loading -->
                    <div class="modal fade" id="modalLoadingLogin">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content" style="text-align:center">
                                <div>
                                    <img src="Content/loadingLogin.gif" />
                                </div>
                            </div>
                        </div>
                    </div>
        <div class="tile">
            <h3 class="tile-title">Add Cover Sheet</h3>
            <div class="tile-body">
          
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="selectModelproblem">Model.</label>
                            <select class="form-control required" id="selectModelproblem">
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="selectPhaseproblem">Phase.</label>
                            <select class="form-control required" id="selectPhaseproblem">
                                <option value="">Select Phase MT/MP.</option>
                                <option value="MP">MP</option>
                                <option value="MT">MT</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="selectRankproblem">Jig Rank.</label>
                            <select class="form-control required" id="selectRankproblem">
                                <option value="">Select Rank.</option>
                                <option value="A">A</option>
                                <option value="B1">B1</option>
                                <option value="B2">B2</option>
                                <option value="C">C</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="textareaProblem">Problem.</label>
                            <textarea class="form-control required" id="textareaProblem" rows="4" placeholder="กรอกสภาพปัญหา"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="textareaProblem">Cause.</label>
                            <textarea class="form-control required" id="textareaCase" rows="4" placeholder="กรอกสาเหตุการเกิดปัญหา"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputJigCode">Jig Code</label>
                            <input class="form-control required" id="exampleInputJigCode" type="text" placeholder="กรอก Jig Code" />
                        </div>
                         <div class="form-group">
                            <label for="exampleInputJigName">Jig Name</label>
                            <input class="form-control required" id="exampleInputJigName" type="text" placeholder="กรอก Jig Name" />
                        </div>
                        <div class="form-group">
                            <label for="selectTypeproblem ">Problem Type.</label>
                            <select class="form-control required" id="selectTypeproblem">
                                <option value="">Select Rank.</option>
                                <option value="A">A</option>
                                <option value="B">B</option>
                                <option value="C">C</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="selectToolKey">Jig/Tool Group.</label>
                            <select class="form-control required" id="selectToolKey">
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="selectProblemKey">Problem Group.</label>
                            <select class="form-control required" id="selectProblemKey">
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="selectCheck">Select J3 Check.</label>
                            <select class="form-control required" id="selectCheck">
                            </select>
                        </div>
                        <div>
                            <label for="demoDate">Schedule.</label>
                            <input class="form-control required" id="demoDate" type="text" placeholder="Select Date" />
                        </div>
                    </div>
                
            </div>
            <div class="tile-footer">
                <button class="btn btn-primary" type="button" id="submitBtnAddCoverSheet"><i class="fa fa-fw fa-lg fa-check-circle"></i>Add</button>&nbsp;&nbsp;&nbsp;<a class="btn btn-secondary" href="#"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
            </div>
        </div>
    </div>
    <script src="Content/js/plugins/bootstrap-datepicker.min.js"></script>
    <script src="Content/numeral.min.js"></script>
    <script>$("#addCoverSheet").addClass("active");</script>
    <script>$("#coverSheetMenu").addClass("active");</script>
        <%--select Date--%>
    <script>
        $('#el').click(function () {
            $('#demoDate').datepicker({
                format: "dd/mm/yyyy",
                autoclose: true,
                todayHighlight: true
            });
        });

        $('#demoDate').datepicker({
            format: "dd/mm/yyyy",
            autoclose: true,
            todayHighlight: true
        });
    </script>


       <%--get all for Add Cover--%>
    <script>
        $(document).ready(function () {
            setDataDelProblemKey();
            setDataDelToolKey();
            setDataDel();
            setDataCheckApprove();
        });
        function setDataDelProblemKey() {
            var _dataDelProblem;
            $("#selectProblemKey").empty();
            $.ajax({
                type: "POST",
                url: "problem_MasterData.aspx/AjaxGetProblemKeyDelete",
                async: false,
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    _dataDelProblem = response.d;
                    _dataDelProblem = $.parseJSON(_dataDelProblem);
                    $("#selectProblemKey").append('<option value=' + "" + '>' + "เลือก Problem Group" + '</option>');
                    $.each(_dataDelProblem, function (_dataDelProblem, value) {
                        $("#selectProblemKey").append("<option value=" + '"' + value.PROBLEM_KEY_NAME + '"' + ">" + value.PROBLEM_KEY_NAME + "</option>");
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }
            });
        }
        function setDataDelToolKey() {
            var _dataDelTool;
            $("#selectToolKey").empty();
            $.ajax({
                type: "POST",
                url: "problem_MasterData.aspx/AjaxGetToolKeyDelete",
                async: false,
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    _dataDelTool = response.d;
                    _dataDelTool = $.parseJSON(_dataDelTool);
                    $("#selectToolKey").append('<option value=' + "" + '>' + "เลือก Tool Group." + '</option>');
                    $.each(_dataDelTool, function (_dataDelTool, value) {
                        $("#selectToolKey").append("<option value=" + '"' + value.TOOL_KEY_NAME + '"' + ">" + value.TOOL_KEY_NAME + "</option>");
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }
            });
        }
        function setDataDel() {
            var _dataDel;
            $("#selectModelproblem").empty();
            $.ajax({
                type: "POST",
                url: "problem_MasterData.aspx/AjaxGetModelDelete",
                async: false,
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    _dataDel = response.d;
                    _dataDel = $.parseJSON(_dataDel);
                    $("#selectModelproblem").append('<option value=' + "" + '>' + "เลือก Model" + '</option>');
                    $.each(_dataDel, function (_dataDel, value) {
                        $("#selectModelproblem").append("<option value=" + '"' + value.MODEL + '"' + ">" + value.MODEL + "</option>");
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }
            });
        }
    </script>


   <%-- Check Approved J4,J4--%>
    <script>
        function setDataCheckApprove() {
            var _dataDel;
            $("#selectCheck").empty();
            $.ajax({
                type: "POST",
                url: "problem_AddCoverSheet.aspx/AjaxGetJ3J4",
                async: false,
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    _dataJ3J4 = response.d;
                    _dataJ3J4 = $.parseJSON(_dataJ3J4);
                    $("#selectCheck").append('<option value=' + "" + '>' + "เลือก Approved" + '</option>');
                    $.each(_dataJ3J4, function (_dataJ3J4, value) {
                        $("#selectCheck").append("<option value=" + '"' + value.EMP_NAME + '"' + ">" + value.EMP_NAME + "</option>");
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }
            });
        }
    </script>
   
        <%-- Add Cover Sheet--%>
    <script>      
        $('#submitBtnAddCoverSheet').click(function (e) {
            var isValid = true;
            $('input[type="text"].required').each(function () {
                if ($.trim($(this).val()) == '') {
                    isValid = false;
                    $(this).css({
                        "border": "1px solid red",
                        "background": "#FFCECE",
                    });
                }
                else {
                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                }
            });
            $('select.required').each(function () {
                if ($.trim($(this).val()) == '') {
                    isValid = false;
                    $(this).css({
                        "border": "1px solid red",
                        "background": "#FFCECE",
                    });
                }
                else {
                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                }
            });
            $('textarea.required').each(function () {
                if ($.trim($(this).val()) == '') {
                    isValid = false;
                    $(this).css({
                        "border": "1px solid red",
                        "background": "#FFCECE",
                    });
                }
                else {
                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                }
            });
            
            if (isValid == false) {
                e.preventDefault();
            }
            else {
                AddCoverSheet();
            }
        });
        function AddCoverSheet() {
            var Coversheet_Add = {};
            var EMP_NO = sessionStorage.getItem("SetEMP_NO");         
            $.extend(Coversheet_Add, {
                NO: "",
                DATE_PROBLEM: "",
                MODEL: $("#selectModelproblem").val(),
                PHASE_MT_MP: $("#selectPhaseproblem").val(),
                RANK_JIG: $("#selectRankproblem").val(),
                PROBLEM: $("#textareaProblem").val(),
                JIG_TOOL_CODE: $("#exampleInputJigCode").val(),
                PROBLEM_TYPE: $("#selectTypeproblem").val(),                       
                CAUSE: $("#textareaCase").val(),
                PERMANENT_ACTION: "",
                SCHEDULE: $("#demoDate").val(),
                EMP_NO: EMP_NO,
                LINK_FILE: "",
                TOOL_KEY_NAME: $("#selectToolKey").val(),
                PROBLEM_KEY_NAME: $("#selectProblemKey").val(),
                PLACE: $("#selectCheck").val(),
                JIG_TOOL_NAME: $("#exampleInputJigName").val(),
                TEMP_ACTION:""
            });
            $.ajax({
                type: "POST",
                url: "problem_AddCoverSheet.aspx/InsertCoverSheet",
                data: "{Coversheet_Add: " + JSON.stringify(Coversheet_Add) + "}",
                contentType: "application/json; charset=utf-8",
                cache: false,
                beforeSend: function () {
                    $("#modalLoadingLogin").modal({
                        backdrop: 'static',
                        keyboard: false,
                    });
                },
                dataType: "JSON",      
                success: function (response) {
                    _data = response.d;
                    _data = $.parseJSON(_data);
                    $("#modalLoadingLogin").modal('toggle');
                    if (_data == "SUCCESS") {
                        $("#modalLoading").html('toggle');
                        swal({
                            title: "เตือน !",
                            text: "ดำเนินการเพิ่มข้อมูลสำเร็จ!",
                            type: "success",
                            dangerMode: true,
                            animation: false,
                            customClass: "animated jackInTheBox",
                            confirmButtonClass: "btn-success",
                            confirmButtonText: "OK"
                        },                    
                        function () {
                            location.href = "problem_AddCoverSheet.aspx";
                        });
                    } else if (_data == "SAVE DATA FAILED") {
                        $("#modalLoading").html('toggle');
                        swal({
                            title: "เตือน !",
                            text: "โปรดเพิ่มไฟล์แนบ PDF!",
                            type: "warning",
                            dangerMode: true,
                            animation: false,
                            customClass: "animated tada",
                            confirmButtonClass: "btn-danger",
                            confirmButtonText: "OK"
                        });
                       
                    }
                    else if (_data == "SAVD TOOLID DUPLICATE") {                      
                        swal({
                            title: "เตือน !",
                            text: "TOOL CODE ซ้ำกรุณาตรวจสอบ!",
                            type: "warning",
                            dangerMode: true,
                            animation: false,
                            customClass: "animated tada",
                            confirmButtonClass: "btn-danger",
                            confirmButtonText: "OK"
                        });
                    }
                },
                error: function (response) {
                    alert(response);
                }
            });
        }
    </script>
</asp:Content>
