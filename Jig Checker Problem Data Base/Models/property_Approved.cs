﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Jig_Checker_Problem_Data_Base.Models
{
    public class property_Approved
    {
        public string NO { get; set; }
        public string APPROVE_NAME { get; set; }
        public string APPROVE_STATUS { get; set; }
        public string APPROVE_DATE { get; set; }
        public string APPROVE_TIME { get; set; }
     }

    public class property_CountApprove {
       public string BACK_RESULT { get; set; }
       public string MOUNT_BACK { get; set; }
    }


    }