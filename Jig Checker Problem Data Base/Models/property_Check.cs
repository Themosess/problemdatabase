﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Jig_Checker_Problem_Data_Base.Models
{
    public class property_Check
    {
        public string NO { get; set; }
        public string CHECK_NAME { get; set; }
        public string CHECK_STATUS { get; set; }
        public string CHECK_DATE { get; set; }
        public string CHECK_TIME { get; set; }
    }

    public class property_CheckReturn {
        public string NO { get; set; }
        public string EMP_NAME { get; set; }
        public string CHECK_STATUS { get; set; }
        public string CHECK_DATE { get; set; }      
        public string PROBLEM { get; set; }
        public string JIG_TOOL_CODE { get; set; }
        public string LINK_FILE { get; set; }

    }
}