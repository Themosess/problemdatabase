﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Jig_Checker_Problem_Data_Base.Models
{
    public class property_ProblemKey
    {
        public string PROBLEM_KEY { get; set; }
        public string PROBLEM_KEY_DATE { get; set; }
        public string PROBLEM_KEY_NAME { get; set; }
    }
}