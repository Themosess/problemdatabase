﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Jig_Checker_Problem_Data_Base.Models
{
    public class property_User
    {
        public string EMP_NO { get; set; }
        public string EMP_NAME { get; set; }
        public string EMP_LASTNAME { get; set; }
        public string POSITION_ID { get; set; }
        public string EMAIL { get; set; }
        public string PASSWORD { get; set; }
        public string STATUS { get; set; }
        public string PITURE { get; set; }
        public string DEPTS { get; set; }
        public string LinkDitinationPiture { get; set; }

    }
    public class property_UserSearch
    {
        public string tpyeSearch { get; set; }
        public string searchData { get; set; }
    }
}