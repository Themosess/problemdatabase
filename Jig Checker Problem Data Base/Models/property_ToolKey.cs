﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Jig_Checker_Problem_Data_Base.Models
{
    public class property_ToolKey
    {
        public string TOOL_KEY { get; set; }
        public string TOOL_KEY_DATE { get; set; }
        public string TOOL_KEY_NAME { get; set; }
    }
}