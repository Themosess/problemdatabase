﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Jig_Checker_Problem_Data_Base.Models
{
    public class property_CoverSheet
    {
        public string NO { get; set; }
        public string DATE_PROBLEM { get; set; }
        public string MODEL { get; set; }
        public string PHASE_MT_MP { get; set; }
        public string RANK_JIG { get; set; }
        public string PROBLEM{ get; set; }
        public string JIG_TOOL_CODE { get; set; }
        public string PROBLEM_TYPE { get; set; }    
        public string PERSON_INCHARG { get; set; }
        public string CAUSE { get; set; }
        public string PERMANENT_ACTION { get; set; }
        public string SCHEDULE { get; set; }
        public string EMP_NO { get; set; }
        public string LINK_FILE { get; set; }
        public string TOOL_KEY_NAME { get; set; }
        public string PROBLEM_KEY_NAME { get; set; }
        public string PLACE { get; set; }
        public string JIG_TOOL_NAME { get; set; }
        public string TEMP_ACTION { get; set; }
        public string status_Approved { get; set; }
        public string DEPTS { get; set; }
        public string CHECK_STATUS { get; set; }
        public string CHECK_NAME { get; set; }
        public string CHECK_DATE { get; set; }
        public string APPROVE_NAME { get; set; }
        public string APPROVE_STATUS { get; set; }
        public string APPROVE_DATE { get; set; }
   
    }

    public class property_Savefile {
        public string fileName { get; set; }
        public string linkfile { get; set; }
    }

    public class property_Search {
        public string toolSelect { get; set; }
        public string listSearch { get; set; }
    }
}
