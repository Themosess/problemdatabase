﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;

namespace Jig_Checker_Problem_Data_Base.Models
{
    public class ManageDatabase
    {
        private OracleConnection con;

        private void connection()
        {
            string conString = ConfigurationManager.ConnectionStrings["connectionString"].ToString();
            con = new OracleConnection(conString);
        }
        // get Number coverSheet
        public string getNumbercoverSheet()
        {
            connection();
            con.Open();
            string sqlcmdShow = @"SELECT MAX(NO) from T_PROBLEM_COVERSHEET";
            OracleCommand cmd = new OracleCommand(sqlcmdShow, con);
            string backNum = cmd.ExecuteScalar().ToString();
            return backNum;
        }
        // insert coverSheet
        public bool insertCoversheet(property_CoverSheet data)
        {
            string noList = getNumbercoverSheet();
            string ConStr = "Data Source=PDE-DB-TEST/JIGDWHTEST;User ID=jigctrltest_ra;Password=jigctrltest_ra;Max Pool Size=500;Enlist=true";
            string sqlcmdInsertApproved = @"insert into T_PROBLEM_CHECK(NO, CHECK_NAME, CHECK_STATUS, CHECK_DATE, CHECK_TIME) VALUES(:NO, :CHECK_NAME, :CHECK_STATUS, :CHECK_DATE, :CHECK_TIME)";
            string sqlcmdInsertClose = @"insert into T_PROBLEM_APPROVE(NO, APPROVE_STATUS, APPROVE_DATE, APPROVE_TIME) VALUES(:NO, :APPROVE_STATUS, :APPROVE_DATE, :APPROVE_TIME)";
            string sqlcmdInsertT_TOOL = @"INSERT INTO T_PROBLEM_COVERSHEET(NO,DATE_PROBLEM,MODEL,PHASE_MT_MP,RANK_JIG,PROBLEM,JIG_TOOL_CODE,PROBLEM_TYPE,CAUSE,PERMANENT_ACTION,SCHEDULE,EMP_NO,LINK_FILE,TOOL_KEY_NAME,PROBLEM_KEY_NAME,JIG_TOOL_NAME, TEMP_ACTION) VALUES(:NO,:DATE_PROBLEM,:MODEL,:PHASE_MT_MP,:RANK_JIG,:PROBLEM,:JIG_TOOL_CODE,:PROBLEM_TYPE,:CAUSE,:PERMANENT_ACTION,:SCHEDULE,:EMP_NO,:LINK_FILE,:TOOL_KEY_NAME,:PROBLEM_KEY_NAME,:JIG_TOOL_NAME, :TEMP_ACTION)";
            using (OracleConnection con = new OracleConnection(ConStr))
            {
                try
                {
                    OracleCommand cmdClose = new OracleCommand(sqlcmdInsertClose, con);
                    OracleCommand cmdApproved = new OracleCommand(sqlcmdInsertApproved, con);
                    OracleCommand cmdTool = new OracleCommand(sqlcmdInsertT_TOOL, con);
                    con.Open();
                    OracleTransaction tran = con.BeginTransaction();
                    cmdTool.Parameters.Add(":NO", OracleDbType.Varchar2).Value = data.NO;
                    cmdTool.Parameters.Add(":DATE_PROBLEM", OracleDbType.Varchar2).Value = DateTime.Now.ToString("dd/MM/yyyy");
                    cmdTool.Parameters.Add(":MODEL", OracleDbType.Varchar2).Value = data.MODEL;
                    cmdTool.Parameters.Add(":PHASE_MT_MP", OracleDbType.Varchar2).Value = data.PHASE_MT_MP;
                    cmdTool.Parameters.Add(":RANK_JIG", OracleDbType.Varchar2).Value = data.RANK_JIG;
                    cmdTool.Parameters.Add(":PROBLEM", OracleDbType.Varchar2).Value = data.PROBLEM;
                    cmdTool.Parameters.Add(":JIG_TOOL_CODE", OracleDbType.Varchar2).Value = data.JIG_TOOL_CODE;
                    cmdTool.Parameters.Add(":PROBLEM_TYPE", OracleDbType.Varchar2).Value = data.PROBLEM_TYPE;
                    cmdTool.Parameters.Add(":CAUSE", OracleDbType.Varchar2).Value = data.CAUSE;
                    cmdTool.Parameters.Add(":PERMANENT_ACTION", OracleDbType.Varchar2).Value = data.PERMANENT_ACTION;
                    cmdTool.Parameters.Add(":SCHEDULE", OracleDbType.Varchar2).Value = data.SCHEDULE;
                    cmdTool.Parameters.Add(":EMP_NO", OracleDbType.Varchar2).Value = data.EMP_NO;
                    cmdTool.Parameters.Add(":LINK_FILE", OracleDbType.Varchar2).Value = data.LINK_FILE;
                    cmdTool.Parameters.Add(":TOOL_KEY_NAME", OracleDbType.Varchar2).Value = data.TOOL_KEY_NAME;
                    cmdTool.Parameters.Add(":PROBLEM_KEY_NAME", OracleDbType.Varchar2).Value = data.PROBLEM_KEY_NAME;
                    cmdTool.Parameters.Add(":JIG_TOOL_NAME", OracleDbType.Varchar2).Value = data.JIG_TOOL_NAME;
                    cmdTool.Parameters.Add(":TEMP_ACTION", OracleDbType.Varchar2).Value = data.TEMP_ACTION;
                    cmdApproved.Parameters.Add(":NO", OracleDbType.Varchar2).Value = data.NO;
                    cmdApproved.Parameters.Add(":CHECK_NAME", OracleDbType.Varchar2).Value = data.PLACE;
                    cmdApproved.Parameters.Add(":CHECK_STATUS", OracleDbType.Varchar2).Value = "Wait Check";
                    cmdApproved.Parameters.Add(":CHECK_DATE", OracleDbType.Varchar2).Value = DateTime.Now.ToString("dd/MM/yyyy");
                    cmdApproved.Parameters.Add(":CHECK_TIME", OracleDbType.Varchar2).Value = DateTime.Now.ToString("HH:mm:ss");
                    cmdClose.Parameters.Add(":NO", OracleDbType.Varchar2).Value = data.NO;
                    cmdClose.Parameters.Add(":APPROVE_STATUS", OracleDbType.Varchar2).Value = "Wait Close";
                    cmdClose.Parameters.Add(":APPROVE_DATE", OracleDbType.Varchar2).Value = DateTime.Now.ToString("dd/MM/yyyy");
                    cmdClose.Parameters.Add(":APPROVE_TIME", OracleDbType.Varchar2).Value = DateTime.Now.ToString("HH:mm:ss");
                    int i = cmdTool.ExecuteNonQuery();
                    int j = cmdApproved.ExecuteNonQuery();
                    int l = cmdClose.ExecuteNonQuery();
                    tran.Commit();
                    con.Close();
                    if (i >= 1 && j >= 1 && l >= 1)
                        return true;
                    else
                        return false;
                }
                catch (Exception ex)
                {
                    string exShow = ex.ToString();
                    return false;
                }
            }
        }
        // get all coverSheet
        public List<property_CoverSheet> GetCoverSheet()
        {

            connection();
            List<property_CoverSheet> coverSheetlist = new List<property_CoverSheet>();
            string sqlcmdShow = @"SELECT T_PROBLEM_COVERSHEET.NO, T_PROBLEM_COVERSHEET.DATE_PROBLEM, T_PROBLEM_COVERSHEET.MODEL, T_PROBLEM_COVERSHEET.PHASE_MT_MP, T_PROBLEM_COVERSHEET.RANK_JIG, T_PROBLEM_COVERSHEET.PROBLEM, T_PROBLEM_COVERSHEET.JIG_TOOL_CODE, T_PROBLEM_COVERSHEET.PROBLEM_TYPE, T_PROBLEM_COVERSHEET.CAUSE, T_PROBLEM_COVERSHEET.PERMANENT_ACTION, T_PROBLEM_COVERSHEET.SCHEDULE, T_PROBLEM_COVERSHEET.LINK_FILE,
            T_PROBLEM_PROBLEM_KEY.PROBLEM_KEY_NAME, T_PROBLEM_TOOL_KEY.TOOL_KEY_NAME, T_PROBLEM_USER.EMP_NAME, T_PROBLEM_USER.DEPTS, T_PROBLEM_CHECK.CHECK_STATUS, T_PROBLEM_APPROVE.APPROVE_STATUS
            from  T_PROBLEM_COVERSHEET 
            INNER JOIN T_PROBLEM_CHECK ON T_PROBLEM_COVERSHEET.NO = T_PROBLEM_CHECK.NO           
            INNER JOIN T_PROBLEM_PROBLEM_KEY ON T_PROBLEM_COVERSHEET.PROBLEM_KEY_NAME = T_PROBLEM_PROBLEM_KEY.PROBLEM_KEY_NAME
            INNER JOIN T_PROBLEM_TOOL_KEY ON T_PROBLEM_COVERSHEET.TOOL_KEY_NAME = T_PROBLEM_TOOL_KEY.TOOL_KEY_NAME
            INNER JOIN T_PROBLEM_USER ON T_PROBLEM_COVERSHEET.EMP_NO = T_PROBLEM_USER.EMP_NO
            INNER JOIN T_PROBLEM_APPROVE ON T_PROBLEM_COVERSHEET.NO  = T_PROBLEM_APPROVE.NO
            order by NO ASC";
            OracleCommand cmd = new OracleCommand(sqlcmdShow, con);
            OracleDataAdapter OD = new OracleDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            OD.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                coverSheetlist.Add(
                new property_CoverSheet
                {
                    //TOOL_NAME = Convert.ToString(dr["TOOL_NAME"])
                    NO = Convert.ToString(dr["NO"]),
                    DATE_PROBLEM = Convert.ToString(dr["DATE_PROBLEM"]),
                    MODEL = Convert.ToString(dr["MODEL"]),
                    PHASE_MT_MP = Convert.ToString(dr["PHASE_MT_MP"]),
                    RANK_JIG = Convert.ToString(dr["RANK_JIG"]),
                    PROBLEM = Convert.ToString(dr["PROBLEM"]),
                    JIG_TOOL_CODE = Convert.ToString(dr["JIG_TOOL_CODE"]),
                    PROBLEM_TYPE = Convert.ToString(dr["PROBLEM_TYPE"]),
                    CAUSE = Convert.ToString(dr["CAUSE"]),
                    PERMANENT_ACTION = Convert.ToString(dr["PERMANENT_ACTION"]),
                    SCHEDULE = Convert.ToString(dr["SCHEDULE"]),
                    EMP_NO = Convert.ToString(dr["EMP_NAME"]),
                    LINK_FILE = Convert.ToString(dr["LINK_FILE"]),
                    status_Approved = Convert.ToString(dr["CHECK_STATUS"]),
                    PLACE = Convert.ToString(dr["APPROVE_STATUS"])
                });
            }
            return coverSheetlist;
        }
        //get detail coverSheet
        public List<property_CoverSheet> GetDetailCoverSheet(string number)
        {

            connection();
            List<property_CoverSheet> coverSheetlist = new List<property_CoverSheet>();
            string sqlcmdShow = @"SELECT T_PROBLEM_COVERSHEET.NO, T_PROBLEM_COVERSHEET.DATE_PROBLEM, T_PROBLEM_COVERSHEET.MODEL, T_PROBLEM_COVERSHEET.PHASE_MT_MP, T_PROBLEM_COVERSHEET.RANK_JIG, T_PROBLEM_COVERSHEET.PROBLEM, T_PROBLEM_COVERSHEET.JIG_TOOL_CODE, T_PROBLEM_COVERSHEET.PROBLEM_TYPE, T_PROBLEM_COVERSHEET.CAUSE, T_PROBLEM_COVERSHEET.PERMANENT_ACTION, T_PROBLEM_COVERSHEET.SCHEDULE, T_PROBLEM_COVERSHEET.LINK_FILE,
            T_PROBLEM_PROBLEM_KEY.PROBLEM_KEY_NAME, T_PROBLEM_TOOL_KEY.TOOL_KEY_NAME, T_PROBLEM_USER.EMP_NAME, T_PROBLEM_USER.DEPTS, T_PROBLEM_CHECK.CHECK_STATUS, T_PROBLEM_APPROVE.APPROVE_STATUS
            from  T_PROBLEM_COVERSHEET 
            INNER JOIN T_PROBLEM_CHECK ON T_PROBLEM_COVERSHEET.NO = T_PROBLEM_CHECK.NO
            INNER JOIN T_PROBLEM_APPROVE ON T_PROBLEM_COVERSHEET.NO = T_PROBLEM_APPROVE.NO             
            INNER JOIN T_PROBLEM_PROBLEM_KEY ON T_PROBLEM_COVERSHEET.PROBLEM_KEY_NAME = T_PROBLEM_PROBLEM_KEY.PROBLEM_KEY_NAME
            INNER JOIN T_PROBLEM_TOOL_KEY ON T_PROBLEM_COVERSHEET.TOOL_KEY_NAME = T_PROBLEM_TOOL_KEY.TOOL_KEY_NAME
            INNER JOIN T_PROBLEM_USER ON T_PROBLEM_COVERSHEET.EMP_NO = T_PROBLEM_USER.EMP_NO
            where T_PROBLEM_COVERSHEET.NO ='" + number + "'";
            OracleCommand cmd = new OracleCommand(sqlcmdShow, con);
            OracleDataAdapter OD = new OracleDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            OD.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                coverSheetlist.Add(
                new property_CoverSheet
                {
                    //TOOL_NAME = Convert.ToString(dr["TOOL_NAME"])
                    NO = Convert.ToString(dr["NO"]),
                    DATE_PROBLEM = Convert.ToString(dr["DATE_PROBLEM"]),
                    MODEL = Convert.ToString(dr["MODEL"]),
                    PHASE_MT_MP = Convert.ToString(dr["PHASE_MT_MP"]),
                    RANK_JIG = Convert.ToString(dr["RANK_JIG"]),
                    PROBLEM = Convert.ToString(dr["PROBLEM"]),
                    JIG_TOOL_CODE = Convert.ToString(dr["JIG_TOOL_CODE"]),
                    PROBLEM_TYPE = Convert.ToString(dr["PROBLEM_TYPE"]),
                    CAUSE = Convert.ToString(dr["CAUSE"]),
                    PERMANENT_ACTION = Convert.ToString(dr["PERMANENT_ACTION"]),
                    SCHEDULE = Convert.ToString(dr["SCHEDULE"]),
                    EMP_NO = Convert.ToString(dr["EMP_NAME"]),
                    LINK_FILE = Convert.ToString(dr["LINK_FILE"]),
                    DEPTS = Convert.ToString(dr["DEPTS"]),
                    APPROVE_STATUS = Convert.ToString(dr["APPROVE_STATUS"]),
                });
            }
            return coverSheetlist;
        }
        //get search coverSheet
        public List<property_CoverSheet> GetSearchCoverSheet(property_Search Coversheet_Search)
        {

            connection();
            List<property_CoverSheet> coverSheetlist = new List<property_CoverSheet>();
            string sqlcmdShow = @"SELECT T_PROBLEM_COVERSHEET.NO, T_PROBLEM_COVERSHEET.DATE_PROBLEM, T_PROBLEM_COVERSHEET.MODEL, T_PROBLEM_COVERSHEET.PHASE_MT_MP, T_PROBLEM_COVERSHEET.RANK_JIG, T_PROBLEM_COVERSHEET.PROBLEM, T_PROBLEM_COVERSHEET.JIG_TOOL_CODE, T_PROBLEM_COVERSHEET.PROBLEM_TYPE, T_PROBLEM_COVERSHEET.CAUSE, T_PROBLEM_COVERSHEET.PERMANENT_ACTION, T_PROBLEM_COVERSHEET.SCHEDULE, T_PROBLEM_COVERSHEET.LINK_FILE,
            T_PROBLEM_PROBLEM_KEY.PROBLEM_KEY_NAME, T_PROBLEM_TOOL_KEY.TOOL_KEY_NAME, T_PROBLEM_USER.EMP_NAME, T_PROBLEM_USER.DEPTS, T_PROBLEM_CHECK.CHECK_STATUS, T_PROBLEM_APPROVE.APPROVE_STATUS
            from  T_PROBLEM_COVERSHEET 
            INNER JOIN T_PROBLEM_CHECK ON T_PROBLEM_COVERSHEET.NO = T_PROBLEM_CHECK.NO           
            INNER JOIN T_PROBLEM_PROBLEM_KEY ON T_PROBLEM_COVERSHEET.PROBLEM_KEY_NAME = T_PROBLEM_PROBLEM_KEY.PROBLEM_KEY_NAME
            INNER JOIN T_PROBLEM_TOOL_KEY ON T_PROBLEM_COVERSHEET.TOOL_KEY_NAME = T_PROBLEM_TOOL_KEY.TOOL_KEY_NAME
            INNER JOIN T_PROBLEM_USER ON T_PROBLEM_COVERSHEET.EMP_NO = T_PROBLEM_USER.EMP_NO
            INNER JOIN T_PROBLEM_APPROVE ON T_PROBLEM_COVERSHEET.NO  = T_PROBLEM_APPROVE.NO
            where " + Coversheet_Search.toolSelect + " like '%" + Coversheet_Search.listSearch + "%' order by NO ASC";
            OracleCommand cmd = new OracleCommand(sqlcmdShow, con);
            OracleDataAdapter OD = new OracleDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {

                con.Open();
                OD.Fill(dt);
                con.Close();
            }
            catch (Exception ex)
            {
                string back = ex.ToString();
                throw;
            }

            foreach (DataRow dr in dt.Rows)
            {
                coverSheetlist.Add(
                new property_CoverSheet
                {
                    NO = Convert.ToString(dr["NO"]),
                    DATE_PROBLEM = Convert.ToString(dr["DATE_PROBLEM"]),
                    MODEL = Convert.ToString(dr["MODEL"]),
                    PHASE_MT_MP = Convert.ToString(dr["PHASE_MT_MP"]),
                    RANK_JIG = Convert.ToString(dr["RANK_JIG"]),
                    PROBLEM = Convert.ToString(dr["PROBLEM"]),
                    JIG_TOOL_CODE = Convert.ToString(dr["JIG_TOOL_CODE"]),
                    PROBLEM_TYPE = Convert.ToString(dr["PROBLEM_TYPE"]),
                    CAUSE = Convert.ToString(dr["CAUSE"]),
                    PERMANENT_ACTION = Convert.ToString(dr["PERMANENT_ACTION"]),
                    SCHEDULE = Convert.ToString(dr["SCHEDULE"]),
                    EMP_NO = Convert.ToString(dr["EMP_NAME"]),
                    LINK_FILE = Convert.ToString(dr["LINK_FILE"]),
                    status_Approved = Convert.ToString(dr["CHECK_STATUS"]),
                    PLACE = Convert.ToString(dr["APPROVE_STATUS"])
                });
            }
            return coverSheetlist;
        }

        //Save Model Master Data
        public bool addModelMaster(property_Model Models)
        {
            string ConStr = "Data Source=PDE-DB-TEST/JIGDWHTEST;User ID=jigctrltest_ra;Password=jigctrltest_ra;Max Pool Size=500;Enlist=true";
            string sqlcmdInsertT_TOOL = @"INSERT INTO T_PROBLEM_MODEL(MODEL,MODEL_DATE) VALUES(:MODEL,:MODEL_DATE)";
            using (OracleConnection con = new OracleConnection(ConStr))
            {
                try
                {
                    OracleCommand cmdTool = new OracleCommand(sqlcmdInsertT_TOOL, con);
                    con.Open();
                    OracleTransaction tran = con.BeginTransaction();
                    cmdTool.Parameters.Add(":MODEL_CODE", OracleDbType.Varchar2, 255).Value = Models.MODEL;
                    cmdTool.Parameters.Add(":MODEL_DATE", OracleDbType.Varchar2, 255).Value = DateTime.Now.ToString("MM/dd/yyyy h:mm tt");

                    int i = cmdTool.ExecuteNonQuery();
                    tran.Commit();
                    con.Close();
                    if (i >= 1)
                        return true;
                    else
                        return false;

                }
                catch (Exception ex)
                {

                    string exShow = ex.ToString();
                    return false;
                }
            }
        }
        //Delete Model Master Data
        public bool DeleteModels(property_Model IdDel)
        {
            string ConStr = "Data Source=PDE-DB-TEST/JIGDWHTEST;User ID=jigctrltest_ra;Password=jigctrltest_ra;Max Pool Size=500;Enlist=true";
            string sqlcmd_DEL_T_TOOL = @"DELETE FROM T_PROBLEM_MODEL WHERE MODEL =  '" + IdDel.MODEL + "'";
            using (OracleConnection con = new OracleConnection(ConStr))
            {
                try
                {
                    OracleCommand cmdTool = new OracleCommand(sqlcmd_DEL_T_TOOL, con);
                    con.Open();
                    OracleTransaction tran = con.BeginTransaction();
                    cmdTool.Parameters.Add(":MODEL_CODE", OracleDbType.Varchar2, 255).Value = IdDel.MODEL;
                    cmdTool.ExecuteNonQuery();
                    tran.Commit();
                    con.Close();
                    return true;
                }
                catch (Exception ex)
                {
                    var ecShow = ex.ToString();
                    return false;
                }
            }
        }
        //get Model Master Data
        public List<property_Model> GetmodelforDel()
        {
            connection();
            List<property_Model> getModelDel = new List<property_Model>();
            string sqlcmdShow = @"SELECT MODEL from T_PROBLEM_MODEL order by MODEL ASC";
            OracleCommand cmd = new OracleCommand(sqlcmdShow, con);
            OracleDataAdapter OD = new OracleDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            OD.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                getModelDel.Add(
                new property_Model
                {
                    //TOOL_NAME = Convert.ToString(dr["TOOL_NAME"])
                    MODEL = Convert.ToString(dr["MODEL"]),

                });
            }
            return getModelDel;
        }

        //Save Toolkey Master data
        public bool addToolkeyMaster(property_ToolKey Toolkey)
        {
            string ConStr = "Data Source=PDE-DB-TEST/JIGDWHTEST;User ID=jigctrltest_ra;Password=jigctrltest_ra;Max Pool Size=500;Enlist=true";
            string sqlcmdShow = @"SELECT MAX(TOOL_KEY) from T_PROBLEM_TOOL_KEY";
            string sqlcmdInsertT_TOOL = @"INSERT INTO T_PROBLEM_TOOL_KEY(TOOL_KEY,TOOL_KEY_DATE,TOOL_KEY_NAME,TOOL_KEY_SEARCH) VALUES(:TOOL_KEY,:TOOL_KEY_DATE,:TOOL_KEY_NAME,:TOOL_KEY_SEARCH)";
            using (OracleConnection con = new OracleConnection(ConStr))
            {
                try
                {
                    OracleCommand cmdTool = new OracleCommand(sqlcmdInsertT_TOOL, con);
                    OracleCommand cmdToolCount = new OracleCommand(sqlcmdShow, con);
                    con.Open();
                    string output = cmdToolCount.ExecuteScalar().ToString();
                    int outputINT = Convert.ToInt32(output);
                    OracleTransaction tran = con.BeginTransaction();
                    cmdTool.Parameters.Add(":TOOL_KEY", OracleDbType.Varchar2, 255).Value = (outputINT + 1).ToString();
                    cmdTool.Parameters.Add(":TOOL_KEY_DATE", OracleDbType.Varchar2, 255).Value = DateTime.Now.ToString("MM/dd/yyyy h:mm tt");
                    cmdTool.Parameters.Add(":TOOL_KEY_NAME", OracleDbType.Varchar2, 255).Value = Toolkey.TOOL_KEY_NAME;
                    cmdTool.Parameters.Add(":TOOL_KEY_SEARCH", OracleDbType.Varchar2, 255).Value = Toolkey.TOOL_KEY_NAME;

                    int i = cmdTool.ExecuteNonQuery();
                    tran.Commit();
                    con.Close();
                    if (i >= 1)
                        return true;
                    else
                        return false;

                }
                catch (Exception ex)
                {

                    string exShow = ex.ToString();
                    return false;
                }
            }
        }

        //get Model Master Data
        public List<property_ToolKey> GetToolforDel()
        {
            connection();
            List<property_ToolKey> getToolDel = new List<property_ToolKey>();
            string sqlcmdShow = @"SELECT TOOL_KEY_NAME, TOOL_KEY from T_PROBLEM_TOOL_KEY order by TOOL_KEY ASC";
            OracleCommand cmd = new OracleCommand(sqlcmdShow, con);
            OracleDataAdapter OD = new OracleDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            OD.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                getToolDel.Add(
                new property_ToolKey
                {
                    //TOOL_NAME = Convert.ToString(dr["TOOL_NAME"])
                    TOOL_KEY_NAME = Convert.ToString(dr["TOOL_KEY_NAME"]),
                    TOOL_KEY = Convert.ToString(dr["TOOL_KEY"])

                });
            }
            return getToolDel;
        }
        //Delete ToolKey Master Data
        public bool DeleteToolKey(property_ToolKey IdDel)
        {
            string ConStr = "Data Source=PDE-DB-TEST/JIGDWHTEST;User ID=jigctrltest_ra;Password=jigctrltest_ra;Max Pool Size=500;Enlist=true";
            string sqlcmd_DEL_T_TOOL = @"DELETE FROM T_PROBLEM_TOOL_KEY WHERE TOOL_KEY =  '" + IdDel.TOOL_KEY + "'";
            using (OracleConnection con = new OracleConnection(ConStr))
            {
                try
                {
                    OracleCommand cmdTool = new OracleCommand(sqlcmd_DEL_T_TOOL, con);
                    con.Open();
                    OracleTransaction tran = con.BeginTransaction();
                    cmdTool.Parameters.Add(":TOOL_KEY", OracleDbType.Varchar2, 255).Value = IdDel.TOOL_KEY;
                    cmdTool.ExecuteNonQuery();
                    tran.Commit();
                    con.Close();
                    return true;
                }
                catch (Exception ex)
                {
                    var ecShow = ex.ToString();
                    return false;
                }
            }
        }

        //Save Problemkey Master data
        public bool addProblemkeyMaster(property_ProblemKey Problemkey)
        {
            string ConStr = "Data Source=PDE-DB-TEST/JIGDWHTEST;User ID=jigctrltest_ra;Password=jigctrltest_ra;Max Pool Size=500;Enlist=true";
            string sqlcmdShow = @"SELECT MAX(PROBLEM_KEY) from T_PROBLEM_PROBLEM_KEY"; ;
            string sqlcmdInsertT_TOOL = @"INSERT INTO T_PROBLEM_PROBLEM_KEY(PROBLEM_KEY,PROBLEM_KEY_DATE,PROBLEM_KEY_NAME,PROBLEM_KEY_SEARCH) VALUES(:PROBLEM_KEY,:PROBLEM_KEY_DATE,:PROBLEM_KEY_NAME,:PROBLEM_KEY_SEARCH)";
            using (OracleConnection con = new OracleConnection(ConStr))
            {
                try
                {
                    OracleCommand cmdTool = new OracleCommand(sqlcmdInsertT_TOOL, con);
                    OracleCommand cmdToolCount = new OracleCommand(sqlcmdShow, con);
                    con.Open();
                    string output = cmdToolCount.ExecuteScalar().ToString();
                    int outputINT = Convert.ToInt32(output);
                    OracleTransaction tran = con.BeginTransaction();
                    cmdTool.Parameters.Add(":PROBLEM_KEY", OracleDbType.Varchar2).Value = (outputINT + 1).ToString(); ;
                    cmdTool.Parameters.Add(":PROBLEM_KEY_DATE", OracleDbType.Varchar2).Value = DateTime.Now.ToString("MM/dd/yyyy h:mm tt");
                    cmdTool.Parameters.Add(":PROBLEM_KEY_NAME", OracleDbType.Varchar2).Value = Problemkey.PROBLEM_KEY_NAME;
                    cmdTool.Parameters.Add(":PROBLEM_KEY_SEARCH", OracleDbType.Varchar2).Value = Problemkey.PROBLEM_KEY_NAME;
                    int i = cmdTool.ExecuteNonQuery();
                    tran.Commit();
                    con.Close();
                    if (i >= 1)
                        return true;
                    else
                        return false;
                }
                catch (Exception ex)
                {
                    string exShow = ex.ToString();
                    return false;
                }
            }
        }

        //get ProblemKey Master Data
        public List<property_ProblemKey> GetProblemforDel()
        {
            connection();
            List<property_ProblemKey> getProblemDel = new List<property_ProblemKey>();
            string sqlcmdShow = @"SELECT PROBLEM_KEY, PROBLEM_KEY_NAME from T_PROBLEM_PROBLEM_KEY order by PROBLEM_KEY ASC";
            OracleCommand cmd = new OracleCommand(sqlcmdShow, con);
            OracleDataAdapter OD = new OracleDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            OD.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                getProblemDel.Add(
                new property_ProblemKey
                {
                    //TOOL_NAME = Convert.ToString(dr["TOOL_NAME"])
                    PROBLEM_KEY = Convert.ToString(dr["PROBLEM_KEY"]),
                    PROBLEM_KEY_NAME = Convert.ToString(dr["PROBLEM_KEY_NAME"])

                });
            }
            return getProblemDel;
        }

        //Delete ProblemKey Master Data        
        public bool DeleteProblemKey(property_ProblemKey IdDel)
        {
            string ConStr = "Data Source=PDE-DB-TEST/JIGDWHTEST;User ID=jigctrltest_ra;Password=jigctrltest_ra;Max Pool Size=500;Enlist=true";
            string sqlcmd_DEL_T_TOOL = @"DELETE FROM T_PROBLEM_PROBLEM_KEY WHERE PROBLEM_KEY =  '" + IdDel.PROBLEM_KEY + "'";
            using (OracleConnection con = new OracleConnection(ConStr))
            {
                try
                {
                    OracleCommand cmdTool = new OracleCommand(sqlcmd_DEL_T_TOOL, con);
                    con.Open();
                    OracleTransaction tran = con.BeginTransaction();
                    cmdTool.Parameters.Add(":PROBLEM_KEY", OracleDbType.Varchar2, 255).Value = IdDel.PROBLEM_KEY;
                    cmdTool.ExecuteNonQuery();
                    tran.Commit();
                    con.Close();
                    return true;
                }
                catch (Exception ex)
                {
                    var ecShow = ex.ToString();
                    return false;
                }
            }
        }

        //get GetUserAll
        public List<property_User> GetUserAll()
        {
            connection();
            List<property_User> getUser = new List<property_User>();
            string sqlcmdShow = @"SELECT * from T_PROBLEM_USER";
            OracleCommand cmd = new OracleCommand(sqlcmdShow, con);
            OracleDataAdapter OD = new OracleDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            OD.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                getUser.Add(
                new property_User
                {
                    //TOOL_NAME = Convert.ToString(dr["TOOL_NAME"])
                    EMP_NO = Convert.ToString(dr["EMP_NO"]),
                    EMP_NAME = Convert.ToString(dr["EMP_NAME"]),
                    EMP_LASTNAME = Convert.ToString(dr["EMP_LASTNAME"]),
                    POSITION_ID = Convert.ToString(dr["POSITION_ID"]),
                    EMAIL = Convert.ToString(dr["EMAIL"]),
                    DEPTS = Convert.ToString(dr["DEPTS"]),
                    PASSWORD = Convert.ToString(dr["PASSWORD"]),
                    STATUS = Convert.ToString(dr["STATUS"])
                });
            }
            return getUser;
        }

        //get GetUserApproved
        public List<property_User> GetUserSearch(property_UserSearch userSearch)
        {
            connection();
            List<property_User> getUser = new List<property_User>();
            string sqlcmdShow = @"SELECT * from T_PROBLEM_USER where " + userSearch.tpyeSearch + " like '%" + userSearch.searchData + "%'";
            OracleCommand cmd = new OracleCommand(sqlcmdShow, con);
            OracleDataAdapter OD = new OracleDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            OD.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                getUser.Add(
                new property_User
                {
                    EMP_NO = Convert.ToString(dr["EMP_NO"]),
                    EMP_NAME = Convert.ToString(dr["EMP_NAME"]),
                    EMP_LASTNAME = Convert.ToString(dr["EMP_LASTNAME"]),
                    POSITION_ID = Convert.ToString(dr["POSITION_ID"]),
                    EMAIL = Convert.ToString(dr["EMAIL"]),
                    DEPTS = Convert.ToString(dr["DEPTS"]),
                    PASSWORD = Convert.ToString(dr["PASSWORD"]),
                    STATUS = Convert.ToString(dr["STATUS"])

                });
            }
            return getUser;
        }

        //update status approved  user       
        public bool UpdateStatusApproved(property_User data)
        {
            string ConStr = "Data Source=PDE-DB-TEST/JIGDWHTEST;User ID=jigctrltest_ra;Password=jigctrltest_ra;Max Pool Size=500;Enlist=true";
            string sqlcmd_Update_T_TOOL = @"UPDATE T_PROBLEM_USER SET STATUS=:STATUS WHERE EMP_NO='" + data.EMP_NO + "'";
            using (OracleConnection con = new OracleConnection(ConStr))
            {
                try
                {
                    OracleCommand cmdTool = new OracleCommand(sqlcmd_Update_T_TOOL, con);
                    con.Open();
                    OracleTransaction tran = con.BeginTransaction();
                    // cmdTool.Parameters.Add(":TOOL_CODE", OracleDbType.Varchar2, 255).Value = data.TOOL_CODE;
                    cmdTool.Parameters.Add(":STATUS", OracleDbType.Varchar2, 255).Value = "ONLINE";
                    cmdTool.ExecuteNonQuery();
                    tran.Commit();
                    con.Close();
                    return true;

                }
                catch (Exception ex)
                {
                    var ecShow = ex.ToString();
                    return false;
                }
            }
        }

        //get GetUserChangeBrand
        public List<property_User> GetUserChangeBrand()
        {
            connection();
            List<property_User> getUser = new List<property_User>();
            string sqlcmdShow = @"SELECT EMP_NO from T_PROBLEM_USER";
            OracleCommand cmd = new OracleCommand(sqlcmdShow, con);
            OracleDataAdapter OD = new OracleDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            OD.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                getUser.Add(
                new property_User
                {
                    //TOOL_NAME = Convert.ToString(dr["TOOL_NAME"])
                    EMP_NO = Convert.ToString(dr["EMP_NO"]),

                });
            }
            return getUser;
        }

        //update brand        
        public bool UpdateBrand(property_User data)
        {
            string ConStr = "Data Source=PDE-DB-TEST/JIGDWHTEST;User ID=jigctrltest_ra;Password=jigctrltest_ra;Max Pool Size=500;Enlist=true";
            string sqlcmd_Update_T_TOOL = @"UPDATE T_PROBLEM_USER SET POSITION_ID=:POSITION_ID WHERE EMP_NO='" + data.EMP_NO + "'";
            using (OracleConnection con = new OracleConnection(ConStr))
            {
                try
                {
                    OracleCommand cmdTool = new OracleCommand(sqlcmd_Update_T_TOOL, con);
                    con.Open();
                    OracleTransaction tran = con.BeginTransaction();
                    // cmdTool.Parameters.Add(":TOOL_CODE", OracleDbType.Varchar2, 255).Value = data.TOOL_CODE;
                    cmdTool.Parameters.Add(":POSITION_ID", OracleDbType.Varchar2, 255).Value = data.POSITION_ID;
                    cmdTool.ExecuteNonQuery();
                    tran.Commit();
                    con.Close();
                    return true;

                }
                catch (Exception ex)
                {
                    var ecShow = ex.ToString();
                    return false;
                }
            }
        }

        // get J3 J4 Check
        public List<property_User> GetJ3J4()
        {
            connection();
            List<property_User> getJ3J4 = new List<property_User>();
            string sqlcmdShow = @"SELECT EMP_NAME from T_PROBLEM_USER where POSITION_ID = 'J3' or POSITION_ID = 'J4'";
            OracleCommand cmd = new OracleCommand(sqlcmdShow, con);
            OracleDataAdapter OD = new OracleDataAdapter(cmd);
            DataTable dt = new DataTable();
            con.Open();
            OD.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                getJ3J4.Add(
                new property_User
                {
                    //TOOL_NAME = Convert.ToString(dr["TOOL_NAME"])
                    EMP_NAME = Convert.ToString(dr["EMP_NAME"])
                });
            }
            return getJ3J4;
        }

        //get Name & LastName to TextBox
        public List<property_User> GetUserUpdateNameLastName(property_User empGet)
        {
            connection();
            List<property_User> updateUser = new List<property_User>();
            string sqlcmdShow = @"SELECT EMP_NAME, EMP_LASTNAME from T_PROBLEM_USER where EMP_NO = '" + empGet.EMP_NO + "'";
            OracleCommand cmd = new OracleCommand(sqlcmdShow, con);
            OracleDataAdapter OD = new OracleDataAdapter(cmd);
            DataTable dt = new DataTable();
            con.Open();
            OD.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                updateUser.Add(
                new property_User
                {
                    //TOOL_NAME = Convert.ToString(dr["TOOL_NAME"])
                    EMP_NAME = Convert.ToString(dr["EMP_NAME"]),
                    EMP_LASTNAME = Convert.ToString(dr["EMP_LASTNAME"])
                });
            }
            return updateUser;
        }

        //update User  Name & LastName 
        public bool UpdateUserNameLasetname(property_User data)
        {
            string ConStr = "Data Source=PDE-DB-TEST/JIGDWHTEST;User ID=jigctrltest_ra;Password=jigctrltest_ra;Max Pool Size=500;Enlist=true";
            string sqlcmd_Update_T_TOOL = @"UPDATE T_PROBLEM_USER SET EMP_NAME=:EMP_NAME, EMP_LASTNAME=:EMP_LASTNAME WHERE EMP_NO='" + data.EMP_NO + "'";
            using (OracleConnection con = new OracleConnection(ConStr))
            {
                try
                {
                    OracleCommand cmdTool = new OracleCommand(sqlcmd_Update_T_TOOL, con);
                    con.Open();
                    OracleTransaction tran = con.BeginTransaction();
                    // cmdTool.Parameters.Add(":TOOL_CODE", OracleDbType.Varchar2, 255).Value = data.TOOL_CODE;
                    cmdTool.Parameters.Add(":EMP_NAME", OracleDbType.Varchar2, 255).Value = data.EMP_NAME;
                    cmdTool.Parameters.Add(":EMP_LASTNAME", OracleDbType.Varchar2, 255).Value = data.EMP_LASTNAME;
                    cmdTool.ExecuteNonQuery();
                    tran.Commit();
                    con.Close();
                    return true;

                }
                catch (Exception ex)
                {
                    var ecShow = ex.ToString();
                    return false;
                }
            }
        }
        //update User Depts.
        public bool UpdateUserDepts(property_User data)
        {
            string ConStr = "Data Source=PDE-DB-TEST/JIGDWHTEST;User ID=jigctrltest_ra;Password=jigctrltest_ra;Max Pool Size=500;Enlist=true";
            string sqlcmd_Update_T_TOOL = @"UPDATE T_PROBLEM_USER SET DEPTS=:DEPTS WHERE EMP_NO ='" + data.EMP_NO + "'";
            using (OracleConnection con = new OracleConnection(ConStr))
            {
                try
                {
                    OracleCommand cmdTool = new OracleCommand(sqlcmd_Update_T_TOOL, con);
                    con.Open();
                    OracleTransaction tran = con.BeginTransaction();
                    // cmdTool.Parameters.Add(":TOOL_CODE", OracleDbType.Varchar2, 255).Value = data.TOOL_CODE;
                    cmdTool.Parameters.Add(":DEPTS", OracleDbType.Varchar2, 255).Value = data.DEPTS;
                    cmdTool.ExecuteNonQuery();
                    tran.Commit();
                    con.Close();
                    return true;

                }
                catch (Exception ex)
                {
                    var ecShow = ex.ToString();
                    return false;
                }
            }
        }
        //get email upadte 
        public List<property_User> GetEmailUpdate(property_User data)
        {
            connection();
            List<property_User> updateEmail = new List<property_User>();
            string sqlcmdShow = @"SELECT EMAIL from T_PROBLEM_USER where EMP_NO = '" + data.EMP_NO + "'";
            OracleCommand cmd = new OracleCommand(sqlcmdShow, con);
            OracleDataAdapter OD = new OracleDataAdapter(cmd);
            DataTable dt = new DataTable();
            con.Open();
            OD.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                updateEmail.Add(
                new property_User
                {
                    //TOOL_NAME = Convert.ToString(dr["TOOL_NAME"])
                    EMAIL = Convert.ToString(dr["EMAIL"])
                });
            }
            return updateEmail;
        }

        //get Piture link
        public List<property_User> GetLinkBack(property_User data)
        {
            connection();
            List<property_User> updatePiture = new List<property_User>();
            string sqlcmdShow = @"SELECT PITURE from T_PROBLEM_USER where EMP_NO = '" + data.EMP_NO + "'";
            OracleCommand cmd = new OracleCommand(sqlcmdShow, con);
            OracleDataAdapter OD = new OracleDataAdapter(cmd);
            DataTable dt = new DataTable();
            con.Open();
            OD.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                updatePiture.Add(
                new property_User
                {
                    //TOOL_NAME = Convert.ToString(dr["TOOL_NAME"])
                    PITURE = Convert.ToString(dr["PITURE"])
                });
            }
            return updatePiture;
        }
        //update User Email.
        public bool UpdateUserEmail(property_User data)
        {
            string ConStr = "Data Source=PDE-DB-TEST/JIGDWHTEST;User ID=jigctrltest_ra;Password=jigctrltest_ra;Max Pool Size=500;Enlist=true";
            string sqlcmd_Update_T_TOOL = @"UPDATE T_PROBLEM_USER SET EMAIL=:EMAIL WHERE EMP_NO ='" + data.EMP_NO + "'";
            using (OracleConnection con = new OracleConnection(ConStr))
            {
                try
                {
                    OracleCommand cmdTool = new OracleCommand(sqlcmd_Update_T_TOOL, con);
                    con.Open();
                    OracleTransaction tran = con.BeginTransaction();
                    // cmdTool.Parameters.Add(":TOOL_CODE", OracleDbType.Varchar2, 255).Value = data.TOOL_CODE;
                    cmdTool.Parameters.Add(":EMAIL", OracleDbType.Varchar2, 255).Value = data.EMAIL;
                    cmdTool.ExecuteNonQuery();
                    tran.Commit();
                    con.Close();
                    return true;
                }
                catch (Exception ex)
                {
                    var ecShow = ex.ToString();
                    return false;
                }
            }
        }

        //update User Piture.     
        public bool UpdateUserPiture(property_User data)
        {
            string ConStr = "Data Source=PDE-DB-TEST/JIGDWHTEST;User ID=jigctrltest_ra;Password=jigctrltest_ra;Max Pool Size=500;Enlist=true";
            string sqlcmd_Update_T_TOOL = @"UPDATE T_PROBLEM_USER SET PITURE=:PITURE WHERE EMP_NO ='" + data.EMP_NO + "'";
            using (OracleConnection con = new OracleConnection(ConStr))
            {
                try
                {
                    OracleCommand cmdTool = new OracleCommand(sqlcmd_Update_T_TOOL, con);
                    con.Open();
                    OracleTransaction tran = con.BeginTransaction();
                    // cmdTool.Parameters.Add(":TOOL_CODE", OracleDbType.Varchar2, 255).Value = data.TOOL_CODE;
                    cmdTool.Parameters.Add(":PITURE", OracleDbType.Varchar2, 255).Value = data.PITURE;
                    cmdTool.ExecuteNonQuery();
                    tran.Commit();
                    con.Close();
                    return true;
                }
                catch (Exception ex)
                {
                    var ecShow = ex.ToString();
                    return false;
                }
            }
        }

        //get coverSheet to excle 
        public List<property_CoverSheet> getTemplateExcel(property_CoverSheet data)
        {

            connection();
            List<property_CoverSheet> coverSheetlist = new List<property_CoverSheet>();
            string sqlcmdShow = @"SELECT T_PROBLEM_COVERSHEET.NO, T_PROBLEM_COVERSHEET.DATE_PROBLEM, T_PROBLEM_COVERSHEET.MODEL, T_PROBLEM_COVERSHEET.PHASE_MT_MP, T_PROBLEM_COVERSHEET.RANK_JIG, T_PROBLEM_COVERSHEET.PROBLEM, T_PROBLEM_COVERSHEET.JIG_TOOL_CODE, T_PROBLEM_COVERSHEET.PROBLEM_TYPE, T_PROBLEM_COVERSHEET.CAUSE, T_PROBLEM_COVERSHEET.PERMANENT_ACTION, T_PROBLEM_COVERSHEET.SCHEDULE, T_PROBLEM_COVERSHEET.LINK_FILE,
            T_PROBLEM_COVERSHEET.JIG_TOOL_NAME, T_PROBLEM_COVERSHEET.PLACE, T_PROBLEM_PROBLEM_KEY.PROBLEM_KEY_NAME, T_PROBLEM_TOOL_KEY.TOOL_KEY_NAME, T_PROBLEM_USER.EMP_NAME, T_PROBLEM_USER.DEPTS, T_PROBLEM_CHECK.CHECK_STATUS, T_PROBLEM_CHECK.CHECK_NAME, T_PROBLEM_CHECK.CHECK_DATE, T_PROBLEM_APPROVE.APPROVE_NAME, T_PROBLEM_APPROVE.APPROVE_STATUS, T_PROBLEM_APPROVE.APPROVE_DATE 
            from  T_PROBLEM_COVERSHEET 
            INNER JOIN T_PROBLEM_CHECK ON T_PROBLEM_COVERSHEET.NO = T_PROBLEM_CHECK.NO
            INNER JOIN T_PROBLEM_APPROVE ON T_PROBLEM_COVERSHEET.NO = T_PROBLEM_APPROVE.NO     
            INNER JOIN T_PROBLEM_PROBLEM_KEY ON T_PROBLEM_COVERSHEET.PROBLEM_KEY_NAME = T_PROBLEM_PROBLEM_KEY.PROBLEM_KEY_NAME
            INNER JOIN T_PROBLEM_TOOL_KEY ON T_PROBLEM_COVERSHEET.TOOL_KEY_NAME = T_PROBLEM_TOOL_KEY.TOOL_KEY_NAME
            INNER JOIN T_PROBLEM_USER ON T_PROBLEM_COVERSHEET.EMP_NO = T_PROBLEM_USER.EMP_NO
            where T_PROBLEM_COVERSHEET.NO ='" + data.NO + "'";
            OracleCommand cmd = new OracleCommand(sqlcmdShow, con);
            OracleDataAdapter OD = new OracleDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            OD.Fill(dt);
            con.Close();
            try
            {

                foreach (DataRow dr in dt.Rows)
                {
                    coverSheetlist.Add(
                    new property_CoverSheet
                    {

                        NO = Convert.ToString(dr["NO"]),
                        JIG_TOOL_CODE = Convert.ToString(dr["JIG_TOOL_CODE"]),
                        JIG_TOOL_NAME = Convert.ToString(dr["JIG_TOOL_NAME"]),
                        MODEL = Convert.ToString(dr["MODEL"]),
                        DATE_PROBLEM = Convert.ToString(dr["DATE_PROBLEM"]),
                        PLACE = Convert.ToString(dr["PLACE"]),
                        PROBLEM = Convert.ToString(dr["PROBLEM"]),
                        TOOL_KEY_NAME = Convert.ToString(dr["TOOL_KEY_NAME"]),
                        PROBLEM_KEY_NAME = Convert.ToString(dr["PROBLEM_KEY_NAME"]),
                        EMP_NO = Convert.ToString(dr["EMP_NAME"]),
                        CAUSE = Convert.ToString(dr["CAUSE"]),
                        CHECK_STATUS = Convert.ToString(dr["CHECK_STATUS"]),
                        CHECK_NAME = Convert.ToString(dr["CHECK_NAME"]),
                        CHECK_DATE = Convert.ToString(dr["CHECK_DATE"]),
                        APPROVE_NAME = Convert.ToString(dr["APPROVE_NAME"]),
                        APPROVE_STATUS = Convert.ToString(dr["APPROVE_STATUS"]),
                        APPROVE_DATE = Convert.ToString(dr["APPROVE_DATE"]),
                        LINK_FILE = Convert.ToString(dr["LINK_FILE"])
                    });
                }
            }
            catch (Exception ex)
            {
                string data12 = ex.ToString();
                throw;
            }

            return coverSheetlist;
        }

        //update coverSheet 
        public bool updateCover(property_User data)
        {
            string ConStr = "Data Source=PDE-DB-TEST/JIGDWHTEST;User ID=jigctrltest_ra;Password=jigctrltest_ra;Max Pool Size=500;Enlist=true";
            string sqlcmd_Update_T_TOOL = @"UPDATE T_PROBLEM_USER SET DEPTS=:DEPTS WHERE EMP_NO ='" + data.EMP_NO + "'";
            using (OracleConnection con = new OracleConnection(ConStr))
            {
                try
                {
                    OracleCommand cmdTool = new OracleCommand(sqlcmd_Update_T_TOOL, con);
                    con.Open();
                    OracleTransaction tran = con.BeginTransaction();
                    // cmdTool.Parameters.Add(":TOOL_CODE", OracleDbType.Varchar2, 255).Value = data.TOOL_CODE;
                    cmdTool.Parameters.Add(":DEPTS", OracleDbType.Varchar2, 255).Value = data.DEPTS;
                    cmdTool.ExecuteNonQuery();
                    tran.Commit();
                    con.Close();
                    return true;

                }
                catch (Exception ex)
                {
                    var ecShow = ex.ToString();
                    return false;
                }
            }
        }

        //update data form file excel
        public void updateDBFormfile(property_CoverSheet data)
        {
            string ConStr = "Data Source=PDE-DB-TEST/JIGDWHTEST;User ID=jigctrltest_ra;Password=jigctrltest_ra;Max Pool Size=500;Enlist=true";
            string sqlcmd_Update_T_TOOL = @"UPDATE T_PROBLEM_COVERSHEET SET JIG_TOOL_NAME=:JIG_TOOL_NAME, JIG_TOOL_CODE=:JIG_TOOL_CODE, MODEL=:MODEL, DATE_PROBLEM=:DATE_PROBLEM, 
            PLACE=:PLACE, PROBLEM=:PROBLEM, PERMANENT_ACTION=:PERMANENT_ACTION, LINK_FILE=:LINK_FILE, TEMP_ACTION=:TEMP_ACTION, CAUSE=:CAUSE  WHERE NO='" + data.NO + "'";
            string sqlcmdInsertApproved = @"update T_PROBLEM_CHECK SET CHECK_STATUS=:CHECK_STATUS where NO='" + data.NO + "'";
            using (OracleConnection con = new OracleConnection(ConStr))
            {
                try
                {
                    OracleCommand cmdTool = new OracleCommand(sqlcmd_Update_T_TOOL, con);
                    OracleCommand cmdApproved = new OracleCommand(sqlcmdInsertApproved, con);
                    con.Open();
                    OracleTransaction tran = con.BeginTransaction();
                    cmdTool.Parameters.Add(":JIG_TOOL_NAME", OracleDbType.Varchar2).Value = data.JIG_TOOL_NAME;
                    cmdTool.Parameters.Add(":JIG_TOOL_CODE", OracleDbType.Varchar2).Value = data.JIG_TOOL_CODE;
                    cmdTool.Parameters.Add(":MODEL", OracleDbType.Varchar2).Value = data.MODEL;
                    cmdTool.Parameters.Add(":DATE_PROBLEM", OracleDbType.Varchar2).Value = data.DATE_PROBLEM;
                    cmdTool.Parameters.Add(":PLACE", OracleDbType.Varchar2).Value = data.PLACE;
                    cmdTool.Parameters.Add(":PROBLEM", OracleDbType.Varchar2).Value = data.PROBLEM;
                    cmdTool.Parameters.Add(":PERMANENT_ACTION", OracleDbType.Varchar2).Value = data.PERMANENT_ACTION;
                    cmdTool.Parameters.Add(":LINK_FILE", OracleDbType.Varchar2).Value = data.LINK_FILE;
                    cmdTool.Parameters.Add(":TEMP_ACTION", OracleDbType.Varchar2).Value = data.TEMP_ACTION;
                    cmdTool.Parameters.Add(":CAUSE", OracleDbType.Varchar2).Value = data.CAUSE;
                    cmdApproved.Parameters.Add(":CHECK_STATUS", OracleDbType.Varchar2).Value = "Wait Check";
                    cmdTool.ExecuteNonQuery();
                    cmdApproved.ExecuteNonQuery();
                    tran.Commit();
                    con.Close();
                }
                catch (Exception ex)
                {
                    var ecShow = ex.ToString();
                }
            }
        }

        //select link file for delete 
        public List<property_CoverSheet> getLinkforDelete(string no)
        {

            connection();
            List<property_CoverSheet> coverSheetlist = new List<property_CoverSheet>();
            string sqlcmdShow = @"SELECT LINK_FILE from T_PROBLEM_COVERSHEET where NO ='" + no + "'";
            OracleCommand cmd = new OracleCommand(sqlcmdShow, con);
            OracleDataAdapter OD = new OracleDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            OD.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                coverSheetlist.Add(
                new property_CoverSheet
                {
                    LINK_FILE = Convert.ToString(dr["LINK_FILE"])
                });
            }
            return coverSheetlist;
        }
        //login 
        public bool Login(property_User data)
        {
            try
            {
                connection();
                con.Open();
                string sqlcmdShow = @"SELECT COUNT(*) FROM T_PROBLEM_USER WHERE EMP_NO='" + data.EMP_NO + "' AND PASSWORD='" + data.PASSWORD + "' AND STATUS='ONLINE'";
                OracleCommand cmd = new OracleCommand(sqlcmdShow, con);
                string output = cmd.ExecuteScalar().ToString();
                if (output == "1")
                {

                    return true;

                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {

                string exShow = ex.ToString();
                return false;
            }
        }
        //get User Data
        public List<property_User> GetUserData(property_User getId)
        {
            connection();
            List<property_User> UserListGetId = new List<property_User>();
            string sqlcmdShow = @"SELECT  EMP_NO, EMP_NAME, EMP_LASTNAME, POSITION_ID, PASSWORD, STATUS, PITURE, DEPTS, EMAIL  FROM T_PROBLEM_USER WHERE EMP_NO='" + getId.EMP_NO + "'";
            OracleCommand cmd = new OracleCommand(sqlcmdShow, con);
            OracleDataAdapter OD = new OracleDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            OD.Fill(dt);
            con.Close();
            foreach (DataRow dr in dt.Rows)
            {
                UserListGetId.Add(
                new property_User
                {
                    EMP_NO = Convert.ToString(dr["EMP_NO"]),
                    EMP_NAME = Convert.ToString(dr["EMP_NAME"]),
                    EMP_LASTNAME = Convert.ToString(dr["EMP_LASTNAME"]),
                    POSITION_ID = Convert.ToString(dr["POSITION_ID"]),
                    PASSWORD = Convert.ToString(dr["PASSWORD"]),
                    STATUS = Convert.ToString(dr["STATUS"]),
                    PITURE = Convert.ToString(dr["PITURE"]),
                    DEPTS = Convert.ToString(dr["DEPTS"]),
                    EMAIL = Convert.ToString(dr["EMAIL"]),
                    LinkDitinationPiture = Convert.ToString(dr["PITURE"])
                }
                 );
            }

            return UserListGetId;
        }
        //singup User
        public bool singupUser(property_User data)
        {
            string ConStr = "Data Source=PDE-DB-TEST/JIGDWHTEST;User ID=jigctrltest_ra;Password=jigctrltest_ra;Max Pool Size=500;Enlist=true";
            string sqlcmdInsertT_TOOL = @"INSERT INTO T_PROBLEM_USER(EMP_NO, EMP_NAME, EMP_LASTNAME, POSITION_ID, EMAIL, PASSWORD, STATUS, DEPTS) VALUES(:EMP_NO, :EMP_NAME, :EMP_LASTNAME, :POSITION_ID, :EMAIL, :PASSWORD, :STATUS, :DEPTS)";
            using (OracleConnection con = new OracleConnection(ConStr))
            {
                try
                {
                    OracleCommand cmdTool = new OracleCommand(sqlcmdInsertT_TOOL, con);
                    con.Open();
                    OracleTransaction tran = con.BeginTransaction();
                    cmdTool.Parameters.Add(":EMP_NO", OracleDbType.Varchar2).Value = data.EMP_NO;
                    cmdTool.Parameters.Add(":EMP_NAME", OracleDbType.Varchar2).Value = data.EMP_NAME;
                    cmdTool.Parameters.Add(":EMP_LASTNAME", OracleDbType.Varchar2).Value = data.EMP_LASTNAME;
                    cmdTool.Parameters.Add(":POSITION_ID", OracleDbType.Varchar2).Value = data.POSITION_ID;
                    cmdTool.Parameters.Add(":EMAIL", OracleDbType.Varchar2).Value = data.EMAIL;
                    cmdTool.Parameters.Add(":PASSWORD", OracleDbType.Varchar2).Value = data.PASSWORD;
                    cmdTool.Parameters.Add(":STATUS", OracleDbType.Varchar2).Value = "OFFLINE";
                    cmdTool.Parameters.Add(":DEPTS", OracleDbType.Varchar2).Value = data.DEPTS;
                    int i = cmdTool.ExecuteNonQuery();
                    tran.Commit();
                    con.Close();
                    if (i >= 1)
                        return true;
                    else
                        return false;
                }
                catch (Exception ex)
                {
                    string exShow = ex.ToString();
                    return false;
                }
            }
        }

        public List<property_User> GetUserEmail(property_User getId)
        {
            connection();
            List<property_User> UserListGetId = new List<property_User>();
            string sqlcmdShow = @"SELECT  PASSWORD, EMAIL FROM T_PROBLEM_USER WHERE EMAIL='" + getId.EMAIL + "'";
            OracleCommand cmd = new OracleCommand(sqlcmdShow, con);
            OracleDataAdapter OD = new OracleDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            OD.Fill(dt);
            con.Close();
            foreach (DataRow dr in dt.Rows)
            {

                UserListGetId.Add(
                new property_User
                {
                    PASSWORD = Convert.ToString(dr["PASSWORD"]),
                    EMAIL = Convert.ToString(dr["EMAIL"])
                }
                 );
            }

            return UserListGetId;
        }

        //update name last name email 
        public bool UpdateUserNameLastNameEmail(property_User data)
        {
            string ConStr = "Data Source=PDE-DB-TEST/JIGDWHTEST;User ID=jigctrltest_ra;Password=jigctrltest_ra;Max Pool Size=500;Enlist=true";
            string sqlcmd_Update_T_TOOL = @"UPDATE T_PROBLEM_USER SET EMP_NAME=:EMP_NAME, EMP_LASTNAME=:EMPLASTNAME, EMAIL=:EMAIL WHERE EMP_NO ='" + data.EMP_NO + "'";
            using (OracleConnection con = new OracleConnection(ConStr))
            {
                try
                {
                    OracleCommand cmdTool = new OracleCommand(sqlcmd_Update_T_TOOL, con);
                    con.Open();
                    OracleTransaction tran = con.BeginTransaction();
                    // cmdTool.Parameters.Add(":TOOL_CODE", OracleDbType.Varchar2, 255).Value = data.TOOL_CODE;
                    cmdTool.Parameters.Add(":EMP_NAME", OracleDbType.Varchar2).Value = data.EMP_NAME;
                    cmdTool.Parameters.Add(":EMP_LASTNAME", OracleDbType.Varchar2).Value = data.EMP_LASTNAME;
                    cmdTool.Parameters.Add(":EMAIL", OracleDbType.Varchar2).Value = data.EMAIL;
                    cmdTool.ExecuteNonQuery();
                    tran.Commit();
                    con.Close();
                    return true;
                }
                catch (Exception ex)
                {
                    var ecShow = ex.ToString();
                    return false;
                }
            }
        }

        //Change Password        
        public bool updatePasswordData(property_User data)
        {
            string ConStr = "Data Source=PDE-DB-TEST/JIGDWHTEST;User ID=jigctrltest_ra;Password=jigctrltest_ra;Max Pool Size=500;Enlist=true";
            string sqlcmd_Update_T_TOOL = @"UPDATE T_PROBLEM_USER SET PASSWORD=:PASSWORD WHERE EMP_NO ='" + data.EMP_NO + "'";
            using (OracleConnection con = new OracleConnection(ConStr))
            {
                try
                {
                    OracleCommand cmdTool = new OracleCommand(sqlcmd_Update_T_TOOL, con);
                    con.Open();
                    OracleTransaction tran = con.BeginTransaction();
                    // cmdTool.Parameters.Add(":TOOL_CODE", OracleDbType.Varchar2, 255).Value = data.TOOL_CODE;
                    cmdTool.Parameters.Add(":PASSWORD", OracleDbType.Varchar2).Value = data.PASSWORD;
                    cmdTool.ExecuteNonQuery();
                    tran.Commit();
                    con.Close();
                    return true;
                }
                catch (Exception ex)
                {
                    var ecShow = ex.ToString();
                    return false;
                }
            }
        }

        //get check by j3
        public List<property_Check> GetCountCheckbyJ3(string data)
        {
            connection();
            List<property_Check> checkList = new List<property_Check>();
            string sqlcmdShow = @"SELECT NO, CHECK_NAME, CHECK_STATUS, CHECK_DATE, CHECK_TIME  from  T_PROBLEM_CHECK where CHECK_STATUS = 'Wait Check' and CHECK_NAME = '" + data + "' ";
            OracleCommand cmd = new OracleCommand(sqlcmdShow, con);
            OracleDataAdapter OD = new OracleDataAdapter(cmd);
            DataTable dt = new DataTable();
            con.Open();
            OD.Fill(dt);
            con.Close();
            foreach (DataRow dr in dt.Rows)
            {
                checkList.Add(
                new property_Check
                {
                    NO = Convert.ToString(dr["NO"]),
                    CHECK_NAME = Convert.ToString(dr["CHECK_NAME"]),
                    CHECK_STATUS = Convert.ToString(dr["CHECK_STATUS"]),
                    CHECK_DATE = Convert.ToString(dr["CHECK_DATE"]),
                    CHECK_TIME = Convert.ToString(dr["CHECK_TIME"])
                });
            }
            return checkList;
        }

        //get problem Check
        public List<property_CheckReturn> Get_J3Check(string date)
        {
            connection();
            List<property_CheckReturn> checkList = new List<property_CheckReturn>();
            string sqlcmdShow = @"select T_PROBLEM_CHECK.NO, T_PROBLEM_CHECK.CHECK_DATE, T_PROBLEM_CHECK.CHECK_STATUS, T_PROBLEM_COVERSHEET.PROBLEM, 
            T_PROBLEM_COVERSHEET.JIG_TOOL_CODE, T_PROBLEM_COVERSHEET.LINK_FILE, T_PROBLEM_USER.EMP_NAME from T_PROBLEM_CHECK 
            inner join T_PROBLEM_COVERSHEET on T_PROBLEM_CHECK.NO = T_PROBLEM_COVERSHEET.NO 
            inner join T_PROBLEM_USER on T_PROBLEM_COVERSHEET.EMP_NO =  T_PROBLEM_USER.EMP_NO where CHECK_NAME = '" + date + "' and CHECK_STATUS ='Wait Check'";
            OracleCommand cmd = new OracleCommand(sqlcmdShow, con);
            OracleDataAdapter OD = new OracleDataAdapter(cmd);
            DataTable dt = new DataTable();
            con.Open();
            OD.Fill(dt);
            con.Close();
            foreach (DataRow dr in dt.Rows)
            {
                checkList.Add(
                new property_CheckReturn
                {
                    NO = Convert.ToString(dr["NO"]),
                    EMP_NAME = Convert.ToString(dr["EMP_NAME"]),
                    CHECK_STATUS = Convert.ToString(dr["CHECK_STATUS"]),
                    CHECK_DATE = Convert.ToString(dr["CHECK_DATE"]),
                    PROBLEM = Convert.ToString(dr["PROBLEM"]),
                    JIG_TOOL_CODE = Convert.ToString(dr["JIG_TOOL_CODE"]),
                    LINK_FILE = Convert.ToString(dr["LINK_FILE"])

                });
            }
            return checkList;
        }
        // get problem J3 Check All       
        public List<property_CheckReturn> Get_J3CheckAll()
        {
            connection();
            List<property_CheckReturn> checkList = new List<property_CheckReturn>();
            string sqlcmdShow = @"select T_PROBLEM_CHECK.NO, T_PROBLEM_CHECK.CHECK_DATE, T_PROBLEM_CHECK.CHECK_STATUS, T_PROBLEM_COVERSHEET.PROBLEM, 
            T_PROBLEM_COVERSHEET.JIG_TOOL_CODE, T_PROBLEM_COVERSHEET.LINK_FILE, T_PROBLEM_USER.EMP_NAME from T_PROBLEM_CHECK 
            inner join T_PROBLEM_COVERSHEET on T_PROBLEM_CHECK.NO = T_PROBLEM_COVERSHEET.NO 
            inner join T_PROBLEM_USER on T_PROBLEM_COVERSHEET.EMP_NO =  T_PROBLEM_USER.EMP_NO where CHECK_STATUS ='Wait Check'";
            OracleCommand cmd = new OracleCommand(sqlcmdShow, con);
            OracleDataAdapter OD = new OracleDataAdapter(cmd);
            DataTable dt = new DataTable();
            con.Open();
            OD.Fill(dt);
            con.Close();
            foreach (DataRow dr in dt.Rows)
            {
                checkList.Add(
                new property_CheckReturn
                {
                    NO = Convert.ToString(dr["NO"]),
                    EMP_NAME = Convert.ToString(dr["EMP_NAME"]),
                    CHECK_STATUS = Convert.ToString(dr["CHECK_STATUS"]),
                    CHECK_DATE = Convert.ToString(dr["CHECK_DATE"]),
                    PROBLEM = Convert.ToString(dr["PROBLEM"]),
                    JIG_TOOL_CODE = Convert.ToString(dr["JIG_TOOL_CODE"]),
                    LINK_FILE = Convert.ToString(dr["LINK_FILE"])

                });
            }
            return checkList;
        }

        //update Check OK
        public bool UpdatetoCheckOk(property_CheckReturn data)
        {
            string ConStr = "Data Source=PDE-DB-TEST/JIGDWHTEST;User ID=jigctrltest_ra;Password=jigctrltest_ra;Max Pool Size=500;Enlist=true";
            string sqlcmd_Update_T_TOOL = @"UPDATE T_PROBLEM_CHECK SET CHECK_NAME=:EMP_NAME,CHECK_DATE=:CHECK_DATE, CHECK_STATUS=:CHECK_STATUS, CHECK_TIME=:CHECK_TIME WHERE NO ='" + data.NO + "'";
            using (OracleConnection con = new OracleConnection(ConStr))
            {
                try
                {
                    OracleCommand cmdTool = new OracleCommand(sqlcmd_Update_T_TOOL, con);
                    con.Open();
                    OracleTransaction tran = con.BeginTransaction();
                    // cmdTool.Parameters.Add(":TOOL_CODE", OracleDbType.Varchar2, 255).Value = data.TOOL_CODE;
                    cmdTool.Parameters.Add(":CHECK_NAME", OracleDbType.Varchar2).Value = data.EMP_NAME;
                    cmdTool.Parameters.Add(":CHECK_DATE", OracleDbType.Varchar2).Value = DateTime.Now.ToString("dd/MM/yyyy");
                    cmdTool.Parameters.Add(":CHECK_STATUS", OracleDbType.Varchar2).Value = "J3 Check OK";
                    cmdTool.Parameters.Add(":CHECK_TIME", OracleDbType.Varchar2).Value = DateTime.Now.ToString("HH:mm:ss");
                    cmdTool.ExecuteNonQuery();
                    tran.Commit();
                    con.Close();
                    return true;
                }
                catch (Exception ex)
                {
                    var ecShow = ex.ToString();
                    return false;
                }
            }
        }

        //update Check Reject
        public bool UpdatetoCheckReject(property_CheckReturn data)
        {
            string ConStr = "Data Source=PDE-DB-TEST/JIGDWHTEST;User ID=jigctrltest_ra;Password=jigctrltest_ra;Max Pool Size=500;Enlist=true";
            string sqlcmd_Update_T_TOOL = @"UPDATE T_PROBLEM_CHECK SET CHECK_NAME=:EMP_NAME,CHECK_DATE=:CHECK_DATE, CHECK_STATUS=:CHECK_STATUS, CHECK_TIME=:CHECK_TIME WHERE NO ='" + data.NO + "'";
            using (OracleConnection con = new OracleConnection(ConStr))
            {
                try
                {
                    OracleCommand cmdTool = new OracleCommand(sqlcmd_Update_T_TOOL, con);
                    con.Open();
                    OracleTransaction tran = con.BeginTransaction();
                    // cmdTool.Parameters.Add(":TOOL_CODE", OracleDbType.Varchar2, 255).Value = data.TOOL_CODE;
                    cmdTool.Parameters.Add(":CHECK_NAME", OracleDbType.Varchar2).Value = data.EMP_NAME;
                    cmdTool.Parameters.Add(":CHECK_DATE", OracleDbType.Varchar2).Value = DateTime.Now.ToString("dd/MM/yyyy");
                    cmdTool.Parameters.Add(":CHECK_STATUS", OracleDbType.Varchar2).Value = "J3 Reject";
                    cmdTool.Parameters.Add(":CHECK_TIME", OracleDbType.Varchar2).Value = DateTime.Now.ToString("HH:mm:ss");
                    cmdTool.ExecuteNonQuery();
                    tran.Commit();
                    con.Close();
                    return true;
                }
                catch (Exception ex)
                {
                    var ecShow = ex.ToString();
                    return false;
                }
            }
        }
        // get problem Admin RejectAll       
        public List<property_CoverSheet> Get_RejectAll()
        {
            connection();
            // INNER JOIN T_PROBLEM_APPROVE ON T_PROBLEM_COVERSHEET.NO = T_PROBLEM_APPROVE.NO
            List<property_CoverSheet> coverSheetlist = new List<property_CoverSheet>();
            string sqlcmdShow = @"SELECT T_PROBLEM_COVERSHEET.NO, T_PROBLEM_COVERSHEET.DATE_PROBLEM, T_PROBLEM_COVERSHEET.MODEL, T_PROBLEM_COVERSHEET.PHASE_MT_MP, T_PROBLEM_COVERSHEET.RANK_JIG, T_PROBLEM_COVERSHEET.PROBLEM, T_PROBLEM_COVERSHEET.JIG_TOOL_CODE, T_PROBLEM_COVERSHEET.PROBLEM_TYPE, T_PROBLEM_COVERSHEET.CAUSE, T_PROBLEM_COVERSHEET.PERMANENT_ACTION, T_PROBLEM_COVERSHEET.SCHEDULE, T_PROBLEM_COVERSHEET.LINK_FILE,
            T_PROBLEM_PROBLEM_KEY.PROBLEM_KEY_NAME, T_PROBLEM_TOOL_KEY.TOOL_KEY_NAME, T_PROBLEM_USER.EMP_NAME, T_PROBLEM_USER.DEPTS, T_PROBLEM_CHECK.CHECK_STATUS  
            from  T_PROBLEM_COVERSHEET 
            INNER JOIN T_PROBLEM_CHECK ON T_PROBLEM_COVERSHEET.NO = T_PROBLEM_CHECK.NO           
            INNER JOIN T_PROBLEM_PROBLEM_KEY ON T_PROBLEM_COVERSHEET.PROBLEM_KEY_NAME = T_PROBLEM_PROBLEM_KEY.PROBLEM_KEY_NAME
            INNER JOIN T_PROBLEM_TOOL_KEY ON T_PROBLEM_COVERSHEET.TOOL_KEY_NAME = T_PROBLEM_TOOL_KEY.TOOL_KEY_NAME
            INNER JOIN T_PROBLEM_USER ON T_PROBLEM_COVERSHEET.EMP_NO = T_PROBLEM_USER.EMP_NO 
            where CHECK_STATUS = 'J3 Reject'  order by NO ASC";
            OracleCommand cmd = new OracleCommand(sqlcmdShow, con);
            OracleDataAdapter OD = new OracleDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            OD.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                coverSheetlist.Add(
                new property_CoverSheet
                {
                    //TOOL_NAME = Convert.ToString(dr["TOOL_NAME"])
                    NO = Convert.ToString(dr["NO"]),
                    DATE_PROBLEM = Convert.ToString(dr["DATE_PROBLEM"]),
                    MODEL = Convert.ToString(dr["MODEL"]),
                    PHASE_MT_MP = Convert.ToString(dr["PHASE_MT_MP"]),
                    RANK_JIG = Convert.ToString(dr["RANK_JIG"]),
                    PROBLEM = Convert.ToString(dr["PROBLEM"]),
                    JIG_TOOL_CODE = Convert.ToString(dr["JIG_TOOL_CODE"]),
                    PROBLEM_TYPE = Convert.ToString(dr["PROBLEM_TYPE"]),
                    CAUSE = Convert.ToString(dr["CAUSE"]),
                    PERMANENT_ACTION = Convert.ToString(dr["PERMANENT_ACTION"]),
                    SCHEDULE = Convert.ToString(dr["SCHEDULE"]),
                    EMP_NO = Convert.ToString(dr["EMP_NAME"]),
                    LINK_FILE = Convert.ToString(dr["LINK_FILE"]),
                    status_Approved = Convert.ToString(dr["CHECK_STATUS"])
                });
            }
            return coverSheetlist;
        }

        // get problem  Reject by Name      
        public List<property_CoverSheet> Get_RejecByName(string name)
        {
            connection();
            // INNER JOIN T_PROBLEM_APPROVE ON T_PROBLEM_COVERSHEET.NO = T_PROBLEM_APPROVE.NO
            List<property_CoverSheet> coverSheetlist = new List<property_CoverSheet>();
            string sqlcmdShow = @"SELECT T_PROBLEM_COVERSHEET.NO, T_PROBLEM_COVERSHEET.DATE_PROBLEM, T_PROBLEM_COVERSHEET.MODEL, T_PROBLEM_COVERSHEET.PHASE_MT_MP, T_PROBLEM_COVERSHEET.RANK_JIG, T_PROBLEM_COVERSHEET.PROBLEM, T_PROBLEM_COVERSHEET.JIG_TOOL_CODE, T_PROBLEM_COVERSHEET.PROBLEM_TYPE, T_PROBLEM_COVERSHEET.CAUSE, T_PROBLEM_COVERSHEET.PERMANENT_ACTION, T_PROBLEM_COVERSHEET.SCHEDULE, T_PROBLEM_COVERSHEET.LINK_FILE,
            T_PROBLEM_PROBLEM_KEY.PROBLEM_KEY_NAME, T_PROBLEM_TOOL_KEY.TOOL_KEY_NAME, T_PROBLEM_USER.EMP_NAME, T_PROBLEM_USER.DEPTS, T_PROBLEM_CHECK.CHECK_STATUS  
            from  T_PROBLEM_COVERSHEET 
            INNER JOIN T_PROBLEM_CHECK ON T_PROBLEM_COVERSHEET.NO = T_PROBLEM_CHECK.NO           
            INNER JOIN T_PROBLEM_PROBLEM_KEY ON T_PROBLEM_COVERSHEET.PROBLEM_KEY_NAME = T_PROBLEM_PROBLEM_KEY.PROBLEM_KEY_NAME
            INNER JOIN T_PROBLEM_TOOL_KEY ON T_PROBLEM_COVERSHEET.TOOL_KEY_NAME = T_PROBLEM_TOOL_KEY.TOOL_KEY_NAME
            INNER JOIN T_PROBLEM_USER ON T_PROBLEM_COVERSHEET.EMP_NO = T_PROBLEM_USER.EMP_NO 
            where CHECK_STATUS = 'J3 Reject' and EMP_NAME = '" + name + "' order by NO ASC";
            OracleCommand cmd = new OracleCommand(sqlcmdShow, con);
            OracleDataAdapter OD = new OracleDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            OD.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                coverSheetlist.Add(
                new property_CoverSheet
                {
                    //TOOL_NAME = Convert.ToString(dr["TOOL_NAME"])
                    NO = Convert.ToString(dr["NO"]),
                    DATE_PROBLEM = Convert.ToString(dr["DATE_PROBLEM"]),
                    MODEL = Convert.ToString(dr["MODEL"]),
                    PHASE_MT_MP = Convert.ToString(dr["PHASE_MT_MP"]),
                    RANK_JIG = Convert.ToString(dr["RANK_JIG"]),
                    PROBLEM = Convert.ToString(dr["PROBLEM"]),
                    JIG_TOOL_CODE = Convert.ToString(dr["JIG_TOOL_CODE"]),
                    PROBLEM_TYPE = Convert.ToString(dr["PROBLEM_TYPE"]),
                    CAUSE = Convert.ToString(dr["CAUSE"]),
                    PERMANENT_ACTION = Convert.ToString(dr["PERMANENT_ACTION"]),
                    SCHEDULE = Convert.ToString(dr["SCHEDULE"]),
                    EMP_NO = Convert.ToString(dr["EMP_NAME"]),
                    LINK_FILE = Convert.ToString(dr["LINK_FILE"]),
                    status_Approved = Convert.ToString(dr["CHECK_STATUS"])
                });
            }
            return coverSheetlist;
        }

        //get count Reject 
        public List<property_Check> GetCountReject(string data)
        {
            connection();
            List<property_Check> checkList = new List<property_Check>();
            string sqlcmdShow = @"select T_PROBLEM_CHECK.CHECK_STATUS from T_PROBLEM_CHECK
            inner JOIN T_PROBLEM_COVERSHEET on  T_PROBLEM_CHECK.NO = T_PROBLEM_COVERSHEET.NO
            inner join T_PROBLEM_USER on T_PROBLEM_COVERSHEET.EMP_NO = T_PROBLEM_USER.EMP_NO
            where EMP_NAME = '" + data + "' and CHECK_STATUS = 'J3 Reject'";
            OracleCommand cmd = new OracleCommand(sqlcmdShow, con);
            OracleDataAdapter OD = new OracleDataAdapter(cmd);
            DataTable dt = new DataTable();
            con.Open();
            OD.Fill(dt);
            con.Close();
            foreach (DataRow dr in dt.Rows)
            {
                checkList.Add(
                new property_Check
                {
                    CHECK_STATUS = Convert.ToString(dr["CHECK_STATUS"])
                });
            }
            return checkList;
        }

        //get Approved by M1 GetClose
        public List<property_CoverSheet> GetClose()
        {

            connection();
            List<property_CoverSheet> coverSheetlist = new List<property_CoverSheet>();
            string sqlcmdShow = @"SELECT T_PROBLEM_COVERSHEET.NO, T_PROBLEM_COVERSHEET.DATE_PROBLEM, T_PROBLEM_COVERSHEET.MODEL, T_PROBLEM_COVERSHEET.PHASE_MT_MP, T_PROBLEM_COVERSHEET.RANK_JIG, T_PROBLEM_COVERSHEET.PROBLEM, T_PROBLEM_COVERSHEET.JIG_TOOL_CODE, T_PROBLEM_COVERSHEET.PROBLEM_TYPE, T_PROBLEM_COVERSHEET.CAUSE, T_PROBLEM_COVERSHEET.PERMANENT_ACTION, T_PROBLEM_COVERSHEET.SCHEDULE, T_PROBLEM_COVERSHEET.LINK_FILE,
            T_PROBLEM_PROBLEM_KEY.PROBLEM_KEY_NAME, T_PROBLEM_TOOL_KEY.TOOL_KEY_NAME, T_PROBLEM_USER.EMP_NAME, T_PROBLEM_USER.DEPTS, T_PROBLEM_CHECK.CHECK_STATUS, T_PROBLEM_APPROVE.APPROVE_STATUS
            from  T_PROBLEM_COVERSHEET 
            INNER JOIN T_PROBLEM_CHECK ON T_PROBLEM_COVERSHEET.NO = T_PROBLEM_CHECK.NO           
            INNER JOIN T_PROBLEM_PROBLEM_KEY ON T_PROBLEM_COVERSHEET.PROBLEM_KEY_NAME = T_PROBLEM_PROBLEM_KEY.PROBLEM_KEY_NAME
            INNER JOIN T_PROBLEM_TOOL_KEY ON T_PROBLEM_COVERSHEET.TOOL_KEY_NAME = T_PROBLEM_TOOL_KEY.TOOL_KEY_NAME
            INNER JOIN T_PROBLEM_USER ON T_PROBLEM_COVERSHEET.EMP_NO = T_PROBLEM_USER.EMP_NO
            INNER JOIN T_PROBLEM_APPROVE ON T_PROBLEM_COVERSHEET.NO  = T_PROBLEM_APPROVE.NO
            where  APPROVE_STATUS = 'Wait Close' and  CHECK_STATUS = 'J3 Check OK' 
            order by NO ASC";
            OracleCommand cmd = new OracleCommand(sqlcmdShow, con);
            OracleDataAdapter OD = new OracleDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            OD.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                coverSheetlist.Add(
                new property_CoverSheet
                {
                    NO = Convert.ToString(dr["NO"]),
                    DATE_PROBLEM = Convert.ToString(dr["DATE_PROBLEM"]),
                    MODEL = Convert.ToString(dr["MODEL"]),
                    PHASE_MT_MP = Convert.ToString(dr["PHASE_MT_MP"]),
                    RANK_JIG = Convert.ToString(dr["RANK_JIG"]),
                    PROBLEM = Convert.ToString(dr["PROBLEM"]),
                    JIG_TOOL_CODE = Convert.ToString(dr["JIG_TOOL_CODE"]),
                    PROBLEM_TYPE = Convert.ToString(dr["PROBLEM_TYPE"]),
                    CAUSE = Convert.ToString(dr["CAUSE"]),
                    PERMANENT_ACTION = Convert.ToString(dr["PERMANENT_ACTION"]),
                    SCHEDULE = Convert.ToString(dr["SCHEDULE"]),
                    EMP_NO = Convert.ToString(dr["EMP_NAME"]),
                    LINK_FILE = Convert.ToString(dr["LINK_FILE"]),
                    status_Approved = Convert.ToString(dr["CHECK_STATUS"]),
                    PLACE = Convert.ToString(dr["APPROVE_STATUS"])
                });
            }
            return coverSheetlist;
        }
        //update close OK
        public bool UpdatetoCloseOk(property_Approved data)
        {
            string ConStr = "Data Source=PDE-DB-TEST/JIGDWHTEST;User ID=jigctrltest_ra;Password=jigctrltest_ra;Max Pool Size=500;Enlist=true";
            string sqlcmd_Update_Close = @"UPDATE T_PROBLEM_APPROVE SET APPROVE_NAME=:APPROVE_NAME,APPROVE_DATE=:APPROVE_DATE, APPROVE_STATUS=:APPROVE_STATUS, APPROVE_TIME=:APPROVE_TIME WHERE NO ='" + data.NO + "'";
            using (OracleConnection con = new OracleConnection(ConStr))
            {
                try
                {
                    OracleCommand cmdClose = new OracleCommand(sqlcmd_Update_Close, con);
                    con.Open();
                    OracleTransaction tran = con.BeginTransaction();
                    // cmdTool.Parameters.Add(":TOOL_CODE", OracleDbType.Varchar2, 255).Value = data.TOOL_CODE;
                    cmdClose.Parameters.Add(":APPROVE_NAME", OracleDbType.Varchar2).Value = data.APPROVE_NAME;
                    cmdClose.Parameters.Add(":APPROVE_DATE", OracleDbType.Varchar2).Value = DateTime.Now.ToString("dd/MM/yyyy");
                    cmdClose.Parameters.Add(":APPROVE_STATUS", OracleDbType.Varchar2).Value = "Close Problem";
                    cmdClose.Parameters.Add(":APPROVE_TIME", OracleDbType.Varchar2).Value = DateTime.Now.ToString("HH:mm:ss");
                    cmdClose.ExecuteNonQuery();
                    tran.Commit();
                    con.Close();
                    return true;
                }
                catch (Exception ex)
                {
                    var ecShow = ex.ToString();
                    return false;
                }
            }
        }

        //get status Problem Close Problem 
        public List<property_Approved> getStatusClose()
        {
            connection();
            List<property_Approved> checkList = new List<property_Approved>();
            string sqlcmdShow = @"select T_PROBLEM_APPROVE.APPROVE_STATUS,  T_PROBLEM_APPROVE.APPROVE_DATE from T_PROBLEM_APPROVE
            inner JOIN T_PROBLEM_COVERSHEET on  T_PROBLEM_APPROVE.NO = T_PROBLEM_COVERSHEET.NO          
            where APPROVE_STATUS = 'Close Problem'";
            OracleCommand cmd = new OracleCommand(sqlcmdShow, con);
            OracleDataAdapter OD = new OracleDataAdapter(cmd);
            DataTable dt = new DataTable();
            con.Open();
            OD.Fill(dt);
            con.Close();
            foreach (DataRow dr in dt.Rows)
            {
                checkList.Add(
                new property_Approved
                {
                    APPROVE_STATUS = Convert.ToString(dr["APPROVE_STATUS"]),
                    APPROVE_DATE = Convert.ToString(dr["APPROVE_DATE"])
                });
            }
            return checkList;
        }

        //get status Problem Wait Close 
        public List<property_Approved> getStatusWaitclose()
        {
            connection();
            List<property_Approved> checkList = new List<property_Approved>();
            string sqlcmdShow = @"select T_PROBLEM_APPROVE.APPROVE_STATUS,  T_PROBLEM_APPROVE.APPROVE_DATE from T_PROBLEM_APPROVE
            inner JOIN T_PROBLEM_COVERSHEET on  T_PROBLEM_APPROVE.NO = T_PROBLEM_COVERSHEET.NO          
            where APPROVE_STATUS = 'Wait Close'";
            OracleCommand cmd = new OracleCommand(sqlcmdShow, con);
            OracleDataAdapter OD = new OracleDataAdapter(cmd);
            DataTable dt = new DataTable();
            con.Open();
            OD.Fill(dt);
            con.Close();
            foreach (DataRow dr in dt.Rows)
            {
                checkList.Add(
                new property_Approved
                {
                    APPROVE_STATUS = Convert.ToString(dr["APPROVE_STATUS"]),
                    APPROVE_DATE = Convert.ToString(dr["APPROVE_DATE"])
                });
            }
            return checkList;
        }

        //get count close per Mouth.
        public List<property_CountApprove> getCloseperMouth()
        {
            connection();
            List<property_CountApprove> checkList = new List<property_CountApprove>();
            string sqlcmdShow = @"select SUBSTR(APPROVE_DATE, 4, 2) AS Mouth, count(to_date(SUBSTR(APPROVE_DATE, 4, 10), 'MM-YYYY')) 
            AS BackResult from T_PROBLEM_APPROVE where APPROVE_STATUS ='Close Problem' group by SUBSTR(APPROVE_DATE, 4, 2) order by SUBSTR(APPROVE_DATE, 4, 2) ASC";
            OracleCommand cmd = new OracleCommand(sqlcmdShow, con);
            OracleDataAdapter OD = new OracleDataAdapter(cmd);
            DataTable dt = new DataTable();
            con.Open();
            OD.Fill(dt);
            con.Close();
            foreach (DataRow dr in dt.Rows)
            {
                checkList.Add(
                new property_CountApprove
                {
                    BACK_RESULT = Convert.ToString(dr["BackResult"]),
                    MOUNT_BACK = Convert.ToString(dr["Mouth"])
                });
            }
            return checkList;
        }

        //get count close per Mouth.
        public List<property_CountApprove> getCloseperMouthWait()
        {
            connection();
            List<property_CountApprove> checkList = new List<property_CountApprove>();
            string sqlcmdShow = @"select SUBSTR(APPROVE_DATE, 4, 2)AS Mouth, count(to_date(SUBSTR(APPROVE_DATE, 4, 10), 'MM-YYYY')) 
            AS BackResult from T_PROBLEM_APPROVE where APPROVE_STATUS ='Wait Close' group by SUBSTR(APPROVE_DATE, 4, 2) order by SUBSTR(APPROVE_DATE, 4, 2) ASC";
            OracleCommand cmd = new OracleCommand(sqlcmdShow, con);
            OracleDataAdapter OD = new OracleDataAdapter(cmd);
            DataTable dt = new DataTable();
            con.Open();
            OD.Fill(dt);
            con.Close();
            foreach (DataRow dr in dt.Rows)
            {
                checkList.Add(
                new property_CountApprove
                {
                    BACK_RESULT = Convert.ToString(dr["BackResult"]),
                    MOUNT_BACK = Convert.ToString(dr["Mouth"])
                });
            }
            return checkList;
        }

        //get jig tool count dynamic.       
        public List<property_CountApprove> getDataJigTool()
        {
            connection();
            List<property_CountApprove> checkList = new List<property_CountApprove>();
            string sqlcmdShow = @"select TOOL_KEY_NAME, count(TOOL_KEY_NAME)AS SumTool from T_PROBLEM_COVERSHEET GROUP BY TOOL_KEY_NAME";
            OracleCommand cmd = new OracleCommand(sqlcmdShow, con);
            OracleDataAdapter OD = new OracleDataAdapter(cmd);
            DataTable dt = new DataTable();
            con.Open();
            OD.Fill(dt);
            con.Close();
            foreach (DataRow dr in dt.Rows)
            {
                checkList.Add(
                new property_CountApprove
                {
                    BACK_RESULT = Convert.ToString(dr["SumTool"]),
                    MOUNT_BACK = Convert.ToString(dr["TOOL_KEY_NAME"])
                });
            }
            return checkList;
        }

        //get problem count dynamic.       
        public List<property_CountApprove> getDataProblem()
        {
            connection();
            List<property_CountApprove> checkList = new List<property_CountApprove>();
            string sqlcmdShow = @"select PROBLEM_KEY_NAME, count(PROBLEM_KEY_NAME)AS SumProblem from T_PROBLEM_COVERSHEET GROUP BY PROBLEM_KEY_NAME";
            OracleCommand cmd = new OracleCommand(sqlcmdShow, con);
            OracleDataAdapter OD = new OracleDataAdapter(cmd);
            DataTable dt = new DataTable();
            con.Open();
            OD.Fill(dt);
            con.Close();
            foreach (DataRow dr in dt.Rows)
            {
                checkList.Add(
                new property_CountApprove
                {
                    BACK_RESULT = Convert.ToString(dr["SumProblem"]),
                    MOUNT_BACK = Convert.ToString(dr["PROBLEM_KEY_NAME"])
                });
            }
            return checkList;
        }

        //get User approve.
        public List<property_User> getUserApprove(property_UserSearch userSearch)
        {
            connection();
            List<property_User> getUser = new List<property_User>();
            string sqlcmdShow = @"SELECT * from T_PROBLEM_USER where EMP_NO = " + userSearch.tpyeSearch + "";
            OracleCommand cmd = new OracleCommand(sqlcmdShow, con);
            OracleDataAdapter OD = new OracleDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            OD.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                getUser.Add(
                new property_User
                {
                    EMP_NO = Convert.ToString(dr["EMP_NO"]),
                    EMP_NAME = Convert.ToString(dr["EMP_NAME"]),
                    EMP_LASTNAME = Convert.ToString(dr["EMP_LASTNAME"]),
                    POSITION_ID = Convert.ToString(dr["POSITION_ID"]),
                    EMAIL = Convert.ToString(dr["EMAIL"]),
                    DEPTS = Convert.ToString(dr["DEPTS"]),
                    PASSWORD = Convert.ToString(dr["PASSWORD"]),
                    STATUS = Convert.ToString(dr["STATUS"])

                });
            }
            return getUser;
        }
    }

}

