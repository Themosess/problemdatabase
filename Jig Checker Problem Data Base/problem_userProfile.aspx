﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master_Page.Master" AutoEventWireup="true" CodeBehind="problem_userProfile.aspx.cs" Inherits="Jig_Checker_Problem_Data_Base.problem_userProfile" %>
<%@ OutputCache Duration="3600" Location="Server" VaryByParam="none" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    <!-- Main CSS-->
    <link href="Content/css/main.css" rel="stylesheet" />
    <!-- Font-icon css-->
    <link href="Content/css/font-awesome.min.css" rel="stylesheet" />
      <%-- Block Back Load--%>
    <script type="text/javascript">
        function noBack() {
            window.history.forward()
        }

        noBack();
        window.onload = noBack;
        window.onpageshow = function (evt) { if (evt.persisted) noBack() }
        window.onunload = function () { void (0) }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server"> 
            <div class="row user">
                <div class="col-md-12">
                    <div class="profile">
                        <div class="info">
                            <div class="user-img" id="imgUser"></div>
                            <h4 id="nameUser"></h4>
                            <p id="brandUser"></p>
                        </div>
                        <div class="cover-image"></div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="tile p-0">
                        <ul class="nav flex-column nav-tabs user-tabs">
                            <li class="nav-item"><a class="nav-link active" href="#user-timeline" data-toggle="tab">Profile</a></li>
                            <li class="nav-item"><a class="nav-link" href="#user-settings" data-toggle="tab">Settings</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="tab-content">
                        <div class="tab-pane active" id="user-timeline">
                            <div class="timeline-post">
                                <div class="post-media">                                    
                                        <div id="userImg"></div>
                                    <div class="content">
                                        <h5 id="nameuser"><a href="#"></a></h5>
                                        <p  id="dayTime" class="text-muted"><small>2 January at 9:30</small></p>
                                    </div>
                                </div>
                                <div class="post-content">
                                    <h4 id="name2"></h4>
                                    <hr />
                                    <h4 id="Brand2"></h4>
                                    <hr />
                                    <h4 id="Email2"></h4>
                                    <hr />
                                    <h4 id="Department2"></h4>
                                    <hr />
                                </div>                             
                            </div>
                        </div>
                       <%-- Edit Profile--%>
                        <div class="tab-pane fade" id="user-settings">
                            <div class="tile user-settings">
                                <h4 class="line-head">Settings</h4>
                                <div class="row mb-4">
                                    <div class="col-md-4">
                                        <label>First Name</label>
                                        <input class="form-control required" type="text" id="firstName" />
                                    </div>
                                    <div class="col-md-4">
                                        <label>Last Name</label>
                                        <input class="form-control required" type="text" id="lastName" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8 mb-4">
                                        <label>Email</label>
                                        <input class="form-control required" type="text" id="email" />
                                    </div>                                                                                               
                                </div>
                                <div class="row mb-10">
                                    <div class="col-md-12">
                                        <button class="btn btn-primary" type="button" id="savebtnChaneProfile"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save</button>
                                    </div>
                                </div>                             
                            </div>
                            <div class="tile user-settings">
                                <div class="clearfix"></div>
                                <div class="col-md-8 mb-4">
                                    <label>Change Piture</label>
                                    <input class="form-control required" type="file"  onchange="imgLoad(this, 'img');" />
                                </div>
                                  <div class="row mb-10">
                                    <div class="col-md-12">
                                        <button class="btn btn-primary" type="button" id="submitChangePiture"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save</button>
                                    </div>
                                </div>
                            </div>
                            <div class="tile user-settings">
                                <h4 class="line-head">Setting Password</h4>
                                <div class="clearfix"></div>
                                <div class="col-md-8 mb-4">
                                    <label>Change Password</label>
                                    <input class="form-control required" type="text" id="passwordChange"/>
                                </div>
                                <div class="row mb-10">
                                    <div class="col-md-12">
                                        <button class="btn btn-primary" type="button"  id="btnPasswordChange"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>   
    <script src="Content/js/jquery-3.2.1.min.js"></script>


     <%--Base 64--%>
    <script>
        var TempDataIMG;
        var imgProcess = {
            reduceResolutionPercent: 100,
            reduceResolution: function (val) { return ((val * this.reduceResolutionPercent) / 100); },
            checkFileExtension: function (element) {
                try {
                    var temp = element.value.split('.');
                    if (temp.length > 0)
                        if (element.accept.toUpperCase().indexOf(temp[temp.length - 1].toUpperCase()) !== -1)
                            return true;
                        else
                            return false;

                    else return false;
                } catch (ex) { return false; }
            },
            encodeImageFileAsURL: function (element) {
                //if (this.checkFileExtension(element)) {
                var delayInMilliseconds = 1000;
                var Defer = $.Deferred();
                var reader = new FileReader(); // load file
                reader.onloadend = function () {
                    //var img = new Image;
                    //img.onload = function () {
                    Defer.resolve(this);
                    TempDataIMG = reader.result;
                    setTimeout(function () {
                        $("#modalLoading").modal('toggle');                       
                    }, delayInMilliseconds);
                    //};
                    //img.src = reader.result;
                }
                reader.onerror = function () { Defer.resolve(null); };
                reader.readAsDataURL(element.files[0]);
                return Defer.promise();
                //} else {
                //    alert('àÅ×Í¡ä´éà©¾ÒÐ .png, .jpg, .jpeg');
                //}
            },
            imageToDataUri: function (img, width, height) {
                var canvas = document.createElement('canvas'), ctx = canvas.getContext('2d');
                canvas.width = width;
                canvas.height = height;
                ctx.drawImage(img, 0, 0, width, height);
                return canvas.toDataURL();
            }
        }

        function imgLoad(myFileImage, imgObjId) {          
            $("#modalLoading").modal({
                backdrop: 'static',
                keyboard: false,
            });
            $.when(imgProcess.encodeImageFileAsURL(myFileImage)).then(
                function (Data) {
                    var newDataUri = imgProcess.imageToDataUri(Data, imgProcess.reduceResolution(Data.width), imgProcess.reduceResolution(Data.height));
                    // Display('imgQuiz', newDataUri);                    
                });
        }
    </script>
     <%--getSession Data--%>
    <script>
        $(document).ready(function () {
            var EMP_NO = sessionStorage.getItem("SetEMP_NO");
            var EMP_NAME = sessionStorage.getItem("SetEMP_NAME");
            var EMP_LASTNAME = sessionStorage.getItem("SetEMP_LASTNAME");
            var POLICE = sessionStorage.getItem("SetPOSITION_ID");
            var DEPTS = sessionStorage.getItem("SetDEPTS");
            var PITURE = sessionStorage.getItem("SetPITURE");
            var EMAIL = sessionStorage.getItem("SetEMAIL");
            var PITURE_OLD = sessionStorage.getItem("SetLinkDitinationPiture");
            if (POLICE == null || POLICE != "ADMIN") {
                document.getElementById("masterData").style.display = "None";
            } 
            $("#nameUser").html("<h4>" + EMP_NAME + " " + EMP_LASTNAME + "</h4>");
            $("#brandUser").html("<p>" + POLICE + "</p>");
            $("#imgUser").html("<img class='user-img' src='" + PITURE + "' />");
            $("#userImg").html("<img src='" + PITURE + "'  height='48' width='48' />");
            $("#nameuser").html("<h5>" + "<a href='#'>" + EMP_NAME + " " + EMP_LASTNAME + "</a>" + "</h5>");
            $("#name2").html("<p style='color:#6c757d'>" + "NAME: " + "&nbsp; &nbsp;" + EMP_NAME + " " + EMP_LASTNAME + "</p>");
            $("#Brand2").html("<p style='color:#6c757d'>" + "BRAND: " + "&nbsp; &nbsp;" + POLICE + "</p>");
            $("#Email2").html("<p style='color:#6c757d'>" + "E-MAIL: " + "&nbsp; &nbsp;" + EMAIL + "</p>");
            $("#Department2").html("<p style='color:#6c757d'>" + "DEPT: " + "&nbsp; &nbsp;" + DEPTS + "</p>");
            
        });     
    </script>
    <script>
        var monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"];
        var theDate = new Date();
        $("#dayTime").html("<p  class='text-muted'>" + "<small>" + theDate.getDate() + " " + monthNames[theDate.getMonth()] + "</small>" + "</p>");
    </script>

  <%--  Seting Profile name last name email--%>
    <script>
        $('#savebtnChaneProfile').click(function (e) {
           var isValid = true;
           $('input[type="text"].required').each(function () {
               if ($.trim($(this).val()) == '') {
                   isValid = false;
                   $(this).css({
                       "border": "1px solid red",
                       "background": "#FFCECE",
                   });
               }
               else {
                   $(this).css({
                       "border": "",
                       "background": ""
                   });
               }
           });
            if (isValid == false) {
                e.preventDefault();
            }
            else {
                 updateUser();             
            }
        });
        function updateUser() {
            var EMP_NO = sessionStorage.getItem("SetEMP_NO");
            var Link_oldfile = sessionStorage.getItem("SetLinkDitinationPiture");
            var updateUserData = {};
            $.extend(updateUserData, {
                EMP_NO: EMP_NO,
                EMP_NAME: $("#firstName").val(),
                EMP_LASTNAME: $("#lastName").val(),
                POSITION_ID: "",
                EMAIL: $("#email").val(),
                PASSWORD: "",
                STATUS: "",
                PITURE: "",
                DEPTS: "",
                LinkDitinationPiture: ""
            });
            $.ajax({
                type: "POST",
                url: "problem_userProfile.aspx/updateNameEmail",
                data: "{updateUserData: " + JSON.stringify(updateUserData) + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "JSON",
                async: false,
                success: function (response) {
                    msg = response.d;
                    if (msg == "UPDATE_SUCCESS") {
                        swal({
                            title: "แจ้ง !",
                            text: "แก้ไข Name & Last Name สำเร็จ!",
                            type: "success",
                            dangerMode: true,
                            animation: true,
                        },
                         function () {
                             sessionStorage.clear();
                             location.href = "login.aspx";
                         });
                    }
                    else {
                        swal({
                            title: "เตือน !",
                            text: "ผิดพลาด !",
                            type: "error",
                            dangerMode: true,
                            animation: true,
                        });
                    }
                },
                error: function (response) {
                    alert(response);
                }
            });
        }
    </script>

    <%--Seting Piture Change--%>
    <script>
        $('#submitChangePiture').click(function (e) {
            var isValid = true;
            $('input[type="file"].required').each(function () {
                if ($.trim($(this).val()) == '') {
                    isValid = false;
                    $(this).css({
                        "border": "1px solid red",
                        "background": "#FFCECE",
                    });
                }
                else {
                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                }
            });
            if (isValid == false) {
                e.preventDefault();
            }
            else {
                updatePiture();
            }
        });
        function updatePiture() {
            var EMP_NO = sessionStorage.getItem("SetEMP_NO");
            var Link_oldfile = sessionStorage.getItem("SetLinkDitinationPiture");
            var TempDataIMGSend = String(TempDataIMG);
            var updateUserDataPiture = {};
            $.extend(updateUserDataPiture, {
                EMP_NO: EMP_NO,
                EMP_NAME:"",
                EMP_LASTNAME: "",
                POSITION_ID: "",
                EMAIL: "",
                PASSWORD: "",
                STATUS: "",
                PITURE: TempDataIMGSend,
                DEPTS: "",
                LinkDitinationPiture: Link_oldfile
            });
            $.ajax({
                type: "POST",
                url: "problem_userProfile.aspx/changePiture",
                data: "{updateUserDataPiture: " + JSON.stringify(updateUserDataPiture) + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "JSON",
                async: false,
                success: function (response) {
                    msg = response.d;
                    msg = $.parseJSON(msg);
                    if (msg == "UPDATE SUCCESS") {
                        swal({
                            title: "แจ้ง !",
                            text: "แก้ไข รูปโปรไฟล์ สำเร็จ!",
                            type: "success",
                            dangerMode: true,
                            animation: true,
                        },
                         function () {
                             sessionStorage.clear();
                             location.href = "login.aspx";
                         });
                    }
                    else {
                        swal({
                            title: "เตือน !",
                            text: "ผิดพลาด !",
                            type: "error",
                            dangerMode: true,
                            animation: true,
                        });
                    }
                },
                error: function (response) {
                    alert(response);
                }
            });
        }
    </script>

     <%--  Seting password--%>
    <script>
        $('#btnPasswordChange').click(function (e) {
           var isValid = true;
           $('#passwordChange').each(function () {
               if ($.trim($(this).val()) == '') {
                   isValid = false;
                   $(this).css({
                       "border": "1px solid red",
                       "background": "#FFCECE",
                   });
               }
               else {
                   $(this).css({
                       "border": "",
                       "background": ""
                   });
               }
           });
            if (isValid == false) {
                e.preventDefault();
            }
            else {
                 updatePassword();             
            }
        });
        function updatePassword() {
            var EMP_NO = sessionStorage.getItem("SetEMP_NO");        
            var updateUserPassword = {};
            $.extend(updateUserPassword, {
                EMP_NO: EMP_NO,
                EMP_NAME: "",
                EMP_LASTNAME: "",
                POSITION_ID: "",
                EMAIL: "",
                PASSWORD: $("#passwordChange").val(),
                STATUS: "",
                PITURE: "",
                DEPTS: "",
                LinkDitinationPiture: ""
            });
            $.ajax({
                type: "POST",
                url: "problem_userProfile.aspx/updatePassword",
                data: "{updateUserPassword: " + JSON.stringify(updateUserPassword) + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "JSON",
                async: false,
                success: function (response) {
                    msg = response.d;
                    if (msg == "UPDATE_SUCCESS") {
                        swal({
                            title: "แจ้ง !",
                            text: "แก้ไข Password สำเร็จ!",
                            type: "success",
                            dangerMode: true,
                            animation: true,
                        },
                         function () {
                             sessionStorage.clear();
                             location.href = "login.aspx";
                         });
                    }
                    else {
                        swal({
                            title: "เตือน !",
                            text: "ผิดพลาด !",
                            type: "error",
                            dangerMode: true,
                            animation: true,
                        });
                    }
                },
                error: function (response) {
                    alert(response);
                }
            });
        }
    </script>

</asp:Content>
