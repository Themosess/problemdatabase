﻿using Jig_Checker_Problem_Data_Base.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Jig_Checker_Problem_Data_Base
{
    public partial class problem_userProfile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        
        //Updatename lastname 
        [WebMethod]
        public static string updateNameEmail(property_User updateUserData)
        {
            ManageDatabase VT = new ManageDatabase();
            //update TEXT//
            try
            {
                bool dataBack  = VT.UpdateUserNameLastNameEmail(updateUserData);
                if (dataBack == true)
                {
                    var JsonBack = "UPDATE_SUCCESS";                   
                    return JsonBack;
                }
                else {
                    var JsonBack = "UPDATE_DATA_FAILED";                   
                    return JsonBack;
                }
              

            }
            catch (Exception)
            {
                var JsonBack = "UPDATE_DATA_FAILED";
                var data = JsonConvert.SerializeObject(JsonBack, Formatting.Indented);
                return data;

            }
        }

        //update Piture
        [WebMethod]
        public static string changePiture(property_User updateUserDataPiture)
        {
            ManageDatabase VT = new ManageDatabase();
            String[] arrBase64PDF = updateUserDataPiture.PITURE.Split(',');
            string time = "_" + DateTime.Now.ToString("ss");
            var filenamePDF = updateUserDataPiture.EMP_NO;
            var mapPDF = HttpContext.Current.Server.MapPath("~/Content/Piture_User");
            File.WriteAllBytes(mapPDF + "/" + filenamePDF + time + ".JPG", Convert.FromBase64String(arrBase64PDF[1]));
            var DeleteOldFile = HttpContext.Current.Server.MapPath(updateUserDataPiture.LinkDitinationPiture);
            if (DeleteOldFile != "") {
                File.Delete(DeleteOldFile);
            }          
            updateUserDataPiture.PITURE = "/Content/Piture_User/" + filenamePDF + time + ".JPG";
            //Add TEXT//
            try
            {
                VT.UpdateUserPiture(updateUserDataPiture);
                var JsonBack = "UPDATE SUCCESS";
                var data = JsonConvert.SerializeObject(JsonBack, Formatting.Indented);
                return data;

            }
            catch (Exception)
            {
                var JsonBack = "UPDATE DATA FAILED";
                var data = JsonConvert.SerializeObject(JsonBack, Formatting.Indented);
                return data;

            }
        }

        //change Password
        [WebMethod]
        public static string updatePassword(property_User updateUserPassword)
        {
            ManageDatabase VT = new ManageDatabase();
            //update TEXT//
            try
            {
                bool dataBack = VT.updatePasswordData(updateUserPassword);
                if (dataBack == true)
                {
                    var JsonBack = "UPDATE_SUCCESS";
                    return JsonBack;
                }
                else
                {
                    var JsonBack = "UPDATE_DATA_FAILED";
                    return JsonBack;
                }


            }
            catch (Exception)
            {
                var JsonBack = "UPDATE_DATA_FAILED";
                var data = JsonConvert.SerializeObject(JsonBack, Formatting.Indented);
                return data;

            }
        }
    }
}