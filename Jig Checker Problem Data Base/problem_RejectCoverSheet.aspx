﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master_Page.Master" AutoEventWireup="true" CodeBehind="problem_RejectCoverSheet.aspx.cs" Inherits="Jig_Checker_Problem_Data_Base.problem_RejectCoverSheet" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <link href="Content/css/main.css" rel="stylesheet" />
    <link href="Content/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" />
    <link href="Content/bootstrap-sweetalert-master/dist/animate.css" rel="stylesheet" />
    <link href="Content/bootstrap-sweetalert-master/dist/sweetalert.css" rel="stylesheet" />
        <%-- Block Back Load--%>
    <script type="text/javascript">
        function noBack() {
            window.history.forward()
        }

        noBack();
        window.onload = noBack;
        window.onpageshow = function (evt) { if (evt.persisted) noBack() }
        window.onunload = function () { void (0) }
    </script>
    <link href="Content/JsGrid/jsgrid-theme.css" rel="stylesheet" />
    <link href="Content/JsGrid/jsgrid-theme.min.css" rel="stylesheet" />
    <link href="Content/JsGrid/jsgrid.css" rel="stylesheet" />
    <link href="Content/JsGrid/jsgrid.min.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <div class="app-title">
        <div>
            <h1><i class="fa fa-pied-piper-pp"></i>&nbsp;Reject Problem List</h1>
            <p>ตารางแสดงรายการ Problem List.</p>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
            <li class="breadcrumb-item"><i class="fa fa-pied-piper-pp"></i></li>
            <li class="breadcrumb-item">Tables</li>
            <li class="breadcrumb-item active"><a href="#">Cover Sheet</a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="row">
                    <div class="col-sm-12 col-md-6" id="CountSum" style="color: red"></div>                
                </div>
                <div class="tile-body">
                    <div id="LoadTool"></div>
                    <table id="jsGrid">
                        <thead>
                            <tr></tr>
                        </thead>
                        <tbody>
                            <tr></tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

      <%--model detail--%>
    <div class="modal fade" id="modalDetail" style="border: 1px solid #ddd;">
        <div class="modal-dialog" style="width: 100%;">
            <div class="modal-content">
                <div class="modal-header" style="background-image: linear-gradient(#cedbdb, #e0ffe0);">
                    <h3 class="modal-title" style="font-family: Trocchi, serif; text-align: center;">Detail Problem</h3>
                    <a href="#" class="close" data-dismiss="modal">&times;</a>
                </div>
                <div class="modal-body">
                    <table class="table table-hover ">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Detail</th>
                            </tr>
                        </thead>
                        <tbody id="TdData" class="table">
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer" style="background-image: linear-gradient(#cedbdb, #e0ffe0);">
                    <a href="#" id="closeBtn" class="btn btn-secondary" data-dismiss="modal">Close</a>
                </div>
            </div>
        </div>
    </div>

      <!-- The Modal Loading -->
    <div class="modal fade" id="modalLoading">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div style="text-align: center;">
                    <img src="Content/loadingFile.gif" />
                </div>
            </div>
        </div>
    </div>

        <%-- model download file--%>
    <div class="modal fade" id="modelDownload">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h3>Upload File Problem</h3>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="tile">
                    <div class="form-group">
                        <input class="form-control" type="file" onchange="ExcleLoad(this, 'img');" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="submitSave()">Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script src="Content/js/jquery-3.2.1.min.js"></script>
    <script src="Content/JsGrid/jsgrid.js"></script>
    <script src="Content/JsGrid/jsgrid.min.js"></script>
    <script src="Content/js/plugins/sweetalert.min.js"></script>
    <script src="Content/js/plugins/bootstrap-datepicker.min.js"></script>
    <script src="Content/numeral.min.js"></script>
     <script>$("#checkDataReject").addClass("active");</script>

    <%--Base 64--%>
    <script>
        var TempDataExcle;
        var excleProcess = {
            reduceResolutionPercent: 100,
            reduceResolution: function (val) { return ((val * this.reduceResolutionPercent) / 100); },
            checkFileExtension: function (element) {
                try {
                    var temp = element.value.split('.');
                    if (temp.length > 0)
                        if (element.accept.toUpperCase().indexOf(temp[temp.length - 1].toUpperCase()) !== -1)
                            return true;
                        else
                            return false;

                    else return false;
                } catch (ex) { return false; }
            },
            encodeImageFileAsURL: function (element) {
                //if (this.checkFileExtension(element)) {
                var delayInMilliseconds = 1000;
                var Defer = $.Deferred();
                var reader = new FileReader(); // load file
                reader.onloadend = function () {
                    //var img = new Image;
                    //img.onload = function () {
                    Defer.resolve(this);
                    TempDataExcle = reader.result;
                    setTimeout(function () {
                        $("#modalLoading").modal('toggle');
                        $("#modelDownload").modal();
                    }, delayInMilliseconds);
                    //};
                    //img.src = reader.result;
                }
                reader.onerror = function () { Defer.resolve(null); };
                reader.readAsDataURL(element.files[0]);
                return Defer.promise();
                //} else {
                //    alert('àÅ×Í¡ä´éà©¾ÒÐ .png, .jpg, .jpeg');
                //}
            },
            imageToDataUri: function (img, width, height) {
                var canvas = document.createElement('canvas'), ctx = canvas.getContext('2d');
                canvas.width = width;
                canvas.height = height;
                ctx.drawImage(img, 0, 0, width, height);
                return canvas.toDataURL();
            }
        }

        function ExcleLoad(myFileImage, imgObjId) {
            $("#modelDownload").modal('toggle');
            $("#modalLoading").modal({
                backdrop: 'static',
                keyboard: false,
            });
            $.when(excleProcess.encodeImageFileAsURL(myFileImage)).then(
                function (Data) {
                    var newDataUri = excleProcess.imageToDataUri(Data, excleProcess.reduceResolution(Data.width), excleProcess.reduceResolution(Data.height));
                    // Display('imgQuiz', newDataUri);                    
                });
        }

    </script>
     <%--Even modal--%>
    <script>
        var fileName = "";
        function openModalupload(NO_Cover) {
            $("#modalDetail").modal('toggle');
            $("#modelDownload").modal();
            fileName = NO_Cover;
        }

        function submitSave() {
            var NO_Cover = fileName;
            var TempDataExcleSendStr = String(TempDataExcle);
            TempDataExcleSend = {};
            $.extend(TempDataExcleSend, {
                fileName: NO_Cover,
                linkfile: TempDataExcleSendStr
            });
            $.ajax({
                type: "POST",
                url: "problem_CoverSheet.aspx/saveFileExcel",
                data: "{TempDataExcleSend: " + JSON.stringify(TempDataExcleSend) + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "JSON",
                async: false,
                success: function (response) {
                    _dataBack = response.d;
                    if (_dataBack == "SUCCESS") {
                        swal({
                            title: "เตือน !",
                            text: "ดำเนินการเพิ่มข้อมูลสำเร็จ!",
                            type: "success",
                            dangerMode: true,
                            animation: false,
                            customClass: "animated jackInTheBox",
                            confirmButtonClass: "btn-success",
                            confirmButtonText: "OK"
                        }, function () {
                            window.location.href = "problem_RejectCoverSheet.aspx";
                        });
                    }
                    else {
                        alert("NO return");
                    }
                },
                error: function (response) {
                    alert("ERROR")
                }
            });
        }
    </script>

    <%--get Reject--%>
    <script>
        $(document).ready(function () {
            var nameGet = sessionStorage.getItem("SetEMP_NAME");
            var policeGet = sessionStorage.getItem("SetPOSITION_ID");
            if(policeGet == "J2" || policeGet == "J1"){
                $.ajax({
                    type: "POST",
                    url: "problem_RejectCoverSheet.aspx/ajaxRaad_RejectbyName",
                    async: false,
                    data: '{nameGet:"' + nameGet + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: DataBindReject,
                    error: function (response) {
                        alert(response.responseText.Message);
                    }
                });
                function DataBindReject(response) {
                    _data_Reject = response.d;
                    _data_Reject = $.parseJSON(_data_Reject);
                    count = _data_Reject.length;
                    $("#CountSum").html("จำนวนข้อมูลทั้งหมด :" + count);
                    $(function () {
                        $("#jsGrid").jsGrid({
                            hight: "85%",
                            width: "100%",

                            sorting: true,
                            paging: true,
                            autoload: true,

                            data: _data_Reject,
                            pageIndex: 1,
                            pageSize: 11,
                            pageButtonCount: 3,
                            pagerFormat: "หน้าปัจจุบัน: {pageIndex} &nbsp;&nbsp; {first} {prev} {pages} {next} {last} &nbsp;&nbsp; หน้าทั้งหมด: {pageCount}",
                            pagePrevText: "<",
                            pageNextText: ">",
                            pageFirstText: "หน้าแรก  &nbsp;",
                            pageLastText: "หน้าสุดท้าย  &nbsp;",
                            pageNavigatorNextText: "&#8230;",
                            pageNavigatorPrevText: "&#8230;",

                            fields: [
                                { name: "NO", title: "No.", type: "text", width: "20%", css: "h6" },
                                { name: "DATE_PROBLEM", title: "Date.", type: "text", width: "20%", css: "h6" },
                                { name: "PROBLEM", title: "Problem (สภาพปัญหา).", type: "text", width: "70%", css: "h6" },
                                { name: "EMP_NO", title: "Person In Charge.", type: "text", width: "25%", css: "h6" },
                                { name: "PROBLEM_TYPE", title: "Problem Type.", type: "text", width: "20%", css: "h6" },
                                { name: "SCHEDULE", title: "Schedule.", type: "text", width: "20%", css: "h6" },
                                {
                                    name: "status_Approved", title: "Status Approve.", type: "text", width: "20%",
                                    itemTemplate: function (value, item) {
                                        if (value == "Wait Check") {
                                            return "<div style='color: #FF8C00'>" + value + "</div>"
                                        } else if (value == "J3 Reject") {
                                            return "<div style='color: red'>" + value + "</div>"
                                        } else {
                                            return "<div style='color: #00635a'>" + value + "</div>"
                                        }

                                    }
                                },

                            ],
                            rowClick: function (args) {
                                var getData = args.item;
                                var keys = Object.keys(getData);
                                var text = [];
                                detailProblem(getData.NO)
                            }
                        });
                    });
                }


            } else if (policeGet == "ADMIN" || policeGet == "M1") {
                $.ajax({
                    type: "POST",
                    url: "problem_RejectCoverSheet.aspx/ajaxRaad_RejectAll",
                    async: false,
                    data: '{}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: DataBindJ3CheckALL,
                    error: function (response) {
                        alert(response.responseText.Message);
                    }
                });
                function DataBindJ3CheckALL(response) {
                    _dataJ3_Check = response.d;
                    _dataJ3_Check = $.parseJSON(_dataJ3_Check);
                    count = _dataJ3_Check.length;
                    $("#CountSum").html("จำนวนข้อมูลทั้งหมด :" + count);
                    $(function () {
                        $("#jsGrid").jsGrid({
                            hight: "85%",
                            width: "100%",

                            sorting: true,
                            paging: true,
                            autoload: true,

                            data: _dataJ3_Check,
                            pageIndex: 1,
                            pageSize: 11,
                            pageButtonCount: 3,
                            pagerFormat: "หน้าปัจจุบัน: {pageIndex} &nbsp;&nbsp; {first} {prev} {pages} {next} {last} &nbsp;&nbsp; หน้าทั้งหมด: {pageCount}",
                            pagePrevText: "<",
                            pageNextText: ">",
                            pageFirstText: "หน้าแรก  &nbsp;",
                            pageLastText: "หน้าสุดท้าย  &nbsp;",
                            pageNavigatorNextText: "&#8230;",
                            pageNavigatorPrevText: "&#8230;",

                            fields: [
                                { name: "NO", title: "No.", type: "text", width: "20%", css: "h6" },
                                { name: "DATE_PROBLEM", title: "Date.", type: "text", width: "20%", css: "h6" },
                                { name: "PROBLEM", title: "Problem (สภาพปัญหา).", type: "text", width: "70%", css: "h6" },
                                { name: "EMP_NO", title: "Person In Charge.", type: "text", width: "25%", css: "h6" },
                                { name: "PROBLEM_TYPE", title: "Problem Type.", type: "text", width: "20%", css: "h6" },
                                { name: "SCHEDULE", title: "Schedule.", type: "text", width: "20%", css: "h6" },
                                {
                                    name: "status_Approved", title: "Status Approve.", type: "text", width: "20%",
                                    itemTemplate: function (value, item) {
                                        if (value == "Wait Check") {
                                            return "<div style='color: #FF8C00'>" + value + "</div>"
                                        } else if (value == "J3 Reject") {
                                            return "<div style='color: red'>" + value + "</div>"
                                        } else {
                                            return "<div style='color: #00635a'>" + value + "</div>"
                                        }

                                    }
                                },

                            ],
                            rowClick: function (args) {
                                var getData = args.item;
                                var keys = Object.keys(getData);
                                var text = [];
                                detailProblem(getData.NO)
                            }
                        });
                    });
                }
            }
        });
    </script>
        <%-- detail problem--%>
    <script>
        function detailProblem(number) {
            $("#modalDetail").modal({
                backdrop: 'static',
                keyboard: false
            });
            $(function () {
                $.ajax({
                    type: "POST",
                    url: "problem_CoverSheet.aspx/ajaxRaadDetailCoversheet",
                    async: false,
                    data: '{number:"' + number + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: DataBindDetail,
                    error: function (response) {
                        alert(response.responseText.Message);
                    }
                });
            });
            function DataBindDetail(response) {
                _dataDetail = response.d;
                _dataDetail = $.parseJSON(_dataDetail);
                noSend = String(_dataDetail[0].NO);
                $("#TdData").html(
                             "<tr>" +
                                "<td>No</td>" +
                                "<td style='color: #216bd0'>" +
                                        _dataDetail[0].NO
                                + "</td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td>Date</td>" +
                                "<td style='color: #216bd0'>" +
                                   _dataDetail[0].DATE_PROBLEM
                                + "</td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td>Model</td>" +
                                "<td style='color: #216bd0'>" +
                                     _dataDetail[0].MODEL
                                + "</td>" +
                            "</tr>" +
                            "<tr>"
                                + "<td>Phase MT/MP</td>" +
                                "<td style='color: #216bd0'>" +
                                 _dataDetail[0].PHASE_MT_MP
                                + "</td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td>Rank Jig</td>" +
                                "<td style='color: #216bd0'>" + _dataDetail[0].RANK_JIG + "</td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td>Problem (สภาพปัญหา)</td>" +
                                "<td class='tdSize' style='color: #216bd0'>" + _dataDetail[0].PROBLEM + "</td>"
                            + "</tr>" +
                            "<tr>" +
                                "<td>Jig Code</td>" +
                                "<td style='color: #216bd0'>" + _dataDetail[0].JIG_TOOL_CODE + "</td>"
                            + "</tr>" +
                            "<tr>" +
                                "<td>Problem Type</td>" +
                                "<td style='color: #216bd0'>" + _dataDetail[0].PROBLEM_TYPE + "</td>"
                            + "</tr>" +
                            "<tr>" +
                                "<td>Dept</td>" +
                                "<td style='color: #216bd0'>" +
                                     _dataDetail[0].DEPTS
                                + "</td>" +
                            "</tr>" +
                             "<tr>" +
                                 "<td>Person In Charge</td>" +
                                "<td style='color: #216bd0'>" +
                                     _dataDetail[0].EMP_NO
                                + "</td>" +
                            "</tr>" +
                           "<tr>" +
                                "<td>Cause</td>" +
                               "<td class='tdSize' style='color: #216bd0'>" + _dataDetail[0].CAUSE + "</td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td>Parmanent Action</td>" +
                               "<td class='tdSize' style='color: #216bd0'>" + _dataDetail[0].PERMANENT_ACTION + "</td>" +
                            "</tr>" +
                            "<tr>" +
                               "<td>Schedule</td>" +
                               "<td style='color: #216bd0'>" +
                                 _dataDetail[0].SCHEDULE
                               + "</td>" +
                            "</tr>" +
                               "<td>Flie Problem Download.</td>" +
                                   "<td>" +
                                    "<a  class='btn btn-outline-primary fa fa-download' style='color: #000000' onclick='exportExcleProblem(" + '"' + noSend + '"' + ")'></a>"
                                   + "</td>" +
                                "</tr>" +
                                "<tr>" +
                                "<td>STEP1: Import Excel File Problem</td>" +
                                "<td>" +
                                    "<a class='btn btn-outline-primary fa fa-file-excel-o' style='color: #000000'  data-toggle='modal' onclick='openModalupload(" + '"' + noSend + '"' + ")'></a>"
                                + "</td>"
                             + "</tr>" +                              
                              "<tr>"
                             );
            }
        }

        function exportExcleProblem(numBer) {
            var Coversheet_SendtoExport = {};
            $.extend(Coversheet_SendtoExport, {
                NO: numBer,
                DATE_PROBLEM: "",
                MODEL: "",
                PHASE_MT_MP: "",
                RANK_JIG: "",
                PROBLEM: "",
                JIG_TOOL_CODE: "",
                PROBLEM_TYPE: "",
                DEPT: "",
                PERSON_INCHARG: "",
                CAUSE: "",
                PERMANENT_ACTION: "",
                SCHEDULE: "",
                EMP_NO: "",
                LINK_FILE: "",
                TOOL_KEY: "",
                PROBLEM_KEY: "",
                PLACE: "",
                JIG_TOOL_NAME: ""
            });
            $.ajax({
                type: "POST",
                url: "problem_CoverSheet.aspx/sendToExportTemplate",
                data: "{Coversheet_SendtoExport: " + JSON.stringify(Coversheet_SendtoExport) + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "JSON",
                async: false,
                success: function (response) {
                    msg_User = response.d;
                    msg_User = $.parseJSON(msg_User);
                    if (msg_User != "Failed") {
                        window.open("/Content/Template/TemplateExport.xlsx", "_blank");
                    } else {

                    }
                },
                error: function (response) {
                    alert("ERROR");
                }
            });
        }

    </script>
</asp:Content>
