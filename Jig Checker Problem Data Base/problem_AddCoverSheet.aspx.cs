﻿using Jig_Checker_Problem_Data_Base.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Jig_Checker_Problem_Data_Base
{
    public partial class problem_AddCoverSheet : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public static string getNumberCoversheet()
        {
            ManageDatabase VT = new ManageDatabase();
            string numberMax = VT.getNumbercoverSheet();
            if (numberMax == "")
            {
                string year = DateTime.Now.ToString("yy");
                string returnString = year + "-" + "0000";
                return returnString;
            }
            else
            {
                var forsum = numberMax.Split('-');
                string B = forsum[1];
                int numberConver = Int32.Parse(B);
                int numberConverRe = numberConver + 1;
                string formatRe = String.Format("{0:0000}", numberConverRe);
                string returnNO = formatRe.ToString();
                string year = DateTime.Now.ToString("yy");
                string returnString = year + "-" + returnNO;
                return returnString;
            }
        
        }

        [WebMethod]
        public static string AjaxGetJ3J4()
        {
            ManageDatabase VT = new ManageDatabase();
            var DataTool = VT.GetJ3J4();
            //ListtoDataTableConverter converter = new ListtoDataTableConverter();
            //dtALL = converter.ToDataTable(DataTool);
            var data = JsonConvert.SerializeObject(DataTool, Formatting.Indented);
            return data;

        }
        [WebMethod]
        public static string InsertCoverSheet(property_CoverSheet Coversheet_Add)
        {
            ManageDatabase VT = new ManageDatabase();
            Coversheet_Add.NO = getNumberCoversheet();
            //Add TEXT//
            try
            {
                var AddBack = VT.insertCoversheet(Coversheet_Add);
                if (AddBack == true)
                {
                    var JsonBack = "SUCCESS";
                    var data = JsonConvert.SerializeObject(JsonBack, Formatting.Indented);
                    return data;
                }
                else
                {
                    var JsonBack = "SAVD TOOLID DUPLICATE";
                    var data = JsonConvert.SerializeObject(JsonBack, Formatting.Indented);
                    return data;
                }
            }
            catch (Exception ex)
            {
                string Exback = ex.ToString();
                var JsonBack = "SAVE DATA FAILED";
                var data = JsonConvert.SerializeObject(JsonBack, Formatting.Indented);
                return data;
            }
            
        }


    }
}