﻿using Jig_Checker_Problem_Data_Base.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Jig_Checker_Problem_Data_Base
{
    public partial class login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static String CheckLogin(property_User LoginData)
        {
            try
            {
                ManageDatabase VT = new ManageDatabase();
                bool status = VT.Login(LoginData);
                if (status == true)
                {
                    var pase = "Login Success";
                    var data = JsonConvert.SerializeObject(pase, Formatting.Indented);
                    return data;
                }
                else
                {
                    var pase = "Login Failed";
                    var data = JsonConvert.SerializeObject(pase, Formatting.Indented);
                    return data;
                }

            }
            catch (Exception ex)
            {
                string exShow = ex.ToString();
                return exShow;

            }
        }
        [WebMethod]
        public static String GetUser(property_User GetUserData)
        {
            ManageDatabase VT = new ManageDatabase();
            try
            {
                var outPutCompare = VT.GetUserData(GetUserData);
                var data = JsonConvert.SerializeObject(outPutCompare, Formatting.Indented);
                return data;
            }
            catch (Exception ex)
            {
                var exShow = ex.ToString();
                return "Get Failed";
            }

        }

        [WebMethod]
        public static String GetEmail(property_User LoginData)
        {
            ManageDatabase VT = new ManageDatabase();
            try
            {
                var outPutEMP_NO = VT.GetUserEmail(LoginData);
                MailAddress from = new MailAddress("IT_PROJECT_PDE@cht.canon.co.th");
                MailAddress to = new MailAddress(outPutEMP_NO[0].EMAIL);
                MailMessage message = new MailMessage(from, to);
                message.Subject = "Password Forget.";
                message.Body = @"Your password: " +outPutEMP_NO[0].PASSWORD;
                string server = "10.129.15.206";
                SmtpClient client = new SmtpClient(server);
                try
                {
                    client.Send(message);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception caught in CreateTestMessage4(): {0}",
                                ex.ToString());
                }

                return "Success";
            }
            catch (Exception ex)
            {
                var exShow = ex.ToString();
                return "Failed";
            }

        }

        [WebMethod]
        public static String ajaxRaadNotification(string J3Data)
        {
            ManageDatabase VT = new ManageDatabase();
            try
            {
                var outPutCompare = VT.GetCountCheckbyJ3(J3Data);
                var data = JsonConvert.SerializeObject(outPutCompare, Formatting.Indented);
                return data;
            }
            catch (Exception ex)
            {
                var exShow = ex.ToString();
                return "Get Failed";
            }

        }
        [WebMethod]
        public static String ajaxRaadNotificationReject(string rejectData)
        {
            ManageDatabase VT = new ManageDatabase();
            try
            {
                var outPutCompare = VT.GetCountReject(rejectData);
                var data = JsonConvert.SerializeObject(outPutCompare, Formatting.Indented);
                return data;
            }
            catch (Exception ex)
            {
                var exShow = ex.ToString();
                return "Get Failed";
            }

        }
    }
}