﻿using Jig_Checker_Problem_Data_Base.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Jig_Checker_Problem_Data_Base
{
    public partial class problem_RejectCoverSheet : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public static DataTable dtALL;
        [WebMethod]
        public static string ajaxRaad_RejectAll()
        {
            ManageDatabase VT = new ManageDatabase();
            var DataTool = VT.Get_RejectAll();
            ListtoDataTableConverter converter = new ListtoDataTableConverter();
            dtALL = converter.ToDataTable(DataTool);
            var data = JsonConvert.SerializeObject(DataTool, Formatting.Indented);
            return data;

        }
        [WebMethod]
        public static string ajaxRaad_RejectbyName(string nameGet)
        {
            ManageDatabase VT = new ManageDatabase();
            var DataTool = VT.Get_RejecByName(nameGet);
            ListtoDataTableConverter converter = new ListtoDataTableConverter();
            dtALL = converter.ToDataTable(DataTool);
            var data = JsonConvert.SerializeObject(DataTool, Formatting.Indented);
            return data;

        }

        public class ListtoDataTableConverter
        {
            public DataTable ToDataTable<T>(List<T> items)
            {
                DataTable dataTable = new DataTable(typeof(T).Name);
                //Get all the properties
                PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo prop in Props)
                {
                    //Setting column names as Property names
                    dataTable.Columns.Add(prop.Name);
                }

                foreach (T item in items)
                {
                    var values = new object[Props.Length];
                    for (int i = 0; i < Props.Length; i++)
                    {
                        //inserting property values to datatable rows
                        values[i] = Props[i].GetValue(item, null);
                    }

                    dataTable.Rows.Add(values);

                }
                //put a breakpoint here and check datatable
                return dataTable;
            }
        }
    }
}