﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="Jig_Checker_Problem_Data_Base.login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=10" />
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="shortcut icon" href="Content/ProblemDatabase.png" />
    <link href="Content/css/main.css" rel="stylesheet" />
    <link href="Content/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" />
    <link href="Content/bootstrap-sweetalert-master/dist/animate.css" rel="stylesheet" />
    <link href="Content/bootstrap-sweetalert-master/dist/sweetalert.css" rel="stylesheet" />
    <title>Login Problem Data Base</title>
    <%-- Block Back Load--%>
    <script type="text/javascript">
        function noBack() {
            window.history.forward()
        }

        noBack();
        window.onload = noBack;
        window.onpageshow = function (evt) { if (evt.persisted) noBack() }
        window.onunload = function () { void (0) }
    </script>
</head>
<body>
    <section class="material-half-bg">
        <div class="cover"></div>
    </section>
    <section class="login-content">
        <div class="logo">
            <h1>Problam Data Base</h1>
        </div>
        <div class="login-box">
            <form class="login-form">
                <h3 class="login-head"><i class="fa fa-lg fa-fw fa-user"></i>SIGN IN</h3>
                <div class="form-group">
                    <label class="control-label">USERNAME</label>
                    <input class="form-control" type="text" placeholder="EMP ID" id="urlUser" autofocus/>
                </div>
                <div class="form-group">
                    <label class="control-label">PASSWORD</label>
                    <input class="form-control" type="password" placeholder="Password"  id="urlPass"/>
                </div>
                <div class="form-group">
                    <div class="utility">
                        <div class="animated-checkbox">
                            <label>
                                <input type="checkbox"/><span class="label-text">Stay Signed in</span>
                            </label>
                        </div>
                        <p class="semibold-text mb-2"><a href="#" data-toggle="flip">Forgot Password ?</a></p>
                    </div>
                    <div class="utility">
                        <div>
                        </div>
                        <p class="semibold-text mb-2"><a href="register.aspx" >Register user ?</a></p>
                    </div>
                </div>
                <div class="form-group btn-container">
                    <button class="btn btn-primary btn-block" id="submit1"><i class="fa fa-sign-in fa-lg fa-fw"></i>SIGN IN</button>
                </div>
            </form>
            <form class="forget-form">
                <h3 class="login-head"><i class="fa fa-lg fa-fw fa-lock"></i>Forgot Password ?</h3>
                <div class="form-group">
                    <label class="control-label">EMAIL</label>
                    <input class="form-control" type="text" id="emailText" placeholder="Email"/>
                </div>
                <div class="form-group btn-container">
                    <button class="btn btn-primary btn-block" id="resetButton"><i class="fa fa-unlock fa-lg fa-fw"></i>RESET</button>
                </div>
                <div class="form-group mt-3">
                    <p class="semibold-text mb-0"><a href="#" data-toggle="flip"><i class="fa fa-angle-left fa-fw"></i>Back to Login</a></p>
                </div>
            </form>        
        </div>
               <!-- The Modal Loading -->
                    <div class="modal fade" id="modalLoadingLogin">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content" style="text-align:center">
                                <div>
                                    <img src="Content/loadingLogin.gif" />
                                </div>
                            </div>
                        </div>
                    </div>
    </section>
    <!-- Essential javascripts for application to work-->
    <script src="Content/js/jquery-3.2.1.min.js"></script>
    <script src="Content/js/popper.min.js"></script>
    <script src="Content/js/bootstrap.min.js"></script>
    <script src="Content/js/main.js"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="Content/js/plugins/pace.min.js"></script>
    <script src="Content/bootstrap-sweetalert-master/dist/sweetalert.js"></script>
    <script src="Content/bootstrap-sweetalert-master/dist/sweetalert.min.js"></script>
    <script type="text/javascript">
        // Login Page Flipbox control
        $('.login-content [data-toggle="flip"]').click(function () {
            $('.login-box').toggleClass('flipped');
            return false;
        });
    </script>
   <%-- login send--%>
    <script>
        $("#submit1").click(function () {
            getValuesLogin();
            return false;
        });
        function getValuesLogin() {
            var LoginData = {};
            $.extend(LoginData, {
                EMP_NO: $("#urlUser").val(),
                EMP_NAME: "",
                EMP_LASTNAME: "",
                POSITION_ID: "",
                EMAIL: "",
                PASSWORD: $("#urlPass").val(),
                STATUS: "",
                PITURE: "",
                DEPTS: "",
                LinkDitinationPiture:""
            })
            $.ajax({
                type: "POST",
                url: "login.aspx/CheckLogin",
                data: "{LoginData: " + JSON.stringify(LoginData) + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "JSON",
                async: false,
                beforeSend: function () {
                    $("#modalLoadingLogin").modal({
                        backdrop: 'static',
                        keyboard: false,
                    });
                },
                success: function (response) {
                    msg = response.d;
                    msg = $.parseJSON(msg);
                    if (msg == "Login Success") {
                        var pass = $("#urlPass").val();
                        getUser();
                    }
                    else {
                        $("#modalLoadingLogin").html('toggle');
                        swal({
                            title: "เตือน !",
                            text: "Employee ID or Password ผิดพลาด!",
                            type: "error",
                            dangerMode: true,
                            animation: true,                          
                        });
                    }
                },
                error: function (response) {
                    alert(response);
                }
            });           
        }
        function getUser() {
            var GetUserData = {};
            $.extend(GetUserData, {
                EMP_NO: $("#urlUser").val(),
                EMP_NAME: "",
                EMP_LASTNAME: "",
                POSITION_ID: "",
                EMAIL: "",
                PASSWORD: $("#urlPass").val(),
                STATUS: "",
                PITURE: "",
                DEPTS: "",
                LinkDitinationPiture: ""
            })
            $.ajax({
                type: "POST",
                url: "Login.aspx/GetUser",
                data: "{GetUserData: " + JSON.stringify(GetUserData) + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "JSON",
                async: false,
                success: function (response) {
                    msg = response.d;
                    msg = $.parseJSON(msg);
                    if (msg.length > 0) {
                        sessionStorage.setItem("SetEMP_NO", msg[0]["EMP_NO"]);
                        sessionStorage.setItem("SetEMP_NAME", msg[0]["EMP_NAME"]);
                        sessionStorage.setItem("SetEMP_LASTNAME", msg[0]["EMP_LASTNAME"]);
                        sessionStorage.setItem("SetPOSITION_ID", msg[0]["POSITION_ID"]);                       
                        sessionStorage.setItem("SetSTATUS", msg[0]["STATUS"]);
                        sessionStorage.setItem("SetPITURE", msg[0]["PITURE"]);
                        sessionStorage.setItem("SetDEPTS", msg[0]["DEPTS"]);
                        sessionStorage.setItem("SetEMAIL", msg[0]["EMAIL"]); 
                        sessionStorage.setItem("SetLinkDitinationPiture", msg[0]["LinkDitinationPiture"]);
                        GetJ3Check(msg[0]["EMP_NAME"]);
                        GetReject(msg[0]["EMP_NAME"]);
                        window.location.href = "problemDatabaseBorad.aspx";
                    }
                },
                error: function (response) {
                    alert(response);
                }
            });
        }
    </script>
   <%-- reset Password form email--%>
    <script>
        $("#resetButton").click(function () {
           resetPassword();
            return false;
        });
        function resetPassword() {
            var LoginData = {};
            $.extend(LoginData, {
                EMP_NO: "",
                EMP_NAME: "",
                EMP_LASTNAME: "",
                POSITION_ID: "",
                EMAIL: $("#emailText").val(),
                PASSWORD: "",
                STATUS: "",
                PITURE: "",
                DEPTS: "",
                LinkDitinationPiture: ""
            })
            $.ajax({
                type: "POST",
                url: "login.aspx/GetEmail",
                data: "{LoginData: " + JSON.stringify(LoginData) + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "JSON",
                async: false,
                success: function (response) {
                    msg = response.d;                 
                    if (msg == "Success") {
                        swal({
                            title: "แจ้ง !",
                            text: "ส่ง Password ไปที่ Email แล้ว!",
                            type: "success",
                            dangerMode: true,
                            animation: true,
                        }, {

                        });
                    }
                    else {
                        swal({
                            title: "เตือน !",
                            text: "Email ไม่มีในฐานข้อมูล!",
                            type: "error",
                            dangerMode: true,
                            animation: true,
                        });
                    }
                },
                error: function (response) {
                    alert(response);
                }
            });
        }
    </script>

     <%--getNotification--%>  
    <script>
        function GetJ3Check(J3Data) {
            $.ajax({
                type: "POST",
                url: "login.aspx/ajaxRaadNotification",
                async: false,
                data: '{J3Data:"' + J3Data + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: DataBind,
                error: function (response) {
                    alert(response.responseText.Message);
                }
            });
            function DataBind(response) {
                _dataJ3 = response.d;
                _dataJ3 = $.parseJSON(_dataJ3);
                count = _dataJ3.length;
                sessionStorage.setItem("Set_countJ3", count);
                sessionStorage.setItem("Set_dateDrip", _dataJ3[0]["CHECK_DATE"]);
            }
        }

        function GetReject(rejectData) {
            $.ajax({
                type: "POST",
                url: "login.aspx/ajaxRaadNotificationReject",
                async: false,
                data: '{rejectData:"' + rejectData + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: DataBind,
                error: function (response) {
                    alert(response.responseText.Message);
                }
            });
            function DataBind(response) {
                _dataReject = response.d;
                _dataReject = $.parseJSON(_dataReject);
                count = _dataReject.length;
                sessionStorage.setItem("Set_countReject", count);              
            }
        }
    </script>
</body>
</html>
