﻿using Jig_Checker_Problem_Data_Base.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Jig_Checker_Problem_Data_Base
{
    public partial class problem_MasterUser : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static string ajaxSearchUser(property_UserSearch searchData)

        {
            ManageDatabase VT = new ManageDatabase();
            var DataTool = VT.GetUserSearch(searchData);
            var data = JsonConvert.SerializeObject(DataTool, Formatting.Indented);
            return data;

        }
        [WebMethod]
        public static string ajaxAllUser()

        {
            ManageDatabase VT = new ManageDatabase();
            var DataTool = VT.GetUserAll();         
            var data = JsonConvert.SerializeObject(DataTool, Formatting.Indented);
            return data;

        }

        [WebMethod]
        public static string changeBrand(property_User userChangeBrand)
        {
            ManageDatabase VT = new ManageDatabase();
    
            try
            {
                VT.UpdateBrand(userChangeBrand);
                var JsonBack = "UPDATE SUCCESS";
                var data = JsonConvert.SerializeObject(JsonBack, Formatting.Indented);
                return data;

            }
            catch (Exception)
            {
                var JsonBack = "UPDATE DATA FAILED";
                var data = JsonConvert.SerializeObject(JsonBack, Formatting.Indented);
                return data;

            }
        }

        [WebMethod]
        public static string userNameLastNameUpdate(property_User name_lastName)
        {
            ManageDatabase VT = new ManageDatabase();
            //Add TEXT//
            try
            {
                VT.UpdateUserNameLasetname(name_lastName);
                var JsonBack = "UPDATE SUCCESS";
                var data = JsonConvert.SerializeObject(JsonBack, Formatting.Indented);
                return data;

            }
            catch (Exception)
            {
                var JsonBack = "UPDATE DATA FAILED";
                var data = JsonConvert.SerializeObject(JsonBack, Formatting.Indented);
                return data;

            }
        }

        [WebMethod]
        public static string changeDepts(property_User userChangeDepts)
        {
            ManageDatabase VT = new ManageDatabase();
         
            try
            {
                VT.UpdateUserDepts(userChangeDepts);
                var JsonBack = "UPDATE SUCCESS";
                var data = JsonConvert.SerializeObject(JsonBack, Formatting.Indented);
                return data;

            }
            catch (Exception)
            {
                var JsonBack = "UPDATE DATA FAILED";
                var data = JsonConvert.SerializeObject(JsonBack, Formatting.Indented);
                return data;

            }
        }

        [WebMethod]
        public static string changeEmail(property_User userChangeEmail)
        {
            ManageDatabase VT = new ManageDatabase();
          
            try
            {
                VT.UpdateUserEmail(userChangeEmail);
                var JsonBack = "UPDATE SUCCESS";
                var data = JsonConvert.SerializeObject(JsonBack, Formatting.Indented);
                return data;

            }
            catch (Exception)
            {
                var JsonBack = "UPDATE DATA FAILED";
                var data = JsonConvert.SerializeObject(JsonBack, Formatting.Indented);
                return data;

            }
        }

        [WebMethod]
        public static string GetUserNameLastNameUpdate(property_User name_lastName)
        {
            ManageDatabase VT = new ManageDatabase();
            var DataTool = VT.GetUserUpdateNameLastName(name_lastName);
            var data = JsonConvert.SerializeObject(DataTool, Formatting.Indented);
            return data;
        }

        [WebMethod]
        public static string GetEmailUpdate(property_User userChangeEmail)
        {
            ManageDatabase VT = new ManageDatabase();
            var DataTool = VT.GetEmailUpdate(userChangeEmail);
            var data = JsonConvert.SerializeObject(DataTool, Formatting.Indented);
            return data;
        }

        [WebMethod]
        public static string GetLink(property_User userChangeLink)
        {
            ManageDatabase VT = new ManageDatabase();
            var DataTool = VT.GetLinkBack(userChangeLink);
            //ListtoDataTableConverter converter = new ListtoDataTableConverter();
            //dtALL = converter.ToDataTable(DataTool);
            var data = JsonConvert.SerializeObject(DataTool, Formatting.Indented);
            return data;
        }

        [WebMethod]
        public static string changePiture(property_User userChangePiture)
        {
            ManageDatabase VT = new ManageDatabase();
            var DeleteOldFile = HttpContext.Current.Server.MapPath(userChangePiture.PITURE);
            if (DeleteOldFile != "")
            {
                File.Delete(DeleteOldFile);
            }
            String[] arrBase64PDF = userChangePiture.LinkDitinationPiture.Split(',');
            string time = "_" + DateTime.Now.ToString("ss");
            var filenamePDF = userChangePiture.EMP_NO;
            var mapPDF = HttpContext.Current.Server.MapPath("~/Content/Piture_User");
            File.WriteAllBytes(mapPDF + "/" + filenamePDF + time + ".JPG", Convert.FromBase64String(arrBase64PDF[1]));
            userChangePiture.PITURE = "/Content/Piture_User/" + filenamePDF + time + ".JPG";
            //Add TEXT//
            try
            {
                VT.UpdateUserPiture(userChangePiture);
                var JsonBack = "UPDATE SUCCESS";
                var data = JsonConvert.SerializeObject(JsonBack, Formatting.Indented);
                return data;

            }
            catch (Exception)
            {
                var JsonBack = "UPDATE DATA FAILED";
                var data = JsonConvert.SerializeObject(JsonBack, Formatting.Indented);
                return data;

            }
        }

        [WebMethod]
        public static string ajaxSearchUserApprove(property_UserSearch searchData)

        {
            ManageDatabase VT = new ManageDatabase();
            var DataTool = VT.getUserApprove(searchData);
            var data = JsonConvert.SerializeObject(DataTool, Formatting.Indented);
            return data;

        }

        [WebMethod]
        public static string CheckApproved(property_User userApproved)
        {
            ManageDatabase VT = new ManageDatabase();
            //Add TEXT//
            try
            {
                VT.UpdateStatusApproved(userApproved);
                var JsonBack = "UPDATE SUCCESS";
                var data = JsonConvert.SerializeObject(JsonBack, Formatting.Indented);
                return data;

            }
            catch (Exception)
            {
                var JsonBack = "UPDATE DATA FAILED";
                var data = JsonConvert.SerializeObject(JsonBack, Formatting.Indented);
                return data;

            }
        }



















        public class ListtoDataTableConverter
        {
            public DataTable ToDataTable<T>(List<T> items)
            {
                DataTable dataTable = new DataTable(typeof(T).Name);
                //Get all the properties
                PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo prop in Props)
                {
                    //Setting column names as Property names
                    dataTable.Columns.Add(prop.Name);
                }

                foreach (T item in items)
                {
                    var values = new object[Props.Length];
                    for (int i = 0; i < Props.Length; i++)
                    {
                        //inserting property values to datatable rows
                        values[i] = Props[i].GetValue(item, null);
                    }

                    dataTable.Rows.Add(values);

                }
                //put a breakpoint here and check datatable
                return dataTable;
            }
        }
    }
}