﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master_Page.Master" AutoEventWireup="true" CodeBehind="problem_MasterUser.aspx.cs" Inherits="Jig_Checker_Problem_Data_Base.problem_MasterUser" %>
<%@ OutputCache Duration="3600" Location="Server" VaryByParam="none" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <%-- Block Back Load--%>
    <script type="text/javascript">
        function noBack() {
            window.history.forward()
        }
        noBack();
        window.onload = noBack;
        window.onpageshow = function (evt) { if (evt.persisted) noBack() }
        window.onunload = function () { void (0) }
    </script>
        <style>
        .table_cell_OutPlan {
            background-color: red;
        }

        .table_cell_SuccessPlan {
            background-color: forestgreen;
        }

        .tdbreak {
            word-break: break-all;
        }
    </style>
    <%-- class search--%>
    <style>
        .searchClass {
            margin-left: 0.5em;
            display: inline-block;
            width: auto;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            padding: .25rem .5rem;
            font-size: .875rem;
            line-height: 1.5;
            border-radius: .2rem;
        }
        @-moz-document url-prefix() {
            .dropdown-select {
                padding-left: 6px;
            }
        }

        .dropdown-dark {
            color: #fff;
            background: #444;
            border-color: #111111 #0a0a0a black;
            line-height: 1.5;
            font-size: .875rem;
            width: auto;
            background-image: -webkit-linear-gradient(top, transparent, rgba(0, 0, 0, 0.4));
            background-image: -moz-linear-gradient(top, transparent, rgba(0, 0, 0, 0.4));
            background-image: -o-linear-gradient(top, transparent, rgba(0, 0, 0, 0.4));
            background-image: linear-gradient(to bottom, transparent, rgba(0, 0, 0, 0.4));
            -webkit-box-shadow: inset 0 1px rgba(255, 255, 255, 0.1), 0 1px 1px rgba(0, 0, 0, 0.2);
            box-shadow: inset 0 1px rgba(255, 255, 255, 0.1), 0 1px 1px rgba(0, 0, 0, 0.2);
        }
            .dropdown-dark:before {
                border-bottom-color: #aaa;
            }

            .dropdown-dark:after {
                border-top-color: #aaa;
            }

            .dropdown-dark .dropdown-select {
                color: #cedbdb;
                text-shadow: 0 1px #fff;
                background: #444; /* Fallback for IE 8 */
            }

                .dropdown-dark .dropdown-select:focus {
                    color: #ccc;
                }

                .dropdown-dark .dropdown-select > option {
                    background: #444;
                    text-shadow: 0 1px rgba(0, 0, 0, 0.4);
                }
    </style>
    <link href="Content/JsGrid/jsgrid-theme.css" rel="stylesheet" />
    <link href="Content/JsGrid/jsgrid-theme.min.css" rel="stylesheet" />
    <link href="Content/JsGrid/jsgrid.css" rel="stylesheet" />
    <link href="Content/JsGrid/jsgrid.min.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

      <div class="app-title">
        <div>
            <h1><i class="fa fa-pied-piper-pp"></i>&nbsp;user</h1>
            <p>Table User List.</p>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
            <li class="breadcrumb-item"><i class="fa fa-pied-piper-pp"></i></li>
            <li class="breadcrumb-item">Tables</li>
            <li class="breadcrumb-item active"><a href="#">Master User</a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="row">
                    <div class="col-sm-12 col-md-6" id="CountSum" style="color: red"></div>
                    <div class="col-sm-12 col-md-6" style="text-align: right;">
                        <select class="dropdown dropdown-dark" id="selectSearch">
                            <option value="0">Select…</option>
                            <option value="EMP_NO">Employee Number</option>
                            <option value="EMP_NAME">Employee Name</option>  
                            <option value="STATUS">User Status</option>                                                   
                        </select>
                        <label>
                            <input type="text" class="searchClass" onkeypress="return runScript(event)" id="myInput" placeholder="ค้นหา " title="Type in a name" />
                        </label>
                    </div>
                </div>
                <div class="tile-body">
                    <div id="LoadTool"></div>
                    <table id="jsGrid">
                        <thead>
                            <tr></tr>
                        </thead>
                        <tbody>
                            <tr></tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

     <%-- model Seting Master User Data--%>
        <div class="modal fade bd-example-modal-lg" id="modalManagmentUser" style="border: 1px solid #ddd;">
            <div class="modal-dialog" >
                <div class="modal-content">
                    <div class="modal-header" style="background-image: linear-gradient(#cedbdb, #e0ffe0);">
                        <h3 class="modal-title" style="font-family: Trocchi, serif; text-align: center;">User Management</h3>   
                        <a href="#" class="close" onclick="closeFromUsermaster()" data-dismiss="modal">&times;</a>                                       
                    </div>
                    <div class="modal-body">
                        <div class="tile">
                            <label id="dropDownAppUser"><strong>User Approve.</strong></label>
                            <div class="row col-sm-12">                         
                             <a class="btn btn-primary" style="color: #ffffff" id="appUser" ><i class="fa fa-plus"></i>Approve</a>
                            </div>
                            <label for="dropDownEmpIDSelectBrand"><strong>Chang Brand.</strong></label>
                            <div class="row col-sm-12">                                                            
                                <select class="form-selectNew" id="dropDownSelectBrand">
                                    <option value="">เลือก Brand</option>
                                    <option value="ADMIN">Admin</option>
                                    <option value="M1">M1</option>
                                    <option value="J4">J4</option>
                                    <option value="J3">J3</option>
                                    <option value="J2">J2</option>
                                    <option value="J1">J1</option>
                                </select> 
                                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                             <a class="btn btn-primary" style="color: #ffffff" id="change_Brand"><i class="fa fa-user"></i>Change.</a>
                            </div>
                            <label for="dropDownEmpIDSelectNAMELASTNAME"><strong>Chang Name & LastName.</strong></label>                   
                             <input class="form-control" style="width: 50%; font-size: initial" type="text" id="name_Change" placeholder="Name." />
                                <br />
                            <div class="row col-sm-12">                               
                                <input class="form-control" style="width: 52%; font-size: initial" type="text" id="lastName_Change" placeholder="Last Name."  />
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                   <a class="btn btn-primary" style="color: #ffffff" id="change_NameLastName" ><i class="fa fa-user"></i>Change.</a>
                            </div>
                            <label for="dropDownEmpIDSelectDept"><strong>Chang Dept.</strong></label>
                            <div class="row col-sm-12">                              
                                <select class="form-selectNew" id="dropDownSelectDepts">
                                    <option value="">เลือก Dept</option>                                   
                                    <option value="PDE-J">PDE-J HT</option>
                                    <option value="PDE-J RA">PDE-J RA</option>                                   
                                </select> 
                                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                             <a class="btn btn-primary" style="color: #ffffff" id="change_Depts"><i class="fa fa-cog"></i>Change.</a>
                            </div>
                             <label for="dropDownEmpIDSelectEmail"><strong>Chang Email.</strong></label>                       
                            <div class="row col-sm-12">                               
                                <input class="form-control" style="width: 70%; font-size: initial" type="text"  id="email_Change" placeholder="Email."  />
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                   <a class="btn btn-primary" style="color: #ffffff" id="update_email" ><i class="fa fa-envelope-o"></i>Change.</a>
                            </div>
                              <label for="dropDownEmpIDpiture"><strong>Change Piture.</strong></label>
                            <div class="row col-lg-12">
                                <div class="col-md-8">
                                    <input class="form-control" id="filePiture" type="file" onchange="imgLoad(this, 'img');" />
                                </div>
                                <div class="btn-group">
                                    <a class="btn btn-primary" style="color: #ffffff" id="change_piture"><i class="fa fa-lg fa-edit"></i>Change.</a>
                                </div>
                            </div>
                        </div>
                </div>
                    <div class="modal-footer" style="background-image: linear-gradient(#cedbdb, #e0ffe0);">
                        <a href="#" id="closeBtnUser" class="btn btn-secondary"  onclick="closeFromUsermaster()" data-dismiss="modal">Close</a>
                    </div>
                </div>
            </div>
        </div>

     <!-- The Modal Loading -->
        <div class="modal fade" id="modalLoading" >
            <div class="modal-dialog" role="document">
                <div class="modal-content"> 
                    <div style="text-align:center;">                      
                      <img src="Content/loadingFile.gif" />
                    </div>                     
                </div>
            </div>
        </div>
     <!-- Data table plugin-->
    <script src="Content/js/jquery-3.2.1.min.js"></script>
    <script src="Content/JsGrid/jsgrid.js"></script>
    <script src="Content/JsGrid/jsgrid.min.js"></script>
    <script>$("#masterData").addClass("active");</script>
    <script src="Content/js/plugins/sweetalert.min.js"></script>
    <%--View User--%>
    <script>
        $("#LoadTool").html("<div class='centerMid'>" + "<img src='Content/load.gif' >" + "</div>");
        $.ajax({
            type: "POST",
            url: "problem_MasterUser.aspx/ajaxAllUser",
            data: "{}",
            contentType: "application/json; charset=utf-8",
            cache: false,
            dataType: "JSON",
            sync: false,                  
            success: returnUserAll,
            error: function (response) {
                alert(response);
            }
        });
              
        function returnUserAll(response) {
            _dataSearch = response.d;
            _dataSearch = $.parseJSON(_dataSearch);
            count = _dataSearch.length;
            $("#CountSum").html("จำนวนข้อมูลทั้งหมด :" + count);
            $(function () {
                $("#jsGrid").jsGrid({
                    hight: "85%",
                    width: "100%",
                    sorting: true,
                    paging: true,
                    autoload: true,
                    data: _dataSearch,
                    pageIndex: 1,
                    pageSize: 11,
                    pageButtonCount: 3,
                    pagerFormat: "หน้าปัจจุบัน: {pageIndex} &nbsp;&nbsp; {first} {prev} {pages} {next} {last} &nbsp;&nbsp; หน้าทั้งหมด: {pageCount}",
                    pagePrevText: "<",
                    pageNextText: ">",
                    pageFirstText: "หน้าแรก  &nbsp;",
                    pageLastText: "หน้าสุดท้าย  &nbsp;",
                    pageNavigatorNextText: "&#8230;",
                    pageNavigatorPrevText: "&#8230;",

                    fields: [
                        { name: "EMP_NO", title: "No.", type: "text", width: "20%", css: "h6" },
                        { name: "EMP_NAME", title: "Name.", type: "text", width: "20%", css: "h6" },
                        { name: "POSITION_ID", title: "Brand.", type: "text", width: "20%", css: "h6" },
                        { name: "EMAIL", title: "Email.", type: "text", width: "50%", css: "h6" },
                        { name: "DEPTS", title: "Depts.", type: "text", width: "20%", css: "h6" },                          
                        {
                            name: "STATUS", title: "Status.", type: "text", width: "20%",
                            itemTemplate: function (value, item) {
                                if(value == "ONLINE"){
                                    return "<div style='color: green'>" + value + "</div>"
                                } else if (value == "OFFLINE") {
                                    return "<div style='color: red'>" + value + "</div>"
                                } else {
                                    return "<div style='color: #00635a'>" + value + "</div>"
                                }
                               
                            }
                        },                             
                    ],
                    rowClick: function (args) {
                        var getData = args.item;
                        var keys = Object.keys(getData);
                        var text = [];
                        detailProblem(getData.EMP_NO)
                    }
                });
            });
        }
        $("#LoadTool").html(" ");
    </script>

  <%--  reload Page--%>
    <script>
        function closeFromUsermaster() {
            $("#name_Change").val("");
            $("#lastName_Change").val("");
            $("#email_Change").val("");
            $("#dropDownSelectDepts").val("");
            window.location.href = "problem_MasterUser.aspx";
        }

    </script>
    
     <%--Base 64--%>
    <script>
        var TempDataIMG;
        var imgProcess = {
            reduceResolutionPercent: 100,
            reduceResolution: function (val) { return ((val * this.reduceResolutionPercent) / 100); },
            checkFileExtension: function (element) {
                try {
                    var temp = element.value.split('.');
                    if (temp.length > 0)
                        if (element.accept.toUpperCase().indexOf(temp[temp.length - 1].toUpperCase()) !== -1)
                            return true;
                        else
                            return false;

                    else return false;
                } catch (ex) { return false; }
            },
            encodeImageFileAsURL: function (element) {
                //if (this.checkFileExtension(element)) {
                var delayInMilliseconds = 1000;
                var Defer = $.Deferred();
                var reader = new FileReader(); // load file
                reader.onloadend = function () {
                    //var img = new Image;
                    //img.onload = function () {
                    Defer.resolve(this);
                    TempDataIMG = reader.result;
                    setTimeout(function () {
                        $("#modalLoading").modal('toggle');
                        $("#modalManagmentUser").modal();
                    }, delayInMilliseconds);
                    //};
                    //img.src = reader.result;
                }
                reader.onerror = function () { Defer.resolve(null); };
                reader.readAsDataURL(element.files[0]);
                return Defer.promise();
                //} else {
                //    alert('àÅ×Í¡ä´éà©¾ÒÐ .png, .jpg, .jpeg');
                //}
            },
            imageToDataUri: function (img, width, height) {
                var canvas = document.createElement('canvas'), ctx = canvas.getContext('2d');
                canvas.width = width;
                canvas.height = height;
                ctx.drawImage(img, 0, 0, width, height);
                return canvas.toDataURL();
            }
        }

        function imgLoad(myFileImage, imgObjId) {
            $("#modalManagmentUser").modal('toggle');
            $("#modalLoading").modal({
                backdrop: 'static',
                keyboard: false,
            });
            $.when(imgProcess.encodeImageFileAsURL(myFileImage)).then(
                function (Data) {
                    var newDataUri = imgProcess.imageToDataUri(Data, imgProcess.reduceResolution(Data.width), imgProcess.reduceResolution(Data.height));
                    // Display('imgQuiz', newDataUri);                    
                });
        }
    </script>

    <%-- Search--%>
    <script>    
        function runScript(e) {
            //See notes about 'which' and 'key'
            if (e.keyCode == 13) {
                var keyword = document.getElementById("selectSearch").value;
                var data = document.getElementById("myInput").value;
                searchData = {};
                $.extend(searchData, {
                    tpyeSearch: keyword.toString(),
                    searchData: data.toString()
                })
                $.ajax({
                    type: "POST",
                    url: "problem_MasterUser.aspx/ajaxSearchUser",
                    data: "{searchData: " + JSON.stringify(searchData) + "}",
                    contentType: "application/json; charset=utf-8",
                    cache: false,
                    dataType: "JSON",
                    sync: false,                  
                    success: returnSearch,
                    error: function (response) {
                        alert(response);
                    }
                });
                return false;
            }
            function returnSearch(response) {
                _dataSearch = response.d;
                _dataSearch = $.parseJSON(_dataSearch);
                count = _dataSearch.length;
                $("#CountSum").html("จำนวนข้อมูลทั้งหมด :" + count);
                $(function () {
                    $("#jsGrid").jsGrid({
                        hight: "85%",
                        width: "100%",
                        sorting: true,
                        paging: true,
                        autoload: true,
                        data: _dataSearch,
                        pageIndex: 1,
                        pageSize: 11,
                        pageButtonCount: 3,
                        pagerFormat: "หน้าปัจจุบัน: {pageIndex} &nbsp;&nbsp; {first} {prev} {pages} {next} {last} &nbsp;&nbsp; หน้าทั้งหมด: {pageCount}",
                        pagePrevText: "<",
                        pageNextText: ">",
                        pageFirstText: "หน้าแรก  &nbsp;",
                        pageLastText: "หน้าสุดท้าย  &nbsp;",
                        pageNavigatorNextText: "&#8230;",
                        pageNavigatorPrevText: "&#8230;",

                        fields: [
                            { name: "EMP_NO", title: "No.", type: "text", width: "20%", css: "h6" },
                            { name: "EMP_NAME", title: "Name.", type: "text", width: "20%", css: "h6" },
                            { name: "POSITION_ID", title: "Brand.", type: "text", width: "20%", css: "h6" },
                            { name: "EMAIL", title: "Email.", type: "text", width: "50%", css: "h6" },
                            { name: "DEPTS", title: "Depts.", type: "text", width: "20%", css: "h6" },                          
                            {
                                name: "STATUS", title: "Status.", type: "text", width: "20%",
                                itemTemplate: function (value, item) {
                                    if(value == "ONLINE"){
                                        return "<div style='color: green'>" + value + "</div>"
                                    } else if (value == "OFFLINE") {
                                        return "<div style='color: red'>" + value + "</div>"
                                    } else {
                                        return "<div style='color: #00635a'>" + value + "</div>"
                                    }
                               
                                }
                            },                             
                        ],
                        rowClick: function (args) {
                            var getData = args.item;
                            var keys = Object.keys(getData);
                            var text = [];
                            detailProblem(getData.EMP_NO)
                        }
                    });
                });
            }
            $("#LoadTool").html(" ");
            
        }
    </script>

      <%-- detail problem--%>
    <script>     
        function detailProblem(number) {
            $("#modalManagmentUser").modal({
                backdrop: 'static',
                keyboard: false
            });

            <%--  getCheck Approve --%> 
            searchData = {};
            $.extend(searchData, {
                tpyeSearch: number,
                searchData: ""
            })
            $.ajax({
                type: "POST",
                url: "problem_MasterUser.aspx/ajaxSearchUserApprove",
                async: false,
                data: "{searchData: " + JSON.stringify(searchData) + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    _dataApprovedGet = response.d;
                    _dataApprovedGet = $.parseJSON(_dataApprovedGet);
                    if (_dataApprovedGet[0].STATUS == "ONLINE") {
                        document.getElementById("appUser").style.display = "None";
                        document.getElementById("dropDownAppUser").style.display = "None";
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }
            });

            //Approve User
            $('#appUser').click(function (e) {        
                approvedUser(number);
            });
            function approvedUser(number) {
                userApproved = {};
                $.extend(userApproved, {
                    EMP_NO: number,
                    EMP_NAME: "",
                    EMP_LASTNAME: "",
                    POSITION_ID: "",
                    EMAIL: "",
                    PASSWORD: "",
                    STATUS: "",
                    PITURE: "",
                    DEPTS: "",
                });
                $.ajax({
                    type: "POST",
                    url: "problem_MasterUser.aspx/CheckApproved",
                    data: "{userApproved: " + JSON.stringify(userApproved) + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "JSON",
                    async: false,
                    success: function (response) {
                        msg_User = response.d;
                        msg_User = $.parseJSON(msg_User);
                        toastr.success("Approved สำเร็จ", "แจ้งเตือน")
                        toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": true,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "3000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
                    },
                    error: function (response) {
                        alert(response);
                    }
                });
            }
        

             <%-- getname lastname --%>                
                var name_lastName = {};
                $.extend(name_lastName, {
                    EMP_NO: number,
                    EMP_NAME: "",
                    EMP_LASTNAME: "",
                    POSITION_ID: "",
                    EMAIL: "",
                    PASSWORD: "",
                    STATUS: "",
                    PITURE: "",
                    DEPTS: "",
                });
                $.ajax({
                    type: "POST",
                    url: "problem_MasterUser.aspx/GetUserNameLastNameUpdate",
                    data: "{name_lastName: " + JSON.stringify(name_lastName) + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "JSON",
                    async: false,
                    success: function (response) {
                        msg_User = response.d;
                        msg_User = $.parseJSON(msg_User);
                        $("#name_Change").val(msg_User[0]["EMP_NAME"]);
                        $("#lastName_Change").val(msg_User[0]["EMP_LASTNAME"]);
                    },
                    error: function (response) {
                        alert(response);
                    }
                });
            
              <%-- getEmail --%>     
                var userChangeEmail = {};
                $.extend(userChangeEmail, {
                    EMP_NO: number,
                    EMP_NAME: "",
                    EMP_LASTNAME: "",
                    POSITION_ID: "",
                    EMAIL: "",
                    PASSWORD: "",
                    STATUS: "",
                    PITURE: "",
                    DEPTS: ""
                });
                $.ajax({
                    type: "POST",
                    url: "problem_MasterUser.aspx/GetEmailUpdate",
                    data: "{userChangeEmail: " + JSON.stringify(userChangeEmail) + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "JSON",
                    async: false,
                    success: function (response) {
                        msg_User = response.d;
                        msg_User = $.parseJSON(msg_User);
                        $("#email_Change").val(msg_User[0]["EMAIL"]);
                    },
                    error: function (response) {
                        alert(response);
                    }
                });
     
            <%-- Change Brand By Admin--%>
            $('#change_Brand').click(function (e) {
                var isValid = true;
                $('#dropDownSelectBrand').each(function () {
                    if ($.trim($(this).val()) == '') {
                        isValid = false;
                        $(this).css({
                            "border": "1px solid red",
                            "background": "#FFCECE",
                        });
                    }
                    else {
                        $(this).css({
                            "border": "",
                            "background": ""
                        });
                    }
                });
                if (isValid == false) {
                    e.preventDefault();
                }
                else {
                    updateBrand(number);
                }
            });
            function updateBrand(number) {
                userChangeBrand = {};
                $.extend(userChangeBrand, {
                    EMP_NO: number,
                    EMP_NAME: "",
                    EMP_LASTNAME: "",
                    POSITION_ID: $("#dropDownSelectBrand").val(),
                    EMAIL: "",
                    PASSWORD: "",
                    STATUS: "",
                    PITURE: "",
                    DEPTS: "",
                });
                $.ajax({
                    type: "POST",
                    url: "problem_MasterUser.aspx/changeBrand",
                    data: "{userChangeBrand: " + JSON.stringify(userChangeBrand) + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "JSON",
                    async: false,
                    success: function (response) {
                        msg_Brand = response.d;
                        msg_Brand = $.parseJSON(msg_Brand);
                        toastr.success("เปลี่ยน Brand สำเร็จ", "แจ้งเตือน")
                        toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": true,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "3000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
                    },
                    error: function (response) {
                        alert(response);
                    }
                });
            }

              <%--  Change Name Last Name--%>
            $('#change_NameLastName').click(function (e) {
                var isValid = true;
                $('#name_Change').each(function () {
                    if ($.trim($(this).val()) == '') {
                        isValid = false;
                        $(this).css({
                            "border": "1px solid red",
                            "background": "#FFCECE",
                        });
                    }
                    else {
                        $(this).css({
                            "border": "",
                            "background": ""
                        });
                    }
                });
                if (isValid == false) {
                    e.preventDefault();
                }
                else {
                    $('#lastName_Change').each(function () {
                        if ($.trim($(this).val()) == '') {
                            isValid = false;
                            $(this).css({
                                "border": "1px solid red",
                                "background": "#FFCECE",
                            });
                        }
                        else {
                            $(this).css({
                                "border": "",
                                "background": ""
                            });
                        }
                    });
                    if (isValid == false) {
                        e.preventDefault();
                    }
                    else {
                        updateUser(number);
                    }
                }
            });
            function updateUser(number) {
                var name_lastName = {};
                $.extend(name_lastName, {
                    EMP_NO: number,
                    EMP_NAME: $("#name_Change").val(),
                    EMP_LASTNAME: $("#lastName_Change").val(),
                    POSITION_ID: "",
                    EMAIL: "",
                    PASSWORD: "",
                    STATUS: "",
                    PITURE: "",
                    DEPTS: "",
                });
                $.ajax({
                    type: "POST",
                    url: "problem_MasterUser.aspx/userNameLastNameUpdate",
                    data: "{name_lastName: " + JSON.stringify(name_lastName) + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "JSON",
                    async: false,
                    success: function (response) {
                        msg_UserUpdate = response.d;
                        msg_UserUpdate = $.parseJSON(msg_UserUpdate);
                        if (msg_UserUpdate == "UPDATE SUCCESS") {
                            swal({
                                title: "เตือน !",
                                text: "ดำเนินการแก้ไขข้อมูลสำเร็จ!",
                                type: "success",
                                dangerMode: true,
                                animation: false,
                                customClass: "animated jackInTheBox",
                                confirmButtonClass: "btn-success",
                                confirmButtonText: "OK"
                            });
                        } else if (msg_UserUpdate == "UPDATE DATA FAILED") {
                            toastr.error("ไม่สามารถแก้ไขได้กรุณาตรวจสอบ", "แจ้งเตือน")
                            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": false,
                                "positionClass": "toast-top-right",
                                "preventDuplicates": false,
                                "onclick": null,
                                "showDuration": "300",
                                "hideDuration": "1000",
                                "timeOut": "3000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            }
                        }
                    },
                    error: function (response) {
                        alert(response);
                    }
                });
            }

            <%-- Chang Depts--%>
            $('#change_Depts').click(function (e) {
                var isValid = true;        
                    $('#dropDownSelectDepts').each(function () {
                        if ($.trim($(this).val()) == '') {
                            isValid = false;
                            $(this).css({
                                "border": "1px solid red",
                                "background": "#FFCECE",
                            });
                        }
                        else {
                            $(this).css({
                                "border": "",
                                "background": ""
                            });
                        }
                    });
                    if (isValid == false) {
                        e.preventDefault();
                    }
                    else {
                        updateDepts(number);
                    }
                });
            function updateDepts(number) {
                userChangeDepts = {};
                $.extend(userChangeDepts, {
                    EMP_NO: number,
                    EMP_NAME: "",
                    EMP_LASTNAME: "",
                    POSITION_ID: "",
                    EMAIL: "",
                    PASSWORD: "",
                    STATUS: "",
                    PITURE: "",
                    DEPTS: $("#dropDownSelectDepts").val(),
                });
                $.ajax({
                    type: "POST",
                    url: "problem_MasterUser.aspx/changeDepts",
                    data: "{userChangeDepts: " + JSON.stringify(userChangeDepts) + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "JSON",
                    async: false,
                    success: function (response) {
                        msg_Dept = response.d;
                        msg_Dept = $.parseJSON(msg_Dept);
                        toastr.success("เปลี่ยน Dept สำเร็จ", "แจ้งเตือน")
                        toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": true,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "3000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
                    },
                    error: function (response) {
                        alert(response);
                    }
                });
            }

             <%-- Chang Email--%>
            $('#update_email').click(function (e) {
                var isValid = true;
                $('#email_Change').each(function () {
                    if ($.trim($(this).val()) == '') {
                        isValid = false;
                        $(this).css({
                            "border": "1px solid red",
                            "background": "#FFCECE",
                        });
                    }
                    else {
                        $(this).css({
                            "border": "",
                            "background": ""
                        });
                    }
                });
                if (isValid == false) {
                    e.preventDefault();
                }
                else {
                    updateEmail(number);
                }
            });
            function updateEmail(number) {
                userChangeEmail = {};
                $.extend(userChangeEmail, {
                    EMP_NO: number,
                    EMP_NAME: "",
                    EMP_LASTNAME: "",
                    POSITION_ID: "",
                    EMAIL: $("#email_Change").val(),
                    PASSWORD: "",
                    STATUS: "",
                    PITURE: "",
                    DEPTS: ""
                });
                $.ajax({
                    type: "POST",
                    url: "problem_MasterUser.aspx/changeEmail",
                    data: "{userChangeEmail: " + JSON.stringify(userChangeEmail) + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "JSON",
                    async: false,
                    success: function (response) {
                        msg_Dept = response.d;
                        msg_Dept = $.parseJSON(msg_Dept);
                        toastr.success("เปลี่ยน Email สำเร็จ", "แจ้งเตือน")
                        toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": true,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "3000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
                    },
                    error: function (response) {
                        alert(response);
                    }
                });
            }

            <%-- Change Piture --%>
            $('#change_piture').click(function (e) {
                var isValid = true;           
                    $('#filePiture').each(function () {
                        if ($.trim($(this).val()) == '') {
                            isValid = false;
                            $(this).css({
                                "border": "1px solid red",
                                "background": "#FFCECE",
                            });
                        }
                        else {
                            $(this).css({
                                "border": "",
                                "background": ""
                            });
                        }
                    });
                    if (isValid == false) {
                        e.preventDefault();
                    }
                    else {
                        changePiture(number);
                    }
            });
            function changePiture(number) {
                userChangeLink = {};
                $.extend(userChangeLink, {
                    EMP_NO: number,
                    EMP_NAME: "",
                    EMP_LASTNAME: "",
                    POSITION_ID: "",
                    EMAIL: "",
                    PASSWORD: "",
                    STATUS: "",
                    PITURE: "",
                    DEPTS: ""
                });
                $.ajax({
                    type: "POST",
                    url: "problem_MasterUser.aspx/GetLink",
                    data: "{userChangeLink: " + JSON.stringify(userChangeLink) + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "JSON",
                    async: false,
                    success: function (response) {
                        msg_User = response.d;
                        msg_User = $.parseJSON(msg_User);
                        var linkOld = msg_User[0]["PITURE"]
                        UpdateLink(linkOld);
                    },
                    error: function (response) {
                        alert(response);
                    }
                });
                function UpdateLink(linkOld) {
                    var linkOld2 = linkOld;
                    var TempDataIMGSend = String(TempDataIMG);
                    userChangePiture = {};
                    $.extend(userChangePiture, {
                        EMP_NO: number,
                        EMP_NAME: "",
                        EMP_LASTNAME: "",
                        POSITION_ID: "",
                        EMAIL: "",
                        PASSWORD: "",
                        STATUS: "",
                        PITURE: linkOld2,
                        DEPTS: "",
                        LinkDitinationPiture: TempDataIMGSend
                    });
                    $.ajax({
                        type: "POST",
                        url: "problem_MasterUser.aspx/changePiture",
                        data: "{userChangePiture: " + JSON.stringify(userChangePiture) + "}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "JSON",
                        async: false,
                        success: function (response) {
                            msg_Piture = response.d;
                            msg_Piture = $.parseJSON(msg_Piture);
                            toastr.success("เปลี่ยน Piture สำเร็จ", "แจ้งเตือน")
                            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": true,
                                "positionClass": "toast-top-right",
                                "preventDuplicates": false,
                                "onclick": null,
                                "showDuration": "300",
                                "hideDuration": "1000",
                                "timeOut": "3000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            }
                        },
                        error: function (response) {
                            alert(response);
                        }
                    });
                }
            }
        }
    </script>
</asp:Content>
