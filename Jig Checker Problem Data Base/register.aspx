﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="register.aspx.cs" Inherits="Jig_Checker_Problem_Data_Base.register" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge"  />
    <title>Sign Up Form</title>    
    <!-- Font Icon -->
    <link href="Content/Register/fonts/material-icon/css/material-design-iconic-font.min.css" rel="stylesheet" />
    <!-- Main css -->
    <link href="Content/Register/css/style.css" rel="stylesheet" />
    <link href="Content/bootstrap-sweetalert-master/dist/animate.css" rel="stylesheet" />
    <link href="Content/bootstrap-sweetalert-master/dist/sweetalert.css" rel="stylesheet" />
</head>
<body>
    <div class="main">            
            <div class="container">
                <div class="signup-content">
                    <form  id="signup-form" class="signup-form">
                        <h2 class="form-title">Create account</h2>
                        <div class="form-group">
                            <input type="text" class="form-input required"  id="empID" placeholder="Your employee id (ไม่ต้องใส่ C)" />
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-input required"  id="empName" placeholder="Your name" />
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-input required" id="empLastname" placeholder="Your last name" />
                        </div>
                           <div class="form-group">
                            <input type="email" class="form-input required" id="empEmail" placeholder="Your email" />
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-input required" id="empPassword" placeholder="Password" />                           
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-input required"  id="re_password" placeholder="Repeat your password" />
                        </div> 
                        <div class="form-group">
                            <select id="selectBrand" class="form-input required">
                                <option value="">Select Brand</option>
                                <option value="M1">M1</option>
                                <option value="J4">J4</option>
                                <option value="J3">J3</option>
                                <option value="J2">J2</option>
                                <option value="J1">J1</option>
                            </select>
                        </div>
                         <div class="form-group">
                          <select id="selectDepts" class="form-input required">
                              <option value="">Select Depts.</option>
                              <option value="PDE-J RA">PDE-J RA</option>
                              <option value="PDE-J HT">PDE-J HT</option>
                          </select>
                        </div>                         
                        <div class="form-group">
                            <input type="button"  id="submitSignup" class="form-submit" value="Sign up" />
                        </div>
                    </form>
                    <p class="loginhere">
                        Have already an account ? <a href="login.aspx" class="loginhere-link">Login here</a>
                    </p>
                </div>
            </div>       
    </div>
      <!-- JS -->
    <script src="Content/js/jquery-3.2.1.min.js"></script>
  <%-- <script src="Content/Register/vendor/jquery/jquery.min.js"></script>--%>
   <script src="Content/Register/js/main.js"></script>
   <script src="Content/bootstrap-sweetalert-master/dist/sweetalert.js"></script>
   <script src="Content/bootstrap-sweetalert-master/dist/sweetalert.min.js"></script>
 
    <script>
        $('#submitSignup').click(function (e) {
            var isValid = true;
            $('input.required').each(function () {
                if ($.trim($(this).val()) == '') {
                    isValid = false;
                    $(this).css({
                        "border": "1px solid red",
                        "background": "#FFCECE",
                    });
                }
                else {
                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                }
            });
            if (isValid == false) {
                e.preventDefault();
            }
            else {
                $('select.required').each(function () {
                    if ($.trim($(this).val()) == '') {
                        isValid = false;
                        $(this).css({
                            "border": "1px solid red",
                            "background": "#FFCECE",
                        });
                    }
                    else {
                        $(this).css({
                            "border": "",
                            "background": ""
                        });
                    }
                });
                if (isValid == false) {
                    e.preventDefault();
                }
                else {
                    var pass1 = $("#empPassword").val();
                    var pass2 = $("#re_password").val();
                    if (pass1 == pass2) {
                        $("#empPassword").css({
                            "border": "",
                            "background": ""
                        });
                        $("#re_password").css({
                            "border": "",
                            "background": ""
                        });
                        singupUser();
                    } else {
                        $("#empPassword").css({
                            "border": "1px solid red",
                            "background": "#FFCECE",
                        });
                        $("#re_password").css({
                            "border": "1px solid red",
                            "background": "#FFCECE",
                        });
                    }                 
                }
            }
        });
        function singupUser() {               
                var singupList = {};
                $.extend(singupList, {
                    EMP_NO: $("#empID").val(),
                    EMP_NAME: $("#empName").val(),
                    EMP_LASTNAME: $("#empLastname").val(),
                    POSITION_ID: $("#selectBrand").val(),
                    EMAIL: $("#empEmail").val(),
                    PASSWORD: $("#re_password").val(),
                    STATUS: "",
                    PITURE: "",
                    DEPTS: $("#selectDepts").val(),
                    LinkDitinationPiture: ""
                });
                $.ajax({
                    type: "POST",
                    url: "register.aspx/addUser",
                    data: "{singupList: " + JSON.stringify(singupList) + "}",
                    contentType: "application/json; charset=utf-8",
                    cache: false,
                    dataType: "JSON",
                    success: function (response) {
                        _dataUser = response.d;
                        _dataUser = $.parseJSON(_dataUser);                      
                        if (_dataUser == "SUCCESS") {
                            swal({
                                title: "เตือน !",
                                text: "ลงทะเบียน สำเร็จ!",
                                type: "success",
                                dangerMode: true,
                                animation: true,                               
                                confirmButtonText: "OK"
                            },
                            function () {
                                location.href = "login.aspx";
                            });
                        } else if (_dataUser == "SAVE DATA FAILED") {
                            swal({
                                title: "เตือน !",
                                text: "โปรดเพิ่มไฟล์แนบ PDF!",
                                type: "error",
                                dangerMode: true,
                                animation: true,                               
                                confirmButtonText: "OK"
                            });
                        }
                        else if (_dataUser == "SAVD TOOLID DUPLICATE") {
                            swal({
                                title: "เตือน !",
                                text: "พนักงานได้ลงทะเบียนแล้ว!",
                                type: "error",
                                dangerMode: true,
                                animation: true,                              
                                confirmButtonText: "OK"
                            });
                        }
                    },
                    error: function (response) {
                        alert(response);
                    }
                });
            }               
    </script>
</body>
</html>
